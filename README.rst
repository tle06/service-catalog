Service Catalogue Application
=============================

|build-status| |docs|

Purpose
-------

`Service Catalogue` is made to list all the services use in your company. Each service can be link to other, to provider, to a pricings or a contracts.

Documentation
---------------------

You will find complete documentation for setting up your project at `here`_.

.. _here: https://service-catalog.readthedocs.io/en/latest/?badge=latest

Get in touch
------------

To be definied

Contributing
------------

To be definied

Quickstart
----------


#. Git clone the porject

#. Run docker compose


.. |build-status| image:: https://gitlab.com/tle06/service-catalog/badges/main/pipeline.svg
    :alt: build status
    :target: https://gitlab.com/tle06/service-catalog/-/commits/main
.. |docs| image:: https://readthedocs.org/projects/service-catalog/badge/?version=latest
    :target: https://service-catalog.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

License
-------

`GNU`_ © 2022 Service Catalogue, TLNK & contributors

.. _GNU: LICENSE