Pricing
=======

The pricing object refer to a price affected to a service.
It can be use as a pricing follow up or inventory of pricing without the necessity of a contract

Composition of a pricing
------------------------

.. csv-table::
   :widths: auto
   :header: "Field", "Description", "Mandatory", "Deault value"

   "Name","The name for the pricing object","Yes"
   "Total cost","The total cost of the contract","No"
   "Volume","The total volume in the contract","No"
   "Cost/unit","The cost/unit","No"
   "Currency","The currency of the pricing","No",EUR,
   "Pricing date","The date when the pricing was obtain","No"

