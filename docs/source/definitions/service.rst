Service
=======

A service can be a work performed by a person a team or an online subscription.
In the service page you can register all the service proposed by your corporate.

Composition of a service
------------------------

.. csv-table::
   :widths: auto
   :header: "Field", "Description", "Mandatory", "Deault value"

   "Name","Name of the service","Yes"
   "service category","Category where the","No"
   "Description","The descriptino of the service","No"
   "Department","The department in charge of the service","No"
   "Sub-Department","The sub-department in charge of the service","No"
   "Owner","Who own the service","No"
   "Supported by","Who give suppor to the service","No"
   "Tier","The tier","Yes","Tier 1"
   "Provider","Who is the provider","No"
   "Status","The status of the service","Yes","Active"
   "Health Check","The status page or any health check url for the serivce","No"
   "Icon URL","The icon url of the service","No"
   "Company","The company name who bought the service","No"
   "Integrated with IDP","Is the service integarted with the IDP","No"
   "Service URL","The URL of the service if web","No"
   "Service website","The website of the provider or the service","No"
   "Created by","The creator of the service","Yes","User who created the service"



Service tiers
-------------

The following Tiers are avaialble in the app. You can refer to the following `definition and example <https://orhanergun.net/tier-1-tier-2-and-tier-3-service-providers/>`_.

.. csv-table::
   :widths: auto
   :header: "Tier", "Description"

   "Tier 1","Top level service do not depend on other service"
   "Tier 2","Service that depend on Tier 1 service"
   "Tier 3","Service that depend on Tier 2 service"
   "Tier 4","Service that depend on Tier 3 service"


Service category
----------------

Service can be classify with custom category.
Category can be created by an Admin from the admin web page.

|diagram|

.. |diagram| image:: https://orhanergun.net/wp-content/uploads/2019/11/tiers.jpg
                  :target: https://orhanergun.net/wp-content/uploads/2019/11/tiers.jpg


Service Status
--------------

.. csv-table::
   :widths: auto
   :header: "Status", "Description"

   "Active","Service is currently in used"
   "Suspended","Service is suspended"
   "Deleted","Service is not used anymore or decomission"
