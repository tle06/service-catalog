Contract
========

The contract object refer to the legal binding between a provider and the company using the specific service.

Composition of a contract
-------------------------

.. csv-table::
   :widths: auto
   :header: "Field", "Description", "Mandatory", "Default value"

   "Name","The name for the contract","Yes"
   "Provider","The provider link to the contract","No"
   "Start date","The start date of the contract","No"
   "End date","The end date of the contract","No"
   "Renewal date","The renewal date of the contract","No"
   "Duration","The duration","No"
   "Duration unit","The duration unit Day/Month/Year","Yes","Month"
   "Total cost","The total cost of the contract","No"
   "Volume","The total volume in the contract","No"
   "Cost/unit","The cost/unit","No"
   "Currency","The currency of the contract","No","EUR"
   "Status","The contract status","No"
   "Company","The company who signed the contract","No"

Contract duration unit
----------------------

the current value available are:

- Day
- Month
- Year

Contract Status
---------------

.. csv-table::
   :widths: auto
   :header: "Status", "Description"

    "Active","The contract is running"
    "Suspended","The contract has been suspended"
    "Terminated","The contract is finished"

Permission
----------

Contracts can be edit if the following permission is assigned to the users. You can also set them to a group and attach the group to the user.

- service.view_contract
- service.add_contract
- service.change_contract
- service.delete_contract
- service.view_contractdocument
- service.delete_contractdocument
- service.add_contractdocument