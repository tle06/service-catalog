Welcome to Service catalogue's documentation!
=============================================

**Service Catalogue** is made to list all the services used in your company.
Each service can refer to:
- Others services
- To a specific provider
- Contracts
- Pricings

Check out the :doc:`usage` section for further information, including
how to :ref:`installation` the project.

.. note::

   This project is under active development.

Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: Definitions
   :numbered:

   /definitions/service
   /definitions/Hardware
   /definitions/provider
   /definitions/contract
   /definitions/pricing

.. toctree::
   :maxdepth: 2
   :caption: Installation
   :numbered:

   /installation/deployment
   /installation/environment
   /installation/scim

.. toctree::
   :maxdepth: 2
   :caption: Development
   :numbered:

   /development/local

