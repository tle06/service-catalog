Local development
=================

The project use Poetry as package maanger. Follow the documentation to install it in your environment

Clone the project

.. code-block:: cmd
   :caption: git clone

   git clone https://gitlab.com/tle06/service-catalog.git

Create the virtual environment and install the dependency

.. code-block:: cmd
   :caption: poetry shell

   cd src
   poetry shell
   poetry install

Run the application with poetry

.. code-block:: cmd
   :caption: poetry run

   cd src
   poetry ryn python manage.py runserver


Database installation
---------------------

Migrate the database

.. code-block:: cmd
   :caption: migrate

    cd src
    poetry run python manage.py migrate

Docker build
------------

.. code-block:: cmd
   :caption: docker build

   docker build -t tlnk/catalogue:local .

Docker compose
--------------

.. code-block:: cmd
   :caption: docker compose

   docker compose --profile app up


If you need to build

.. code-block:: cmd
   :caption: docker compose

   docker compose --profile app build

Create the super user
----------------------

.. code-block:: cmd
   :caption: createsuperuser

    cd src
    poetry run python manage.py createsuperuser

Get all manage.py commands
--------------------------

.. code-block:: cmd
   :caption: manage.py

    cd src
    poetry run python manage.py

Pre-commit
----------

Pre-commit is installe on the project. To activate it on you local, do:

.. code-block:: cmd
   :caption: pre-commit install

    pre-commit install

To use it, do:

.. code-block:: cmd
   :caption: pre-commit run

    cd src
    pre-commit run --all-file

Formating
----------

Formating is following Black

.. code-block:: cmd
   :caption: black formating

    cd src
    poetry run black . *.py

As well ruff is available

.. code-block:: cmd
   :caption: black formating

    cd src
    poetry run ruff --fix . *.py