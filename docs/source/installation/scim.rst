SCIM
====

SCIM v2 is implemented for Users and Groups according the `RFC <https://www.rfc-editor.org/rfc/rfc7643>`.
It's currently compatible with the following provider:

- Onelogin
- Okta (not tested)
  
SCIM URL
--------

The following URL are available:

.. csv-table::
   :widths: auto
   :header: "URL", "HTTP Methode", "Description"

   "/scim/v2/Users","GET","List all available users"
   "/scim/v2/Users/<id>","GET","List user id"
   "/scim/v2/Users","POST","Create user"
   "/scim/v2/Users/<id>","PUT","Update user id"
   "/scim/v2/Users/<id>","PATCH","Update user id"
   "/scim/v2/Users/<id>","DELETE","Delete user id"
   "/scim/v2/Groups","GET","List all available groups"
   "/scim/v2/Groups/<id>","GET","List group id"
   "/scim/v2/Groups","POST","Create group"
   "/scim/v2/Groups/<id>","PUT","Update group id"
   "/scim/v2/Groups/<id>","PATCH","Update group id"
   "/scim/v2/Groups/<id>","DELETE","Delete group id"

SCIM Authentication
-------------------

Currently user need to be admin to use the SCIM endpoint. To authenticate your IDP you will need to generate a token in the admin page under the Token menu for the specific user.

Then you will have to configure the header as follow in your identity provider.

.. code-block:: json
    Authorization: Token copyTheTokenHereFromTheAdminPage


User schema
-----------

Not the whole User schema is implemented yet. Below you will find the data available with an example.
The enterprise schema extension is implemented as follow:

- costCenter will be map to department in the user profile
- organization will be map to company in the user profile
- division will be map to sub-department in the user profile
- department will be map to department in the user profile

In the case that the department/sub-department/company do not exist, the value will be created first then be affected to the user.
If the manager do not exist in the user base, the afffectation in the user profile will be skip until the user is created.

- On the first run the SCIM will create all the user.
- On the second run, manager will be assigned

.. code-block:: json
    {
        "schemas": [
            "urn:ietf:params:scim:schemas:core:2.0:User",
            "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",
            "urn:scim:my:custom:schema"
        ],
        "id": "2819c223-7f76-453a-919d-413861904646",
        "externalId": "701984",
        "userName": "bjensen@example.com",
        "name": {
            "formatted": "Ms. Barbara J Jensen, III",
            "familyName": "Jensen",
            "givenName": "Barbara",
        },
        "displayName": "Babs Jensen",
        "emails": [
            {
                "value": "bjensen@example.com",
                "type": "work",
                "primary": true
            }
        ],
        "photos": [
            {
                "value": "https://photos.example.com/profilephoto/72930000000Ccne/F",
                "type": "photo"
            }
        ],
        "title": "Tour Guide",
        "active": true,
        "groups": [
            {
                "value": "e9e30dba-f08f-4109-8486-d5c6a331660a",
                "$ref": "../Groups/e9e30dba-f08f-4109-8486-d5c6a331660a",
                "display": "Tour Guides"
            },
            {
                "value": "fc348aa8-3835-40eb-a20b-c726e15c55b5",
                "$ref": "../Groups/fc348aa8-3835-40eb-a20b-c726e15c55b5",
                "display": "Employees"
            },
            {
                "value": "71ddacd2-a8e7-49b8-a5db-ae50d0a5bfd7",
                "$ref": "../Groups/71ddacd2-a8e7-49b8-a5db-ae50d0a5bfd7",
                "display": "US Employees"
            }
        ],
        "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User": {
            "employeeNumber": "701984",
            "costCenter": "4130",
            "organization": "Universal Studios",
            "division": "Theme Park",
            "department": "Tour Operations",
            "manager": {
                "value": "26118915-6090-4610-87e4-49d8ca9f808d",
                "$ref": "../Users/26118915-6090-4610-87e4-49d8ca9f808d",
                "displayName": "John Smith"
            }
        },
        "urn:scim:my:custom:schema": {
            "building_id": "Paris 1"
        },
        "meta": {
            "resourceType": "User",
            "created": "2010-01-23T04:56:22Z",
            "lastModified": "2011-05-13T04:42:34Z",
            "location": "https://example.com/v2/Users/2819c223-7f76-453a-919d-413861904646"
        }
    }

Group schema
------------

Not the whole Group schema is implemented. Below you will find the data available with an example.

.. code-block:: json
    {
        "schemas": [
            "urn:ietf:params:scim:schemas:core:2.0:Group"
        ],
        "id": "e9e30dba-f08f-4109-8486-d5c6a331660a",
        "displayName": "Tour Guides",
        "members": [
            {
                "value": "2819c223-7f76-453a-919d-413861904646",
                "$ref": "https://example.com/v2/Users/2819c223-7f76-453a-919d-413861904646",
                "display": "Babs Jensen"
            },
            {
                "value": "902c246b-6245-4190-8e05-00816be7344a",
                "$ref": "https://example.com/v2/Users/902c246b-6245-4190-8e05-00816be7344a",
                "display": "Mandy Pepperidge"
            }
        ],
        "meta": {
            "resourceType": "Group",
            "location": "https://example.com/v2/Groups/e9e30dba-f08f-4109-8486-d5c6a331660a"
        }
    }