Environment variable
====================

The service catalogue application can be deployed with custom configuration through the environment variable.
The environ `package <https://pypi.org/project/environs/>` is used to read the different value. The .env file will be read if place into /app inside the container.

- OIDC is base on mozilla-django-oidc package
- Social login are base on Django all-auth package

Value available
---------------

.. csv-table::
   :widths: auto
   :header: "Name", "Default value", "Description", "Type"

   "DJANO_SECRET_KEY","utils.get_random_secret_key()","The django secret key",""
   "DJANGO_DEBUG","FALSE","If set to true then DEBUG will be activated","Boolean"
   "CSRF_TRUSTED_ORIGINS","http://localhost:8000","CSRF url to be trust. Currently support only one value","String"
   "EMAIL_BACKEND","django.core.mail.backends.smtp.EmailBackend","The email backend that django need to use","String"
   "EMAIL_HOST","The email server host name or IP","String",""
   "EMAIL_HOST_USER","The username of the email server","String",""
   "EMAIL_HOST_PASSWORD","The password for the username","String",""
   "EMAIL_PORT","587","The emails erver port used","integer"
   "DB_DRIVER","django.db.backends.mysql","The database driver that django will use","String"
   "DB_NAME","catalog","The database name","String"
   "DB_USER","catalog","The username to connect to the database","String"
   "DB_PASSWD","catalog","The password to connect to the database","String"
   "DB_SERVER","127.0.0.1","The database server url or IP address","String"
   "DB_PORT","3306","The dabase port","integer"
   "DB_SSL","FALSE","If the database need to use SSL set the value to True","Boolean"
   "ACCOUNT_DEFAULT_HTTP_PROTOCOL","http","ref to all-auth django module","String"
   "ACCOUNT_EMAIL_VERIFICATION","none","ref to all-auth django module","String"
   "SL_AZURE_TENANT_ID","The Azure tenant ID for All_uath module","String",""
   "SL_KEYCLOAK_URL","The keycloak URL for all_auth module","String",""
   "SL_KEYCLOAK_REALM","The keycloak realm for all_auth module","String",""
   "SL_OKTA_BASE_URL","The OKTA base url for all_auth module","String",""
   "SL_GITLAB_URL","The Gitlab base url for all_auth module","String",""
   "SL_SCOPE","The Gitlab Scope for all_auth module","String",""
   "SL_OPENID_SERVER_ID","default","The OPENID server for all_auth module","String"
   "SL_OPENID_SERVER_NAME","default","The OPENID server name for all_auth module","String"
   "SL_OPENID_SERVER_URL","http://localhost/endpoint/","The OPENID server url for all_auth module","String"
   "USE_AWS_S3_FOR_FILES","False","If set to true all file will be upload to the S3 bucket specified","Boolean"
   "AWS_ACCESS_KEY_ID","The aws access key use for the upload module","String",""
   "AWS_ACCESS_KEY_SECRET","The aws access secret","String",""
   "AWS_S3_BUCKET_NAME","The aws bucket name where the file will upload","String",""
   "AWS_S3_CUSTOM_DOMAIN","AWS_STORAGE_BUCKET_NAME.s3.amazonaws.com","The custom domain for the file","String"
   "AWS_S3_SIGNATURE_VERSION","s3v4","Signature version","String"
   "AWS_S3_REGION_NAME","eu-west-3","The AWS region","String"
   "AWS_S3_FILE_OVERWRITE","TRUE","If set to true then file will be overwrite in the bucket","Boolean"
   "AWS_DEFAULT_ACL","You can specify the default ACL for the file","String",""
   "AWS_S3_VERIFY","TRUE","Verify","Boolean"
   "AWS_STATIC_LOCATION","static","The static fodler name in the bucket","String"
   "AWS_PUBLIC_MEDIA_LOCATION","media/public","The public media fodler name in the bucket","String"
   "AWS_PRIVATE_MEDIA_LOCATION","media/private","the private media folder name in the bucket","String"
   "STATIC_URL","/static/","The default static url","String"
   "MEDIA_URL","mediafiles/","The default media url","String"
   "DEFAULT_PAGINATION","24","The default paginatino if not configuration set in the database","integer"
   "HEROKU_USE_STATICFILES","FALSE","If set to true the module for heroku staticfile will be laoded","Boolean"
   "DJANGO_SUPERUSER_EMAIL",,"Username of the superuser","String"
   "DJANGO_SUPERUSER_PASSWORD",,"Superuser password", "String"
   "GUNICORN_WORKER",1,"Gunicorn number of worker","Integer"
   "PORT",8000,"Port for gunicorn","Integer"
   "DB_CONNECTION_TIEMOUT",3,"Timeout in second for the database connection","Integer"
   "ALLOWED_HOSTS",'*',"Allowed host for CSRF","list"
   "CSRF_TRUSTED_ORIGINS","[http://localhost:8000]","The trust origins for the CSRF token","List"
   "CORS_ALLOWED_ORIGINS","[http://localhost:8000]","CORS allowed domain list","List"
   "CORS_ORIGIN_WHITELIST","[http://localhost:8000]","CORS origin whitelist","List"
   "OIDC_ACTIVATED", "False","Activate OIDC auth and URL","Boolean"
   "OIDC_RP_CLIENT_ID", "None","OIDC client ID value","String"
   "OIDC_RP_CLIENT_SECRET", "None","OIDC client secret value","String"
   "OIDC_OP_AUTHORIZATION_ENDPOINT", "None","OIDC Authorization endpoint url","String"
   "OIDC_OP_TOKEN_ENDPOINT", "None","OIDC token endpoint url","String"
   "OIDC_OP_USER_ENDPOINT", "None","OIDC user endpoint url","String"
   "OIDC_RP_SIGN_ALGO", "RS256","OIDC Signature algo","String"
   "OIDC_OP_JWKS_ENDPOINT", "None","OIDC JWK endpoint url","String"
   "OIDC_CREATE_USER", "True","Will create the user if not exist","Boolean"
   "SECURE_SSL_REDIRECT","True","Enable secure SSL redirect for OIDC plugin","Boolean"
   "BACKUP_IGNORE_CONSISTENCY","True","gnores inconsistency between the MigrationRecorder and local migration files", "Boolean"
   "ACTIVATE_DUMP_APP_FROM_IDP","False","Activate the feature to dump application from user IDP. Require SCIM actif","Boolean"
   "IDP_PROVIDER_NAME","onelogin","Default IDP provider name. Currently only Onelogin IDP is suported","String"
   "IDP_CLIENT_ID","None","IDP client ID","String"
   "IDP_CLIENT_SECRET","None","IDP client secret","String"
   "IDP_ROOT_URL","None","IDP root URL to dump app info from","String"
   "DJANGO_EXEC_MAKEMIGRATIONS","None","Run makemigrations command at container start","Boolean"
   "DJANGO_EXEC_MIGRATE","None","Run migrate command at container start","Boolean"
   "DJANGO_EXEC_CREATESUPERUSER","None","Run createsuperuser command at container start","Boolean"
   "DJANGO_EXEC_COLLECTSTATIC","None","Run collectstatic command at container start","Boolean"
   "USE_GUNICORN","None","Server will run with gunicorn intead of runserver localhost:PORT command","Boolean"
   "SERVER_BIND_IP","localhost","The ID or FQDN where the server will run with. Use 0.0.0.0 for prod","String"
   "AC_DB_DRIVER","mssql","The database driver that django will use for the intemo access control server","String",
   "AC_DB_NAME","access_control","The database name for the intemo access control server","String",
   "AC_DB_USER","service_catalog","The user for the intemo access control server","String",
   "AC_DB_PASSWD","None","The password for the intemo access control server","String",
   "AC_DB_SERVER","127.0.0.1","The server FQDN or IP for the intemo access control server","String",
   "AC_DB_PORT","1433","The server port for the intemo access control server","String",
   "AC_DB_ODBC_DRIVER_VERSION","ODBC Driver 17 for SQL Server","The ms SQL driver to use","String",
   "AC_DB_UNICODE_RESULT","True","Unicol result","Boolean",
   "AC_DB_TRUST_CERTIFICATE","True","Trust server certificate","Boolean",
   "AC_DB_CONNECTION_TIMEOUT","1","Connection timeout for the intemo access control server","Integer",
   "AC_DB_CONNECTION_RETRIES","5","Connection retries for the intemo access control server","Integer",
   "WORKER_MODE","False","Start app in worker mode. Only Redis and celery worker, beat will be running","Boolean"
   "START_REDIS","False","Start a redis server for the schedule task","Boolean"
   "START_CELERY_WORKER","False","Start celery worker for schedule task","Boolean"
   "START_CELERY_BEAT","False","Start celery beat for schedule task","Boolean"