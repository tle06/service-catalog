"""service_catalog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from service.views.cost import CostDetailPerUserView, CostListView
from service.views.company import (
    CompanyCreateView,
    CompanyDeleteView,
    CompanyDetailView,
    CompanyExport,
    CompanyListView,
    CompanySearchView,
    CompanyUpdateView,
)

from service.views.provider import (
    ProviderCreateView,
    ProviderDeleteView,
    ProviderDetailView,
    ProviderExport,
    ProviderListView,
    ProviderSearchView,
    ProviderUpdateView,
)
from service.views.service import (
    ServiceCreateView,
    ServiceDeleteView,
    ServiceDetailView,
    ServiceExport,
    ServiceListView,
    ServiceSearchView,
    ServiceUpdateView,
    ProviderQuickCreateView,
)
from service.views.contract import (
    ContractCreateView,
    ContractDeleteView,
    ContractDetailView,
    ContractDocumentCreateView,
    ContractDocumentRemoveView,
    ContractExport,
    ContractListView,
    ContractSearchView,
    ContractUpdateView,
)
from service.views.pricing import (
    PricingCreateView,
    PricingDeleteView,
    PricingDetailView,
    PricingExport,
    PricingListView,
    PricingSearchView,
    PricingUpdateView,
)
from service.views.link import (
    AddServiceLinkToServiceView,
    AddContractLinkToServiceView,
    AddPricingLinkToServiceView,
    RemoveApplicationLinkToServiceView,
    RemoveContractLinkToServiceView,
    RemovePricingLinkToServiceView,
    RemoveServiceLinkToServiceView,
    AddApplicationLinkToServiceView,
)
from service.views.contact import (
    ContactCreateView,
    ContactDeleteView,
    ContactDetailView,
    ContactExport,
    ContactListView,
    ContactSearchView,
    ContactUpdateView,
    RemoveContactLinkToProviderView,
)
from service.views.account import (
    UserAvatarUpdateView,
    UserUpdateView,
    UserDetailView,
)

from service.views.report import ListReport

# swagger doc: https://appliku.com/post/django-rest-framework-swagger-and-typescript-api-c#swagger-documentation
# and https://pypi.org/project/drf-yasg/#installation

urlpatterns = [
    path("cost/", CostListView.as_view(), name="cost_list"),
    path("cost/my", CostDetailPerUserView.as_view(), name="my_cost"),
    path("service/", ServiceListView.as_view(), name="service_list"),
    path("service/<int:pk>", ServiceDetailView.as_view(), name="service_details"),
    path("service/create", ServiceCreateView.as_view(), name="service_creation"),
    path("service/update/<int:pk>", ServiceUpdateView.as_view(), name="service_update"),
    path(
        "service/update/<int:pk>/linkservice",
        AddServiceLinkToServiceView.as_view(),
        name="service_add_linkservice",
    ),
    path(
        "service/update/<int:pk>/removeservice/<int:lpk>",
        RemoveServiceLinkToServiceView.as_view(),
        name="service_remove_linkservice",
    ),
    path(
        "service/update/<int:pk>/linkcontract",
        AddContractLinkToServiceView.as_view(),
        name="service_add_linkcontract",
    ),
    path(
        "service/update/<int:pk>/removecontract/<int:lpk>",
        RemoveContractLinkToServiceView.as_view(),
        name="service_remove_linkcontract",
    ),
    path(
        "service/update/<int:pk>/linkpricing",
        AddPricingLinkToServiceView.as_view(),
        name="service_add_linkpricing",
    ),
    path(
        "service/update/<int:pk>/removespricing/<int:lpk>",
        RemovePricingLinkToServiceView.as_view(),
        name="service_remove_linkpricing",
    ),
    path(
        "service/update/<int:pk>/linkapplication",
        AddApplicationLinkToServiceView.as_view(),
        name="service_add_linkapplication",
    ),
    path(
        "service/update/<int:pk>/removeapplication/<int:lpk>",
        RemoveApplicationLinkToServiceView.as_view(),
        name="service_remove_linkapplication",
    ),
    path("service/delete/<int:pk>", ServiceDeleteView.as_view(), name="service_delete"),
    path("service/search/", ServiceSearchView.as_view(), name="service_search"),
    path("service/export/", ServiceExport.as_view(), name="service_export"),
    path(
        "service/<int:pk>/quickcreateprovider",
        ProviderQuickCreateView.as_view(),
        name="service_quick_create_provider",
    ),
    path("provider/", ProviderListView.as_view(), name="provider_list"),
    path("provider/create", ProviderCreateView.as_view(), name="provider_creation"),
    path("provider/<int:pk>", ProviderDetailView.as_view(), name="provider_details"),
    path(
        "provider/update/<int:pk>", ProviderUpdateView.as_view(), name="provider_update"
    ),
    path(
        "provider/delete/<int:pk>", ProviderDeleteView.as_view(), name="provider_delete"
    ),
    path("provider/search/", ProviderSearchView.as_view(), name="provider_search"),
    path("provider/export/", ProviderExport.as_view(), name="provider_export"),
    path(
        "provider/<int:pk>/removecontact/<int:lpk>",
        RemoveContactLinkToProviderView.as_view(),
        name="provider_remove_contact",
    ),
    path(
        "provider/<int:pk>/contactcreate",
        ContactCreateView.as_view(),
        name="provider_contact_creation",
    ),
    path("contract/", ContractListView.as_view(), name="contract_list"),
    path("contract/create", ContractCreateView.as_view(), name="contract_creation"),
    path("contract/<int:pk>", ContractDetailView.as_view(), name="contract_details"),
    path(
        "contract/update/<int:pk>", ContractUpdateView.as_view(), name="contract_update"
    ),
    path(
        "contract/delete/<int:pk>", ContractDeleteView.as_view(), name="contract_delete"
    ),
    path(
        "contract/update/upload/<int:pk>",
        ContractDocumentCreateView.as_view(),
        name="contract_document_creation",
    ),
    path(
        "contract/update/<int:pk>/removedocument/<int:lpk>",
        ContractDocumentRemoveView.as_view(),
        name="contract_remove_document",
    ),
    path("contract/search/", ContractSearchView.as_view(), name="contract_search"),
    path("contract/export/", ContractExport.as_view(), name="contract_export"),
    path("pricing/", PricingListView.as_view(), name="pricing_list"),
    path("pricing/create", PricingCreateView.as_view(), name="pricing_creation"),
    path("pricing/<int:pk>", PricingDetailView.as_view(), name="pricing_details"),
    path("pricing/update/<int:pk>", PricingUpdateView.as_view(), name="pricing_update"),
    path("pricing/delete/<int:pk>", PricingDeleteView.as_view(), name="pricing_delete"),
    path("pricing/search/", PricingSearchView.as_view(), name="pricing_search"),
    path("pricing/export/", PricingExport.as_view(), name="pricing_export"),
    path("contact/", ContactListView.as_view(), name="contact_list"),
    path("contact/create", ContactCreateView.as_view(), name="contact_creation"),
    path("contact/<int:pk>", ContactDetailView.as_view(), name="contact_details"),
    path("contact/update/<int:pk>", ContactUpdateView.as_view(), name="contact_update"),
    path("contact/delete/<int:pk>", ContactDeleteView.as_view(), name="contact_delete"),
    path("contact/search/", ContactSearchView.as_view(), name="contact_search"),
    path("contact/export/", ContactExport.as_view(), name="contact_export"),
    path("accounts/myprofile/", UserDetailView.as_view(), name="account_details"),
    path(
        "accounts/myprofile/update/<int:pk>",
        UserUpdateView.as_view(),
        name="account_update",
    ),
    path(
        "accounts/myprofile/update/uploadavatar/",
        UserAvatarUpdateView.as_view(),
        name="account_update_avatar",
    ),
    path("company/", CompanyListView.as_view(), name="company_list"),
    path("company/create", CompanyCreateView.as_view(), name="company_creation"),
    path("company/<int:pk>", CompanyDetailView.as_view(), name="company_details"),
    path("company/update/<int:pk>", CompanyUpdateView.as_view(), name="company_update"),
    path("company/delete/<int:pk>", CompanyDeleteView.as_view(), name="company_delete"),
    path("company/search/", CompanySearchView.as_view(), name="company_search"),
    path("company/export/", CompanyExport.as_view(), name="company_export"),
    path("report/", ListReport.as_view(), name="report_list"),
]
