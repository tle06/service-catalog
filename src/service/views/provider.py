from service.models import Provider
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from core.utils import get_pagination_configuration
from django.db.models import Q
from import_export import resources
from core.utils import ViewExport


class ProviderListView(LoginRequiredMixin, ListView):
    model = Provider
    template_name = "provider/providers.html"
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Providers",
        "create_url": "provider_creation",
        "update_url": "provider_update",
        "details_url": "provider_details",
        "delete_url": "provider_delete",
        "export_url": "provider_export",
    }


class ProviderSearchView(LoginRequiredMixin, ListView):
    model = Provider
    context_object_name = "instance_object"
    template_name = "provider/providers.html"
    extra_context = {
        "custom_title": "Providers",
        "create_url": "provider_creation",
        "update_url": "provider_update",
        "details_url": "provider_details",
        "delete_url": "provider_delete",
        "export_url": "provider_export",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Provider.objects.filter(
            Q(name__icontains=query)
            | Q(email__icontains=query)
            | Q(phone__icontains=query)
            | Q(address__icontains=query)
            | Q(country__icontains=query)
            | Q(city__icontains=query)
        )
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "provider_list", kwargs={"query": self.request.GET.get("query")}
        )


class ProviderDetailView(LoginRequiredMixin, DetailView):
    model = Provider
    context_object_name = "instance_object"
    template_name = "provider/provider.html"
    extra_context = {
        "custom_title": "Provider",
        "update_url": "provider_update",
        "delete_url": "provider_delete",
        "create_contact": "provider_contact_creation",
        "list_url": "provider_list",
    }

    def get_success_url(self):
        tab = self.request.GET.get("tab")
        path = reverse_lazy("provider_details", kwargs={"pk": self.object.pk})
        tab = self.request.GET.get("tab", "main")
        url = f"{path}?tab={tab}"
        return url


class ProviderCreateView(LoginRequiredMixin, CreateView):
    model = Provider
    template_name = "provider/create_update_provider.html"
    extra_context = {
        "form_action": "create",
        "custom_title": "Provider",
        "list_url": "provider_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "email",
        "phone",
        "vat_number",
        "street_number",
        "city",
        "zip_code",
        "country",
        "currency_preffered",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("provider_details", kwargs={"pk": self.object.pk})


class ProviderUpdateView(LoginRequiredMixin, UpdateView):
    model = Provider
    template_name = "provider/create_update_provider.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "Provider",
        "list_url": "provider_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "email",
        "phone",
        "vat_number",
        "street_number",
        "city",
        "zip_code",
        "country",
        "currency_preffered",
    ]

    def get_success_url(self):
        return reverse_lazy("provider_details", kwargs={"pk": self.object.pk})


class ProviderDeleteView(LoginRequiredMixin, DeleteView):
    model = Provider
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}

    def get_success_url(self):
        return reverse_lazy("provider_list")


class ProviderResource(resources.ModelResource):
    class Meta:
        model = Provider


class ProviderExport(PermissionRequiredMixin, ViewExport):
    filename = "provider.csv"
    content_type = "csv"
    resource = ProviderResource
    permission_required = "service.export_provider"
