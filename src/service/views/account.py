from service.models import User, Company, Department, SubDepartment
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic import UpdateView, View
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from service.forms import UploadUserAvatarFrom
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect


class UserDetailView(LoginRequiredMixin, TemplateView):
    template_name = "account/profile/profile.html"
    department = Department.objects.all()
    subdepartment = SubDepartment.objects.all()
    company = Company.objects.all()
    user = User.objects.all()
    extra_context = {
        "departments": department,
        "subdepartments": subdepartment,
        "companies": company,
        "users": user,
    }


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    fields = [
        "first_name",
        "last_name",
        "department",
        "subdepartment",
        "manager",
        "company",
        "title",
        "employeeNumber",
        "building_id",
    ]
    template_name = "account/profile/profile.html"

    def get_queryset(self):
        return User.objects.filter(
            pk=self.request.user.id,
        )

    def get_success_url(self):
        return reverse_lazy("account_details", kwargs={})


class UserAvatarUpdateView(LoginRequiredMixin, View):
    form_class = UploadUserAvatarFrom

    def post(self, request):
        form = self.form_class(request.POST, request.FILES)

        if form.is_valid():
            avatar = request.FILES["avatar"]
            user = get_object_or_404(User, pk=self.request.user.id)
            user.avatar = avatar
            user.save()

        return HttpResponseRedirect(reverse_lazy("account_details", kwargs={}))
