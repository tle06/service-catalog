from service.models import (
    Service,
    Contract,
    Pricing,
    SubDepartment,
    Application,
    Provider,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
    View,
)
from django.urls import reverse_lazy
from core.utils import get_pagination_configuration
from django.db.models import Q
from import_export import resources
from core.utils import ViewExport
from django.http import HttpResponseRedirect


class ServiceListView(LoginRequiredMixin, ListView):
    model = Service
    context_object_name = "instance_object"
    template_name = "service/services.html"
    extra_context = {
        "custom_title": "Services",
        "create_url": "service_creation",
        "update_url": "service_update",
        "details_url": "service_details",
        "delete_url": "service_delete",
        "export_url": "service_export",
        "list_url": "service_list",
    }


class ServiceSearchView(LoginRequiredMixin, ListView):
    model = Service
    context_object_name = "instance_object"
    template_name = "service/services.html"
    extra_context = {
        "custom_title": "Services",
        "create_url": "service_creation",
        "update_url": "service_update",
        "details_url": "service_details",
        "delete_url": "service_delete",
        "export_url": "service_export",
        "list_url": "service_list",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Service.objects.filter(
            Q(name__icontains=query) | Q(description__icontains=query)
        )
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "service_list", kwargs={"query": self.request.GET.get("query")}
        )


class ServiceDetailView(LoginRequiredMixin, DetailView):
    model = Service
    template_name = "service/service.html"
    services = Service.objects.all()
    contracts = Contract.objects.all()
    pricings = Pricing.objects.all()
    applications = Application.objects.all()
    context_object_name = "instance_object"
    extra_context = {
        "all_services": services,
        "all_contracts": contracts,
        "all_pricings": pricings,
        "all_applications": applications,
        "create_url": "service_creation",
        "update_url": "service_update",
        "details_url": "service_details",
        "delete_url": "service_delete",
        "export_url": "service_export",
        "list_url": "service_list",
        "custom_title": "Service",
    }

    def get_success_url(self):
        tab = self.request.GET.get("tab")
        path = reverse_lazy("service_details", kwargs={"pk": self.object.pk})
        tab = self.request.GET.get("tab", "main")
        url = f"{path}?tab={tab}"
        return url


class ServiceCreateView(LoginRequiredMixin, CreateView):
    model = Service
    template_name = "service/create_update_service.html"
    subdepartments = SubDepartment.objects.all()
    extra_context = {
        "form_action": "create",
        "custom_title": "Service",
        "subdepartments": subdepartments,
        "list_url": "service_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "description",
        "department",
        "subdepartment",
        "owner",
        "supported_by",
        "tier",
        "provider",
        "status",
        "icon",
        "identity_provider",
        "health_check",
        "company",
        "service_website",
        "category",
        "service_url",
        "support_url",
        "administrator",
        "procurement",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("service_details", kwargs={"pk": self.object.pk})


class ServiceUpdateView(LoginRequiredMixin, UpdateView):
    model = Service
    subdepartments = SubDepartment.objects.all()
    template_name = "service/create_update_service.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "Service",
        "subdepartments": subdepartments,
        "list_url": "service_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "description",
        "department",
        "subdepartment",
        "owner",
        "supported_by",
        "tier",
        "provider",
        "status",
        "icon",
        "identity_provider",
        "health_check",
        "company",
        "category",
        "service_url",
        "service_website",
        "support_url",
        "administrator",
        "procurement",
    ]

    def get_success_url(self):
        return reverse_lazy("service_details", kwargs={"pk": self.object.pk})


class ServiceDeleteView(LoginRequiredMixin, DeleteView):
    model = Service
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}
    # permission_required = ('service.delete_service')

    def get_success_url(self):
        return reverse_lazy("service_list")


# https://stackoverflow.com/questions/24008820/use-django-import-export-with-class-based-views
class ServiceResource(resources.ModelResource):
    class Meta:
        model = Service


class ServiceExport(PermissionRequiredMixin, ViewExport):
    filename = "service.csv"
    content_type = "csv"
    resource = ServiceResource
    permission_required = "service.export_service"


class ProviderQuickCreateView(PermissionRequiredMixin, View):
    permission_required = ("service.add_service", "service.add_pricing")

    def post(self, request, pk):
        service = Service.objects.filter(pk=pk).first()
        provider = Provider(
            name=service.name,
            createdByUserId=self.request.user,
            currency_preffered="EUR",
        )
        provider.save()
        service.provider = provider
        service.save()

        return HttpResponseRedirect(
            reverse_lazy("provider_details", kwargs={"pk": provider.id})
        )
