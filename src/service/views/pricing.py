from service.models import Pricing
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from core.utils import get_pagination_configuration
from django.db.models import Q
from import_export import resources
from core.utils import ViewExport
from django.contrib.auth.mixins import PermissionRequiredMixin


class PricingListView(LoginRequiredMixin, ListView):
    model = Pricing
    context_object_name = "instance_object"
    template_name = "pricing/pricings.html"
    extra_context = {
        "custom_title": "Pricings",
        "create_url": "pricing_creation",
        "update_url": "pricing_update",
        "details_url": "pricing_details",
        "delete_url": "pricing_delete",
        "export_url": "pricing_export",
    }


class PricingSearchView(LoginRequiredMixin, ListView):
    model = Pricing
    context_object_name = "instance_object"
    template_name = "pricing/pricings.html"
    extra_context = {
        "custom_title": "Pricings",
        "create_url": "pricing_creation",
        "update_url": "pricing_update",
        "details_url": "pricing_details",
        "delete_url": "pricing_delete",
        "export_url": "pricing_export",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Pricing.objects.filter(Q(name__icontains=query))
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "pricing_list", kwargs={"query": self.request.GET.get("query")}
        )


class PricingDetailView(LoginRequiredMixin, DetailView):
    model = Pricing
    context_object_name = "instance_object"
    template_name = "pricing/pricing.html"
    extra_context = {
        "custom_title": "Pricing",
        "update_url": "pricing_update",
        "delete_url": "pricing_delete",
        "list_url": "pricing_list",
    }


class PricingCreateView(LoginRequiredMixin, CreateView):
    model = Pricing
    template_name = "pricing/create_update_pricing.html"
    context_object_name = "instance_object"
    extra_context = {
        "form_action": "create",
        "custom_title": "Pricing",
        "list_url": "pricing_list",
    }
    fields = [
        "name",
        "cost_unit",
        "total_cost",
        "volume",
        "pricing_date",
        "currency",
        "time_unit",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("pricing_details", kwargs={"pk": self.object.pk})


class PricingUpdateView(LoginRequiredMixin, UpdateView):
    model = Pricing
    template_name = "pricing/create_update_pricing.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "Pricing",
        "list_url": "pricing_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "cost_unit",
        "total_cost",
        "volume",
        "pricing_date",
        "currency",
        "time_unit",
    ]

    def get_success_url(self):
        return reverse_lazy("pricing_details", kwargs={"pk": self.object.pk})


class PricingDeleteView(LoginRequiredMixin, DeleteView):
    model = Pricing
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}

    def get_success_url(self):
        return reverse_lazy("pricing_list")


class PricingResource(resources.ModelResource):
    class Meta:
        model = Pricing


class PricingExport(PermissionRequiredMixin, ViewExport):
    filename = "pricing.csv"
    content_type = "csv"
    resource = PricingResource
    permission_required = "service.export_pricing"
