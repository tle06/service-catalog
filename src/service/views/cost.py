from django.views.generic import ListView
from service.models import User
from core.utils import get_pagination_configuration
from django.contrib.auth.mixins import PermissionRequiredMixin

query_per_dep = """SELECT d.id, d.name as department_name, 
    COALESCE(SUM(p.cost_unit),0) as total_cost_department, count(DISTINCT u.id) as total_users 
    FROM service_department as d 
    RIGHT OUTER JOIN service_user as u ON u.department_id = d.id 
    LEFT OUTER JOIN service_user_application as ua ON u.id = ua.user_id 
    LEFT OUTER JOIN service_application as a ON ua.application_id = a.id 
    LEFT OUTER JOIN service_pricing as p ON a.pricing_id =p.id 
    GROUP BY d.id,d.name ORDER BY sum(p.cost_unit) DESC;"""


class CostListView(PermissionRequiredMixin, ListView):
    model = User
    paginate_by = get_pagination_configuration("user_pagination")
    template_name = "cost/costs.html"
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Cost",
        "create_url": "cost_creation",
        "update_url": "cost_update",
        "details_url": "cost_details",
        "delete_url": "cost_delete",
        "export_url": "cost_export",
    }
    permission_required = "service.list_cost_per_dep"

    def get_queryset(self):
        queryset = User.objects.raw(query_per_dep)

        return queryset


class CostDetailPerUserView(PermissionRequiredMixin, ListView):
    model = User
    template_name = "cost/cost_current_user.html"
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Cost",
        "create_url": "cost_creation",
        "update_url": "cost_update",
        "details_url": "cost_details",
        "delete_url": "cost_delete",
        "export_url": "cost_export",
    }
    permission_required = "service.list_cost_per_dep"

    def get_queryset(self):
        user = User.objects.filter(pk=self.request.user.id).first()
        apps = user.application.all()

        return apps
