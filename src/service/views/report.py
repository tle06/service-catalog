from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic.base import TemplateView
from service.storages import AWSPrivateMediaStorage
import os
from datetime import datetime


class ListReport(PermissionRequiredMixin, TemplateView):
    template_name = "report/reports.html"
    folder_name = "reports/"
    permission_required = "service.view_report"
    extra_context = {
        "create_url": "service_creation",
        "update_url": "service_update",
        "details_url": "service_details",
        "delete_url": "service_delete",
        "export_url": "service_export",
        "list_url": "service_list",
        "custom_title": "Report",
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Initialize the storage class
        storage = AWSPrivateMediaStorage()

        # Construct the folder path
        folder_path = f"{storage.location}/{self.folder_name}".strip("/")

        # Use the boto3 client to list objects in the specific folder
        s3_client = storage.connection.meta.client
        bucket_name = storage.bucket_name

        # List objects in the specified folder
        response = s3_client.list_objects_v2(Bucket=bucket_name, Prefix=folder_path)
        files = response.get("Contents", [])

        # Generate presigned URLs for each file
        file_list = []
        for file in files:
            print(file)
            if not file["Key"].endswith("/"):
                file_url = s3_client.generate_presigned_url(
                    "get_object",
                    Params={"Bucket": bucket_name, "Key": file["Key"]},
                    ExpiresIn=3600,  # URL expiration time in seconds
                )
                file_list.append(
                    {
                        "name": os.path.basename(file["Key"]),
                        "url": file_url,
                        "last_modified": file["LastModified"],
                        "size": round((file["Size"] / (1024 * 1024)), 2),
                    }
                )

        file_list.sort(key=lambda x: x["last_modified"], reverse=True)
        # Add the file list and folder name to the context
        context["file_list"] = file_list
        context["folder_name"] = self.folder_name
        return context
