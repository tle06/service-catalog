from django.views.generic import View
from django.urls import reverse_lazy
from service.models import Service
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404


class AddServiceLinkToServiceView(View):
    def post(self, request, pk):
        object_relation_id = request.POST.get("relation")
        current_object = get_object_or_404(Service, pk=pk)
        current_object.service_relationships.add(object_relation_id)
        path = reverse_lazy("service_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "service")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class RemoveServiceLinkToServiceView(View):
    def post(self, request, pk, lpk):
        request.POST.get("relation")
        current_object = get_object_or_404(Service, pk=pk)
        current_object.service_relationships.remove(lpk)
        path = reverse_lazy("service_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "service")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class AddContractLinkToServiceView(View):
    def post(self, request, pk):
        object_relation_id = request.POST.get("relation")
        current_object = get_object_or_404(Service, pk=pk)
        current_object.contract.add(object_relation_id)
        path = reverse_lazy("service_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "contract")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class RemoveContractLinkToServiceView(View):
    def post(self, request, pk, lpk):
        request.POST.get("relation")
        current_object = get_object_or_404(Service, pk=pk)
        current_object.contract.remove(lpk)
        path = reverse_lazy("service_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "contract")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class AddPricingLinkToServiceView(View):
    def post(self, request, pk):
        object_relation_id = request.POST.get("relation")
        current_object = get_object_or_404(Service, pk=pk)
        current_object.pricing.add(object_relation_id)
        path = reverse_lazy("service_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "pricing")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class RemovePricingLinkToServiceView(View):
    def post(self, request, pk, lpk):
        request.POST.get("relation")
        current_object = get_object_or_404(Service, pk=pk)
        current_object.pricing.remove(lpk)
        path = reverse_lazy("service_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "pricing")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class AddApplicationLinkToServiceView(View):
    def post(self, request, pk):
        object_relation_id = request.POST.get("relation")
        current_object = get_object_or_404(Service, pk=pk)
        current_object.application.add(object_relation_id)
        path = reverse_lazy("service_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "application")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class RemoveApplicationLinkToServiceView(View):
    def post(self, request, pk, lpk):
        request.POST.get("relation")
        current_object = get_object_or_404(Service, pk=pk)
        current_object.application.remove(lpk)
        path = reverse_lazy("service_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "application")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)
