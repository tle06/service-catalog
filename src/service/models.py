from django.db import models
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser
from django.utils.translation import gettext_lazy as _
from service.manager import UserManager
from django.conf import settings
from service.storages import AWSPrivateMediaStorage
from django.core.files.storage import default_storage
import os
from django.core.mail import send_mail


class Configuration(models.Model):
    CATEGORY = (
        ("public", "Public"),
        ("social_provider", "Social provider"),
        ("private", "private"),
    )
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=255, blank=True, null=True)
    category = models.CharField(
        max_length=30, choices=CATEGORY, default="private", editable=True
    )
    key = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    enabled = models.BooleanField(default=False, verbose_name=_("Enabled"))

    class Meta:
        verbose_name = _("configuration")
        verbose_name_plural = _("configurations")
        ordering = ["id"]
        permissions = [
            ("export_configuration", "Can export configuration"),
        ]

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_department_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("department")
        verbose_name_plural = _("departments")
        ordering = ["id"]
        permissions = [
            ("export_department", "Can export department"),
        ]

    def __str__(self):
        return self.name


class SubDepartment(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    department = models.ForeignKey(
        Department,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=False,
    )
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_subdepartment_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("sub-department")
        verbose_name_plural = _("sub-departments")
        ordering = ["id"]
        permissions = [
            ("export_subdepartment", "Can export sub-department"),
        ]

    def __str__(self):
        return self.name


class Title(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_title_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("title")
        verbose_name_plural = _("titles")
        ordering = ["id"]
        permissions = [
            ("export_title", "Can export title"),
        ]

    def __str__(self):
        return self.name


class ServiceCategory(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_service_category_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("service category")
        verbose_name_plural = _("service categories")
        ordering = ["id"]
        permissions = [
            ("export_service_catagory", "Can export service catagory"),
        ]

    def __str__(self):
        return self.name


class IdentityProvider(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    icon_url = models.CharField(max_length=255, blank=True, null=True)
    service_url = models.CharField(max_length=255, blank=True, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_identity_provider_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("identity provider")
        verbose_name_plural = _("identity providers")
        ordering = ["id"]
        permissions = [
            ("export_identity_provider", "Can export identity provider"),
        ]

    def __str__(self):
        return self.name


class Contact(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    email = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    firstname = models.CharField(max_length=255, blank=True, null=True)
    lastname = models.CharField(max_length=255, blank=True, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_contact_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("contact")
        verbose_name_plural = _("contacts")
        ordering = ["id"]
        permissions = [
            ("export_contact", "Can export contact"),
        ]

    def __str__(self):
        return self.name


class Provider(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    vat_number = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    street_number = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    zip_code = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)
    currency_preffered = models.CharField(max_length=3, blank=True, null=True)
    contact = models.ManyToManyField(Contact, blank=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_provider_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("provider")
        verbose_name_plural = _("providers")
        ordering = ["id"]
        permissions = [
            ("export_provider", "Can export provider"),
        ]

    def __str__(self):
        return self.name


class Company(models.Model):
    TRADE_REGISTER = (
        ("public", "Public"),
        ("restricted_access", "Restricted Access"),
        (
            "restricted_access_registry",
            "Restricted Access (business registry except required to requested)",
        ),
    )

    name = models.CharField(max_length=255, blank=True, null=True)
    vat_number = models.CharField(max_length=255, blank=True, null=True)
    invoice_email = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    street_number = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    zip_code = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)
    brand_name = models.CharField(max_length=255, blank=True, null=True)
    activity = models.CharField(max_length=255, blank=True, null=True)
    incorporation_date = models.DateTimeField(blank=True, null=True)
    share_capital = models.FloatField(blank=True, null=True)
    tax_id = models.CharField(max_length=255, blank=True, null=True)
    trade_register = models.CharField(
        max_length=255, choices=TRADE_REGISTER, editable=True, null=True, blank=True
    )
    shareholders = models.CharField(max_length=255, blank=True, null=True)

    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_company_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("company")
        verbose_name_plural = _("companies")
        ordering = ["id"]
        permissions = [
            ("export_company", "Can export company"),
        ]

    def __str__(self):
        return self.name


class ContractDocument(models.Model):
    default_upload_folder = "contract"

    def set_upload_folder(self, folder_name):
        self._upload_folder = folder_name

    def get_upload_folder(self):
        return getattr(self, "_upload_folder", self.default_upload_folder)

    def get_upload_path(instance, filename):
        name, ext = os.path.splitext(filename)
        return os.path.join(instance.get_upload_folder(), str(instance.lpk), name + ext)

    def get_storage():
        storage = default_storage
        if settings.USE_AWS_S3_FOR_FILES:
            storage = AWSPrivateMediaStorage()
        return storage

    name = models.CharField(max_length=255, blank=False, null=False)
    file = models.FileField(
        upload_to=get_upload_path, null=True, blank=True, storage=get_storage()
    )
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_contrat_document_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("contract document")
        verbose_name_plural = _("contracts documents")
        ordering = ["id"]
        permissions = [
            ("export_contract_document", "Can export contract document"),
        ]

    def __str__(self):
        return self.name


class Contract(models.Model):
    DURATION_UNIT = (
        ("year", "Year"),
        ("month", "Month"),
        ("day", "day"),
    )

    STATUS = (
        ("active", "Active"),
        ("suspended", "Suspended"),
        ("terminated", "Terminated"),
    )

    name = models.CharField(max_length=255, blank=True, null=True)
    provider = models.ForeignKey(
        Provider, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )
    start_date = models.DateTimeField(
        auto_now_add=False, auto_now=False, blank=True, null=True
    )
    end_date = models.DateTimeField(
        auto_now_add=False, auto_now=False, blank=True, null=True
    )
    renewal_date = models.DateTimeField(
        auto_now_add=False, auto_now=False, blank=True, null=True
    )
    duration = models.FloatField(blank=True, null=True)
    duration_unit = models.CharField(
        max_length=30, choices=DURATION_UNIT, default="month"
    )
    total_cost = models.FloatField(blank=True, null=True)
    cost_unit = models.FloatField(blank=True, null=True)
    volume = models.FloatField(blank=True, null=True)
    currency = models.CharField(max_length=3, blank=True, null=True, default="EUR")
    status = models.CharField(max_length=30, choices=STATUS, default="active")
    company = models.ForeignKey(
        Company, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )
    document = models.ManyToManyField(ContractDocument, blank=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_contrat_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("contract")
        verbose_name_plural = _("contracts")
        ordering = ["id"]
        permissions = [
            ("export_contract", "Can export contract"),
        ]

    def __str__(self):
        return self.name


class Pricing(models.Model):
    STATUS = (
        ("active", "Active"),
        ("suspended", "Suspended"),
        ("terminated", "Terminated"),
    )

    DURATION_UNIT = (
        ("year", "Year"),
        ("month", "Month"),
        ("day", "day"),
    )

    name = models.CharField(max_length=255, blank=True, null=True)
    cost_unit = models.FloatField(blank=True, null=True)
    time_unit = models.CharField(max_length=30, choices=DURATION_UNIT, default="month")
    total_cost = models.FloatField(blank=True, null=True)
    volume = models.FloatField(blank=True, null=True)
    pricing_date = models.DateTimeField(
        auto_now_add=False, auto_now=False, blank=True, null=True
    )
    currency = models.CharField(max_length=3, blank=True, null=True, default="EUR")
    status = models.CharField(max_length=30, choices=STATUS, default="active")
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_pricing_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("pricing")
        verbose_name_plural = _("pricings")
        ordering = ["id"]
        permissions = [
            ("export_pricing", "Can export pricing"),
        ]

    def __str__(self):
        return self.name


class Application(models.Model):
    from building.models import Country

    STATUS = (
        ("active", "Active"),
        ("suspended", "Suspended"),
        ("deleted", "Deleted"),
    )

    SSO_TYPE = (
        ("saml1", "SAML1.0"),
        ("saml2", "SAML2.0"),
        ("oauth", "OAuth"),
        ("oidc", "OpenID Connect"),
        ("form-base", "Form-based auth"),
        ("ws-federation", "WS-Federation with SAML 1.1"),
    )

    name = models.CharField(max_length=1000, blank=False, null=True)
    app_id = models.IntegerField(null=True, blank=True)
    icon_url = models.CharField(max_length=255, blank=True, null=True)
    pricing = models.ForeignKey(
        Pricing, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )
    idp = models.ForeignKey(
        IdentityProvider,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=False,
    )
    data_location = models.ForeignKey(
        Country,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="FK_country_data_location_application",
    )
    provider_location = models.ForeignKey(
        Country,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="FK_country_provider_location_application",
    )

    owner = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_application_owners_user_id",
    )
    administrator = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_application_administrators_user_id",
    )

    status = models.CharField(max_length=30, choices=STATUS, default="active")
    sso_type = models.CharField(
        max_length=30, choices=SSO_TYPE, default="saml2", null=True, blank=True
    )
    sso_manage_by_provider = models.BooleanField(default=False)
    provider = models.ForeignKey(
        Provider, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )

    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_application_created_by_user_id",
    )

    class Meta:
        verbose_name = _("application")
        verbose_name_plural = _("applications")
        ordering = ["id"]
        permissions = [
            ("export_application", "Can export application"),
        ]

    def __str__(self):
        return self.name

    def get_service_linked(self):
        return Service.objects.filter(application=self).all()


class Service(models.Model):
    TIERS = (
        ("tier_1", "Tier 1"),
        ("tier_2", "Tier 2"),
        ("tier_3", "Tier 3"),
        ("tier_4", "Tier 4"),
    )

    STATUS = (
        ("active", "Active"),
        ("suspended", "Suspended"),
        ("deleted", "Deleted"),
    )

    def get_upload_path(instance, filename):
        name, ext = os.path.splitext(filename)
        return os.path.join("services", str(instance.id), name + ext)

    def get_storage():
        storage = default_storage
        if settings.USE_AWS_S3_FOR_FILES:
            storage = AWSPrivateMediaStorage()
        return storage

    name = models.CharField(max_length=255, blank=False, null=True)
    description = models.CharField(max_length=255, blank=False, null=False)
    department = models.ForeignKey(
        Department, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )
    subdepartment = models.ForeignKey(
        SubDepartment,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=False,
    )

    owner = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_service_owners_user_id",
    )
    supported_by = models.CharField(max_length=255, blank=True, null=True)
    service_relationships = models.ManyToManyField("self", blank=True)
    tier = models.CharField(max_length=30, choices=TIERS, default="tier_1")
    provider = models.ForeignKey(
        Provider, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )
    status = models.CharField(max_length=30, choices=STATUS, default="active")
    contract = models.ManyToManyField(Contract, blank=True)
    health_check = models.CharField(max_length=255, blank=True, null=True)
    icon_url = models.CharField(max_length=255, blank=True, null=True)
    icon = models.ImageField(
        upload_to=get_upload_path, null=True, blank=True, storage=get_storage()
    )
    pricing = models.ManyToManyField(Pricing, blank=True)
    company = models.ForeignKey(
        Company, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )
    identity_provider = models.ForeignKey(
        IdentityProvider,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=False,
    )
    service_url = models.CharField(max_length=255, blank=True, null=True)
    service_website = models.CharField(max_length=255, blank=True, null=True)
    application = models.ManyToManyField(Application, blank=True)
    support_url = models.CharField(max_length=255, blank=True, null=True)
    category = models.ForeignKey(
        ServiceCategory,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=False,
    )

    administrator = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_service_administrator_user_id",
    )

    procurement = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_service_procurement_user_id",
    )

    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_service_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("service")
        verbose_name_plural = _("services")
        ordering = ["id"]
        permissions = [
            ("export_service", "Can export service"),
        ]

    def __str__(self):
        return self.name


# https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#abstractbaseuser
class User(AbstractBaseUser, PermissionsMixin):
    from building.models import Building
    from hardware.models import HardwareMapping

    @property
    def scim_groups(self):
        return self.groups

    def get_upload_path(instance, filename):
        name, ext = os.path.splitext(filename)
        return os.path.join("profiles", str(instance.id), name + ext)

    def get_storage():
        storage = default_storage
        if settings.USE_AWS_S3_FOR_FILES:
            storage = AWSPrivateMediaStorage()
        return storage

    email = models.EmailField(_("email address"), unique=True)
    first_name = models.CharField(_("first name"), max_length=30, blank=True)
    last_name = models.CharField(_("last name"), max_length=30, blank=True)
    date_joined = models.DateTimeField(_("date joined"), auto_now_add=True)
    is_active = models.BooleanField(_("active"), default=True)
    is_staff = models.BooleanField(_("staff status"), default=False)
    is_superuser = models.BooleanField(_("admin status"), default=False)
    avatar = models.ImageField(
        upload_to=get_upload_path, null=True, blank=True, storage=get_storage()
    )
    department = models.ForeignKey(
        Department, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )
    subdepartment = models.ForeignKey(
        SubDepartment,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=False,
    )
    manager = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=False,
    )
    company = models.ForeignKey(
        Company, on_delete=models.SET_NULL, blank=True, null=True, auto_created=False
    )
    employeeNumber = models.CharField(max_length=255, blank=True, null=True)
    title = models.ForeignKey(
        Title, blank=True, null=True, auto_created=False, on_delete=models.SET_NULL
    )
    application = models.ManyToManyField(Application, blank=True)
    service = models.ManyToManyField(Service, blank=True)
    building_id = models.ForeignKey(
        Building, blank=True, null=True, on_delete=models.SET_NULL
    )

    hardware_mapping = models.ForeignKey(
        HardwareMapping, on_delete=models.SET_NULL, blank=True, null=True
    )
    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
        ordering = ["id"]
        permissions = [
            ("export_user", "Can export user"),
            ("list_cost_per_dep", "Can list cost per department"),
            ("view_report", "Can view report"),
        ]

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_manager_full_name(self):
        """
        Returns the first_name plus the last_name,
        with a space in between for the manager.
        """

        if self.manager:
            full_name = "%s %s" % (self.manager.first_name, self.manager.last_name)
            return full_name.strip()
        return None

    def get_manager_id(self):
        if self.manager:
            return self.manager.id
        return None

    def get_department_name(self):
        if self.department:
            return self.department.name
        return None

    def get_subdepartment_name(self):
        if self.subdepartment:
            return self.subdepartment.name
        return None

    def get_company_name(self):
        if self.company:
            return self.company.name
        return None

    def get_url(self):
        return ""

    def get_building_name(self):
        if self.building_id:
            return self.building_id.name
        return None
