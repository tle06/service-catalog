from mozilla_django_oidc.auth import OIDCAuthenticationBackend


class CustomOIDCAB(OIDCAuthenticationBackend):
    def create_user(self, claims):
        """Return object for a newly created user account."""
        email = claims.get("email")
        self.get_username(claims)
        return self.UserModel.objects.create_user(email=email)
