from django import template
from service.models import (
    Configuration,
    Service,
    Provider,
    Pricing,
    Contract,
    Contact,
    User,
    Application,
    Company,
)
from django.conf import settings
from django.db.models import Sum, Count
from django.db import connection
from core.utils import dictfetchall

register = template.Library()


@register.simple_tag
def get_social_providers_config(key: str, category="social_provider"):
    return Configuration.objects.filter(
        category=category, key=key, enabled=True
    ).first()


@register.simple_tag
def get_social_provider_actived():
    category = "social_provider"
    configuration = (
        Configuration.objects.filter(category=category, enabled=True)
        .exclude(key="oidc")
        .first()
    )
    result = False
    if configuration:
        result = True
    return result


# https://pythoncircle.com/post/701/how-to-set-a-variable-in-django-template/


@register.simple_tag
def setvar(val=None):
    return val


@register.simple_tag
def get_public_configurations(key: str, category="public"):
    return Configuration.objects.filter(
        category=category, key=key, enabled=True
    ).first()


@register.simple_tag
def get_app_version():
    return settings.APP_VERSION


@register.simple_tag
def get_oidc_activated():
    return settings.OIDC_ACTIVATED


@register.simple_tag
def get_service_cost_by_status(id: int, status: str = "active"):
    costs = (
        Service.objects.filter(pk=id, contract__status=status)
        .values("contract__duration_unit", "contract__currency")
        .annotate(total=Sum("contract__total_cost"))
        .order_by()
    )
    return costs


@register.simple_tag
def get_total_services():
    objects = Service.objects.all()
    return objects.count()


@register.simple_tag
def get_total_services_by_status(status: str):
    objects = Service.objects.filter(status=status).all()
    return objects.count()


@register.simple_tag
def get_service_cost_by_department():
    raw_query = """SELECT d.name AS 'department_name', count(DISTINCT s.id) AS 'total_services',
            sum(c.total_cost) AS 'total_cost', c.duration_unit AS 'duration_unit',
            c.currency AS 'currency',COUNT(c.id) AS 'count_contract' FROM service_service AS s
            LEFT OUTER JOIN service_department AS d ON d.id=s.department_id 
            LEFT OUTER JOIN service_service_contract AS sc ON sc.service_id=s.id 
            LEFT OUTER JOIN(SELECT * FROM service_contract
            WHERE status NOT LIKE 'terminated') AS c ON c.id=sc.contract_id 
            WHERE s.status LIKE 'active' 
            GROUP BY s.department_id, c.duration_unit, c.currency 
            ORDER BY sum(c.total_cost) DESC;"""
    cursor = connection.cursor()
    cursor.execute(raw_query)
    objects = dictfetchall(cursor)
    return objects


@register.simple_tag
def get_service_from_owner(user: int):
    objects = Service.objects.filter(owner__id=user.id).all()
    return objects.count()


@register.simple_tag
def get_service_per_idp():
    objects = (
        Service.objects.filter(identity_provider__isnull=False)
        .values("identity_provider__name", "identity_provider__icon_url")
        .annotate(count=Count("pk"))
        .order_by()
    )
    return objects


@register.simple_tag
def get_total_providers():
    objects = Provider.objects.all()
    return objects.count()


@register.simple_tag
def get_total_contracts():
    objects = Contract.objects.all()
    return objects.count()


@register.simple_tag
def get_total_pricings():
    objects = Pricing.objects.all()
    return objects.count()


@register.simple_tag
def get_total_application():
    objects = Application.objects.all()
    return objects.count()


@register.simple_tag
def get_total_contacts():
    objects = Contact.objects.all()
    return objects.count()


@register.simple_tag
def get_service_without_provider():
    objects = Service.objects.filter(provider__isnull=True).all()
    return objects.count()


@register.simple_tag
def get_service_without_contract():
    objects = Service.objects.filter(contract__isnull=True).all()
    return objects.count()


@register.simple_tag
def get_service_without_pricing():
    objects = Service.objects.filter(pricing__isnull=True).all()
    return objects.count()


@register.simple_tag
def get_service_without_owner():
    objects = Service.objects.filter(owner__isnull=True).all()
    return objects.count()


@register.simple_tag
def get_ratio(dividend: int, divisor: int, rounding: int = 1):
    if divisor != 0:
        return round((dividend / divisor) * 100, rounding)
    return 0


@register.simple_tag
def get_app_from_user(user_id: int):
    user = User.objects.filter(pk=user_id).first()
    apps = user.application.all()
    return apps


@register.simple_tag
def get_cost_user_from_department(department_id: int):
    if department_id:
        where_query = f"d.id = {department_id}"
    else:
        where_query = "u.department_id is null"
    raw_query = f"""SELECT d.name as department_name, d.id as department_id,
        u.email,u.id,
        COALESCE(SUM(p.cost_unit),0) as total_cost_user,count(DISTINCT ua.id) as count_app_user 
        FROM service_user as u 
        LEFT OUTER JOIN service_user_application as ua ON u.id = ua.user_id 
        LEFT OUTER JOIN service_application as a ON ua.application_id = a.id 
        LEFT OUTER JOIN service_pricing as p ON a.pricing_id =p.id 
        LEFT OUTER JOIN service_department as d ON u.department_id = d.id 
        WHERE {where_query} 
        GROUP BY d.id,d.name,u.id,u.email 
        ORDER BY sum(p.cost_unit) DESC;"""
    users = User.objects.raw(raw_query)
    return users


@register.filter()
def to_int(value):
    return int(value)


@register.simple_tag
def get_total_cost_user(user_id: int):
    raw_query = f"""SELECT u.email,u.id, COALESCE(SUM(p.cost_unit),0) as total_cost_user,count(DISTINCT ua.id) as count_app_user 
        FROM service_user as u 
        LEFT OUTER JOIN service_user_application as ua ON u.id = ua.user_id 
        LEFT OUTER JOIN service_application as a ON ua.application_id = a.id 
        LEFT OUTER JOIN service_pricing as p ON a.pricing_id =p.id
        WHERE u.id ={user_id}
        GROUP BY u.id,u.email;"""
    user_cost = User.objects.raw(raw_query)

    return user_cost[0]


@register.simple_tag
def get_total_company():
    return Company.objects.all().count()
