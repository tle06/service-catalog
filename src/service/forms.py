from django import forms
from service.models import User, ContractDocument


class UploadContractDocumentFrom(forms.ModelForm):
    class Meta:
        model = ContractDocument
        fields = ["file"]


class UploadUserAvatarFrom(forms.ModelForm):
    class Meta:
        model = User
        fields = ["avatar"]
