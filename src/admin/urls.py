from django.contrib import admin
from django.urls import path, re_path as url
from admin.autocomplete import (
    ManagerAutocomplete,
    HardwareMappingAutocomplete,
    ApplicationMappingAutocomplete,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    url(
        r"^manager-autocomplete/$",
        ManagerAutocomplete.as_view(),
        name="manager-autocomplete",
    ),
    url(
        r"^hardware-mapping-autocomplete/$",
        HardwareMappingAutocomplete.as_view(),
        name="hardware-mapping-autocomplete",
    ),
    url(
        r"^application-autocomplete/$",
        ApplicationMappingAutocomplete.as_view(),
        name="application-autocomplete",
    ),
]
