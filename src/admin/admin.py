from django.contrib import admin
from service.models import (
    Company,
    Configuration,
    Contact,
    Contract,
    ContractDocument,
    Department,
    IdentityProvider,
    Pricing,
    Provider,
    Service,
    ServiceCategory,
    SubDepartment,
    User,
    Title,
)
from building.models import (
    Building,
    AccessControlRecord,
    Controller,
    BuildingSize,
    BuildingType,
    Currency,
    PublicIP,
    ExternalPresenceLogRecord,
    Country,
)
from django.contrib.auth.admin import UserAdmin
from admin.forms import UserChangeForm, UserCreationForm, GroupAdminForm
from django.contrib.auth.models import Permission
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from django.contrib.auth.models import Group


class ConfigurationResource(resources.ModelResource):
    class Meta:
        model = Configuration


class ConfigurationAdmin(ImportExportModelAdmin):
    resource_class = ConfigurationResource

    search_fields = ("name",)
    list_display = ("name", "category", "key", "value", "enabled")
    list_filter = ()
    filter_horizontal = ()


class UserResource(resources.ModelResource):
    class Meta:
        model = User
        exclude = ("password",)


class UserAdmin(ImportExportModelAdmin, UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    resource_class = UserResource

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = (
        "email",
        "is_active",
        "is_superuser",
        "is_staff",
        "date_joined",
        "department",
        "subdepartment",
    )
    list_filter = (
        "is_superuser",
        "is_staff",
        "department",
        "subdepartment",
        "is_active",
        "company",
        "groups",
        "building_id",
    )
    fieldsets = (
        ("Login", {"fields": ("email", "password")}),
        (
            "Personal info",
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "department",
                    "subdepartment",
                    "manager",
                    "company",
                    "title",
                    "building_id",
                    "employeeNumber",
                    "avatar",
                    "application",
                    "hardware_mapping",
                )
            },
        ),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_superuser",
                    "is_staff",
                    "groups",
                    "user_permissions",
                )
            },
        ),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (
            "Login",
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "password1",
                    "password2",
                ),
            },
        ),
        (
            "Personal info",
            {
                "classes": ("wide",),
                "fields": (
                    "first_name",
                    "last_name",
                    "department",
                    "subdepartment",
                    "manager",
                    "company",
                    "title",
                    "building_id",
                    "employeeNumber",
                    "avatar",
                    "application",
                    "hardware_mapping",
                ),
            },
        ),
        (
            "Permissions",
            {"classes": ("wide",), "fields": ("is_active", "is_superuser", "is_staff")},
        ),
    )
    search_fields = ("email", "first_name", "last_name")
    ordering = (
        "email",
        "date_joined",
    )
    filter_horizontal = ()


class ServiceCategoryResource(resources.ModelResource):
    class Meta:
        model = ServiceCategory


class ServiceCategoryAdmin(ImportExportModelAdmin):
    resource_class = ServiceCategoryResource

    search_fields = ("name",)
    list_display = ("name", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class CompanyResource(resources.ModelResource):
    class Meta:
        model = Company


class CompanyAdmin(ImportExportModelAdmin):
    resource_class = CompanyResource

    search_fields = ("name", "id", "vat_number")
    list_display = ("id", "name", "vat_number", "invoice_email", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class ContractResource(resources.ModelResource):
    class Meta:
        model = Contract


class ContractAdmin(ImportExportModelAdmin):
    resource_class = ContractResource

    search_fields = ("name",)
    list_display = ("name", "status", "provider", "createdByUserId")
    list_filter = ("provider",)
    filter_horizontal = ()


class ContractDocumentResource(resources.ModelResource):
    class Meta:
        model = ContractDocument


class ContractDocumentAdmin(ImportExportModelAdmin):
    resource_class = ContractDocumentResource

    search_fields = ("name",)
    list_display = ("name", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class DepartmentResource(resources.ModelResource):
    class Meta:
        model = Department


class DepartmentAdmin(ImportExportModelAdmin):
    resource_class = DepartmentResource
    search_fields = ("name",)
    list_display = ("name", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class IdentityProviderResource(resources.ModelResource):
    class Meta:
        model = IdentityProvider


class IdentityProviderAdmin(ImportExportModelAdmin):
    resource_class = IdentityProviderResource

    search_fields = ("name",)
    list_display = ("name", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class ProviderResource(resources.ModelResource):
    class Meta:
        model = Provider
        exclude = ("contact",)


class ProviderAdmin(ImportExportModelAdmin):
    resource_class = ProviderResource

    search_fields = ("name",)
    list_display = ("name", "email", "country")
    list_filter = ("country",)
    filter_horizontal = ()


class PricingResource(resources.ModelResource):
    class Meta:
        model = Pricing


class PricingAdmin(ImportExportModelAdmin):
    resource_class = PricingResource

    search_fields = ("name",)
    list_display = ("name", "status", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class ServiceResource(resources.ModelResource):
    class Meta:
        model = Service
        exclude = ("service_relationships", "contract", "icon_url", "icon", "pricing")


class ServiceAdmin(ImportExportModelAdmin):
    resource_class = ServiceResource

    fieldsets = (
        ("Main", {"fields": ("name", "category", "provider", "status", "icon")}),
        ("Description", {"fields": ("description",)}),
        (
            "Ownership",
            {
                "fields": (
                    "department",
                    "subdepartment",
                    "owner",
                    "company",
                    "administrator",
                    "procurement",
                )
            },
        ),
        (
            "Other",
            {
                "fields": (
                    "tier",
                    "identity_provider",
                    "health_check",
                    "createdByUserId",
                )
            },
        ),
        ("URL", {"fields": ("service_url", "service_website")}),
        ("Support", {"fields": ("support_url", "supported_by")}),
        ("Service Relation", {"fields": ("service_relationships",)}),
        ("Contract", {"fields": ("contract",)}),
        ("Pricing", {"fields": ("pricing",)}),
        ("Application", {"fields": ("application",)}),
    )

    search_fields = ("name",)
    list_display = (
        "id",
        "name",
        "provider",
        "department",
        "subdepartment",
        "supported_by",
    )
    list_filter = ("provider", "supported_by")
    filter_horizontal = ()


class SubDepartmentResource(resources.ModelResource):
    class Meta:
        model = SubDepartment


class SubDepartmentAdmin(ImportExportModelAdmin):
    resource_class = SubDepartmentResource

    search_fields = ("name",)
    list_display = ("name", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class PermissionResource(resources.ModelResource):
    class Meta:
        model = Permission


class PermissionAdmin(ImportExportModelAdmin):
    resource_class = PermissionResource

    search_fields = ("name",)
    list_display = ("name",)
    list_filter = ()
    filter_horizontal = ()


class ContactResource(resources.ModelResource):
    class Meta:
        model = Contact


class ContactAdmin(ImportExportModelAdmin):
    resource_class = ContactResource

    search_fields = ("name", "email")
    list_display = ("name", "email", "firstname", "lastname", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class TitleResource(resources.ModelResource):
    class Meta:
        model = Title


class TitleAdmin(ImportExportModelAdmin):
    resource_class = TitleResource

    search_fields = ("name",)
    list_display = ("name",)
    list_filter = ()
    filter_horizontal = ()


class BuildingResource(resources.ModelResource):
    class Meta:
        model = Building


class BuildingAdmin(ImportExportModelAdmin):
    resource_class = BuildingResource

    search_fields = ("name",)
    list_display = ("name", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class AccessControlRecordResource(resources.ModelResource):
    class Meta:
        model = AccessControlRecord


class AccessControlRecordAdmin(ImportExportModelAdmin):
    resource_class = AccessControlRecordResource

    search_fields = ("building", "user")
    list_display = (
        "id",
        "user",
        "building",
        "date",
        "user_type",
        "card",
    )
    list_filter = ()
    filter_horizontal = ()


class ControllerResource(resources.ModelResource):
    class Meta:
        model = Controller


class ControllerAdmin(ImportExportModelAdmin):
    resource_class = ControllerResource

    search_fields = ("name", "extrnal_id")
    list_display = ("id", "name", "type", "external_id")
    list_filter = ()
    filter_horizontal = ()


class GroupResource(resources.ModelResource):
    class Meta:
        model = Group


class GroupAdmin(ImportExportModelAdmin):
    # Use our custom form.
    form = GroupAdminForm
    # Filter permissions horizontal as well.
    filter_horizontal = ["permissions"]
    resource_class = GroupResource
    list_display = ("id", "name")
    search_fields = ("name",)
    list_filter = ()
    filter_horizontal = ()


class BuildingTypeResource(resources.ModelResource):
    class Meta:
        model = BuildingType


class BuildingTypeAdmin(ImportExportModelAdmin):
    resource_class = BuildingTypeResource

    search_fields = ("name",)
    list_display = ("id", "name")
    list_filter = ()
    filter_horizontal = ()


class BuildingSizeResource(resources.ModelResource):
    class Meta:
        model = BuildingSize


class BuildingSizeAdmin(ImportExportModelAdmin):
    resource_class = BuildingSizeResource

    search_fields = ("name",)
    list_display = ("id", "name")
    list_filter = ()
    filter_horizontal = ()


class CurrencyResource(resources.ModelResource):
    class Meta:
        model = Currency


class CurrencyAdmin(ImportExportModelAdmin):
    resource_class = CurrencyResource

    search_fields = ("name",)
    list_display = ("id", "name")
    list_filter = ()
    filter_horizontal = ()


class PublicIPResource(resources.ModelResource):
    class Meta:
        model = PublicIP


class PublicIPAdmin(ImportExportModelAdmin):
    resource_class = PublicIPResource

    search_fields = ("ip", "building")
    list_display = ("id", "ip", "building")
    list_filter = ()
    filter_horizontal = ()


class ExternalPresenceLogRecordResource(resources.ModelResource):
    class Meta:
        model = ExternalPresenceLogRecord


class ExternalPresenceLogRecordAdmin(ImportExportModelAdmin):
    resource_class = ExternalPresenceLogRecordResource

    search_fields = ("ip",)
    list_display = ("id", "source_name", "building_id")
    list_filter = ()
    filter_horizontal = ()


class CountryResource(resources.ModelResource):
    class Meta:
        model = Country


class CountryAdmin(ImportExportModelAdmin):
    resource_class = CountryResource

    search_fields = ("ip", "name")
    list_display = ("id", "name")
    list_filter = ()
    filter_horizontal = ()


admin.site.unregister(Group)
admin.site.register(Configuration, ConfigurationAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(ServiceCategory, ServiceCategoryAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Contract, ContractAdmin)
admin.site.register(ContractDocument, ContractDocumentAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(IdentityProvider, IdentityProviderAdmin)
admin.site.register(Provider, ProviderAdmin)
admin.site.register(Pricing, PricingAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(SubDepartment, SubDepartmentAdmin)
admin.site.register(Permission, PermissionAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Title, TitleAdmin)
admin.site.register(Building, BuildingAdmin)
admin.site.register(AccessControlRecord, AccessControlRecordAdmin)
admin.site.register(Controller, ControllerAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(BuildingSize, BuildingSizeAdmin)
admin.site.register(BuildingType, BuildingTypeAdmin)
admin.site.register(Currency, CurrencyAdmin)
admin.site.register(PublicIP, PublicIPAdmin)
admin.site.register(ExternalPresenceLogRecord, ExternalPresenceLogRecordAdmin)
admin.site.register(Country, CountryAdmin)
