from django.core.management.base import BaseCommand
from hardware.models import HardwareMapping, Hardware
from django.db.models import Count


class Command(BaseCommand):
    help = (
        "Assign hardware to mapping rule. By default only rule with missing hardware."
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "--action",
            choices=["add", "remove"],
            default="add",
            help="Chose either to add or remove the hadrware ID on the mapping",
        )

        parser.add_argument(
            "--hid",
            type=int,
            default=None,
            help="Hardware ID from Database. If not set then first hardware found will be use",
        )

        parser.add_argument(
            "--all",
            action="store_true",
            help="If set, all hardware mapping will be used instead of only missing hardware and user > 0",
        )

    def handle(self, *args, **options):
        hid = options["hid"]
        all_mapping = options["all"]
        action = options["action"]
        self.stdout.write(f"Action selected: {action}")
        hardware = Hardware.objects.first()

        if hid:
            hardware = Hardware.objects.filter(pk=hid).first()

        if hardware:
            self.stdout.write(f"Hardware selected: {hardware.name}")

            if all_mapping:
                hardware_mapping = HardwareMapping.objects.annotate(
                    user_count=Count("user")
                ).all()
            else:
                hardware_mapping = HardwareMapping.objects.annotate(
                    user_count=Count("user")
                ).filter(hardware=None, user_count__gt=0)

            self.stdout.write(
                f"Total hardware mapping with user: {hardware_mapping.count()}"
            )

            for mapping in hardware_mapping:
                if action == "add":
                    mapping.hardware.add(hardware)
                    mapping.save()
                    self.stdout.write(
                        f"Hardware {hardware.name} added to mapping {mapping.rule}"
                    )
                if action == "remove":
                    mapping.hardware.remove(hardware)
                    mapping.save()
                    self.stdout.write(
                        f"Hardware {hardware.name} removed from mapping {mapping.rule}"
                    )
        else:
            self.stdout.write(f"Hardware with ID: {hid}, not found")
