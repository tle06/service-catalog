from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings
from service.models import User
from core.settings import logger


class Command(BaseCommand):
    help = "Prepare server before run"

    def handle(self, *args, **options):
        if settings.DJANGO_EXEC_MAKEMIGRATIONS:
            logger.info("Run makemigrations command")
            call_command("makemigrations", "--noinput")
        else:
            logger.info("Skip makemigrations command")

        if settings.DJANGO_EXEC_MIGRATE:
            logger.info("Run migrate command")
            call_command("migrate")
        else:
            logger.info("Skip migrate command")

        if settings.DJANGO_EXEC_CREATESUPERUSER:
            user = User.objects.filter(email=settings.DJANGO_SUPERUSER_EMAIL).first()
            if not user:
                logger.info("Run createsuperuser command")
                call_command(
                    "createsuperuser",
                    f"--no-input --email {settings.DJANGO_SUPERUSER_EMAIL}",
                )
            else:
                logger.info(
                    f"Super user {settings.DJANGO_SUPERUSER_EMAIL} already exist, skip createsuperuser command"
                )
        else:
            logger.info("Skip createsuperuser command")

        if settings.DJANGO_EXEC_COLLECTSTATIC:
            logger.info("Run collectstatic command")
            call_command("collectstatic", "--noinput")
        else:
            logger.info("Skip collectStatic command")
