from django.core.management.base import BaseCommand
from django.db.models import Count
from django.db import connection
from core.settings import logger
from core.utils import dictfetchall
from service.models import Pricing, Application, Service, Provider
from service.models import User
import pandas as pd
import datetime

sso_type_map = [
    {"name": "SAML", "value": "saml1"},
    {"name": "Form-based auth", "value": "form-base"},
    {"name": "OpenID Connect", "value": "oidc"},
    {"name": "SAML2.0", "value": "saml2"},
    {"name": "WS-Federation with SAML 1.1", "value": "ws-federation"},
]


class Command(BaseCommand):
    help = "Create service, provider for every app wihtout one. Assign owner to service"

    def add_arguments(self, parser):
        parser.add_argument(
            "--serverurl",
            type=str,
            default="http://localhost:8000",
            help="Server URL to generate the object URL",
        )
        parser.add_argument(
            "--filename",
            type=str,
            default="result.csv",
            help="File name to store the result of creation",
        )

        parser.add_argument(
            "--sourcefile",
            type=str,
            default="source.csv",
            help="File name to load the owner and app data",
        )

    def handle(self, *args, **options):
        serverurl = options["serverurl"]
        filename = options["filename"]
        source_file = options["sourcefile"]

        create_by_user = User.objects.filter(pk=1).first()
        now = datetime.datetime.now()
        result = []

        dfsource = pd.read_csv(source_file)
        logger.info(f"app without services: {len(dfsource)}")

        start_count_service = Service.objects.all().count()
        start_count_application = Application.objects.all().count()
        start_count_provider = Provider.objects.all().count()
        start_count_application_owner = (
            Application.objects.filter(owner__isnull=False).all().count()
        )

        start_count_application_admin = (
            Application.objects.filter(administrator__isnull=False).all().count()
        )

        start_service_active = Service.objects.filter(status="active").all().count()

        for index, row in dfsource.iterrows():
            app_id = row["id"]
            logger.info(f"processing app id: {app_id}")
            service_status = row["Status"]
            sso_by_provider = row["need support from vendor"]
            sso_type = row["auth_method"]

            if sso_type:
                logger.info("Checking SSO type map")
                for item in sso_type_map:
                    if item["name"] == sso_type:
                        sso_type = item["value"]
                        break

            logger.info(f"sso_type: {sso_type}")

            if sso_by_provider in ["TRUE", "FALSE"]:
                sso_by_provider = True if sso_by_provider == "TRUE" else False
            logger.info(f"sso_by_provider: {sso_by_provider}")

            app = Application.objects.filter(app_id=app_id).first()

            if app:
                logger.info(f"processing app id: {app.id} with name {app.name}")
                service = app.service_set.first()
                app.sso_type = sso_type
                provider = None
                pricing = None

                if service:
                    logger.info(
                        f"app already have the service assigned: {service.name}"
                    )
                else:
                    service = Service.objects.filter(name=app.name).first()

                owner = row["owner"]
                owner2 = row["owner 2"]
                owner_user1 = None
                owner_user2 = None
                if not pd.isnull(owner):
                    owner_user1 = User.objects.filter(email=row["owner"]).first()
                    if not owner_user1:
                        owner_user1 = User(email=owner)
                        owner_user1.save()
                        logger.info(f"owner_user1 created: {owner_user1.email}")

                if not pd.isnull(owner2):
                    owner_user2 = User.objects.filter(email=row["owner 2"]).first()
                    if not owner_user2:
                        owner_user2 = User(email=owner2)
                        owner_user2.save()
                        logger.info(f"owner_user2 created: {owner_user2.email}")

                if not service:
                    provider = Provider(
                        name=app.name,
                        createdByUserId=create_by_user,
                        currency_preffered="EUR",
                    )
                    provider.save()
                    logger.info(f"provider ID created: {provider.id}")
                    service = Service(
                        name=app.name,
                        createdByUserId=create_by_user,
                        description=app.name,
                        identity_provider=app.idp,
                        provider=provider,
                    )
                    service.save()

                    logger.info(f"service ID created: {service.id}")
                else:
                    logger.info(
                        f"service already exist with ID: {service.id} and name: {service.name}"
                    )

                service.application.add(app)
                if service.application.count() <= 1:
                    logger.info(f"Service has app <=1")
                    if service_status in ["Deleted", "To delete"]:
                        service.status = "deleted"
                        logger.info(f"Service status set to deleted")
                else:
                    service.status = "active"

                if owner_user1:
                    service.owner.add(owner_user1)
                    logger.info(f"owner_user1 added to service: {owner_user1.email}")
                    service.save()

                if owner_user2:
                    service.owner.add(owner_user2)
                    logger.info(f"owner_user2 added to service: {owner_user2.email}")
                    service.save()

                if not service.provider:
                    provider = Provider(
                        name=app.name,
                        createdByUserId=create_by_user,
                        currency_preffered="EUR",
                    )
                    provider.save()
                    service.provider = provider
                    service.save()
                    logger.info(f"provider ID created: {provider.id}")
                else:
                    provider = service.provider

                if not app.pricing:
                    pricing = Pricing(
                        name=f"{app.name} licenses",
                        currency="EUR",
                        createdByUserId=create_by_user,
                    )
                    pricing.save()
                    logger.info(f"pricing ID created: {pricing.id}")
                    app.pricing = pricing
                else:
                    pricing = app.pricing

                if not app.creationDate:
                    app.creationDate = now

                if app.creationDate == "0000-00-00 00:00:00.000000":
                    app.creationDate = now

                if not app.provider:
                    app.provider = provider

                if not app.status:
                    app.status = service.status

                logger.info(f"app owner: {app.owner.all()}")
                if len(app.owner.all()) == 0:
                    logger.info("No application owner found")
                    if owner_user1:
                        app.owner.add(owner_user1)
                    if owner_user2:
                        app.owner.add(owner_user2)
                if len(app.administrator.all()) == 0:
                    logger.info("No application administrator found")
                    if owner_user1:
                        app.administrator.add(owner_user1)
                    if owner_user2:
                        app.administrator.add(owner_user2)

                app.save()

                result.append(
                    {
                        "app_id": app.id,
                        "app_name": app.name,
                        "provider_id": provider.id,
                        "service_id": service.id,
                        "service_name": service.name,
                        "pricing_id": pricing.id,
                        "service_url": f"{serverurl}/service/{service.id}",
                        "owner1": owner_user1.email if owner_user1 else "",
                        "owner2": owner_user2.email if owner_user2 else "",
                        "status": service.status,
                        "sso_type": sso_type,
                        "sso_by_provider": sso_by_provider,
                    }
                )

        logger.info(f"total app updated: {len(result)}")
        logger.info(f"save result to CSV: {filename}")
        logger.info(f"start count of services: {start_count_service}")
        logger.info(f"start count of active services: {start_service_active}")
        logger.info(f"start count of applications: {start_count_application}")
        logger.info(f"start count of provider: {start_count_provider}")
        logger.info(f"start count application owners: {start_count_application_owner}")
        logger.info(f"start count application owners: {start_count_application_admin}")

        end_count_service = Service.objects.all().count()
        end_count_application = Application.objects.all().count()
        end_count_provider = Provider.objects.all().count()
        end_count_application_owner = (
            Application.objects.filter(owner__isnull=False).all().count()
        )

        end_count_application_admin = (
            Application.objects.filter(administrator__isnull=False).all().count()
        )

        end_service_active = Service.objects.filter(status="active").all().count()
        logger.info(f"end count of services: {end_count_service}")
        logger.info(f"end count of active services: {end_service_active}")
        logger.info(f"end count of applications: {end_count_application}")
        logger.info(f"end count of provider: {end_count_provider}")
        logger.info(f"end count application owners: {end_count_application_owner}")
        logger.info(f"end count application owners: {end_count_application_admin}")
        df = pd.DataFrame(result)
        df.to_csv(filename)
