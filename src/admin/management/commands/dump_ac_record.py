from django.core.management.base import BaseCommand
from building.tasks import dump_access_control_record_task


class Command(BaseCommand):
    help = "Dump access control record from intemo SQL server"

    def handle(self, *args, **options):
        dump_access_control_record_task()
