from django.apps import AppConfig


class AdminConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "admin"
    label = "app_admin"
    verbose_name = "app_admin"
