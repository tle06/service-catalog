from django.contrib.auth.models import Group
from service.models import (
    User,
    Department,
    SubDepartment,
    Pricing,
    Provider,
    Service,
    Application,
    ContractDocument,
    Company,
    Configuration,
    Contact,
    Contract,
    ServiceCategory,
    IdentityProvider,
    Title,
)
from hardware.models import Hardware, HardwareCategory, RuleMapping, HardwareMapping
from building.models import (
    Building,
    AccessControlRecord,
    Controller,
    Currency,
    BuildingSize,
    BuildingType,
    PublicIP,
    ExternalPresenceLogRecord,
    Country,
)
from rest_framework import viewsets
from restapi.permissions import IsSuperUser
from rest_framework import status
from restapi.serializers import (
    ApplicationSerializer,
    UserSerializer,
    DepartmentSerializer,
    SubDepartmentSerializer,
    PricingSerializer,
    ProviderSerializer,
    CompanySerializer,
    ContactSerializer,
    ContractDocumentSerializer,
    ServiceSerializer,
    HardwareCategorySerializer,
    HardwareSerializer,
    TokenObtainPairResponseSerializer,
    GroupSerializer,
    ContractSerializer,
    ServiceCategorySerializer,
    ConfigurationSerializer,
    IdentityProviderSerializer,
    CustomTokenObtainPairSerializer,
    TitleSerializer,
    RuleMappingSerializer,
    HardwareMappingSerializer,
    BuildingSerializer,
    AccessControlRecordSerializer,
    ControllerSerializer,
    BuildingSizeSerializer,
    BuildingTypeSerializer,
    CurrencySerializer,
    PublicIPSerializer,
    BuildingPublicIpsSerializer,
    ExternalPresenceLogRecordSerializer,
    CountrySerializer,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from graphene_django.views import GraphQLView
from rest_framework_simplejwt.views import TokenObtainPairView
from drf_yasg.utils import swagger_auto_schema
from rest_framework_simplejwt.views import (
    TokenBlacklistView,
    TokenRefreshView,
    TokenVerifyView,
)
from django.http import HttpResponse, HttpResponseServerError

from rest_framework import serializers


def liveliness(request):
    return HttpResponse("OK")


def readiness(request):
    try:
        # Connect to database
        return HttpResponse("OK")
    except Exception:
        return HttpResponseServerError("db: cannot connect to database.")


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = User.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer
    permission_classes = [IsSuperUser]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsSuperUser]


class DepartmentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows departments to be viewed or edited.
    """

    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class SubDepartmentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows sub-departments to be viewed or edited.
    """

    queryset = SubDepartment.objects.all()
    serializer_class = SubDepartmentSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class TitleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows titles to be viewed or edited.
    """

    queryset = Title.objects.all()
    serializer_class = TitleSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class ProviderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows providers to be viewed or edited.
    """

    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class ContractViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows contracts to be viewed or edited.
    """

    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    permission_classes = [IsSuperUser]

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class ContractDocumentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows contracts documents to be viewed or edited.
    """

    queryset = ContractDocument.objects.all()
    serializer_class = ContractDocumentSerializer
    permission_classes = [IsSuperUser]

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class PricingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows pricings to be viewed or edited.
    """

    queryset = Pricing.objects.all()
    serializer_class = PricingSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class CompanyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows document to be viewed or edited.
    """

    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class ServiceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows services to be viewed or edited.
    """

    queryset = Service.objects.all()
    serializer_class = ServiceSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class ServiceCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows service category to be viewed or edited.
    """

    queryset = ServiceCategory.objects.all()
    serializer_class = ServiceCategorySerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class ConfigurationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows configuration to be viewed or edited.
    """

    queryset = Configuration.objects.all()
    serializer_class = ConfigurationSerializer
    permission_classes = [IsSuperUser]

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class IdentityProviderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows identity provider to be viewed or edited.
    """

    queryset = IdentityProvider.objects.all()
    serializer_class = IdentityProviderSerializer
    permission_classes = [IsSuperUser]

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class ContactViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows contacts to be viewed or edited.
    """

    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class HardwareCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows hadrware category to be viewed or edited.
    """

    queryset = HardwareCategory.objects.all()
    serializer_class = HardwareCategorySerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class HardwareViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows hardware to be viewed or edited.
    """

    queryset = Hardware.objects.all()
    serializer_class = HardwareSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class PrivateGraphQLView(LoginRequiredMixin, GraphQLView):
    pass


class CustomTokenObtainTokenPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer


class DecoratedTokenObtainPairView(TokenObtainPairView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenObtainPairResponseSerializer,
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class TokenRefreshResponseSerializer(serializers.Serializer):
    access = serializers.CharField()

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenRefreshView(TokenRefreshView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenRefreshResponseSerializer,
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class TokenVerifyResponseSerializer(serializers.Serializer):
    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenVerifyView(TokenVerifyView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenVerifyResponseSerializer,
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class TokenBlacklistResponseSerializer(serializers.Serializer):
    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenBlacklistView(TokenBlacklistView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenBlacklistResponseSerializer,
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class ApplicationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows hardware to be viewed or edited.
    """

    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class RuleMappingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows rule mapping to be viewed or edited.
    """

    queryset = RuleMapping.objects.all()
    serializer_class = RuleMappingSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class HardwareMappingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows hardware mapping to be viewed or edited.
    """

    queryset = HardwareMapping.objects.all()
    serializer_class = HardwareMappingSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)

    def get_queryset(self):
        queryset = HardwareMapping.objects.all()
        department_name = self.request.query_params.get("department_name")
        subdepartment_name = self.request.query_params.get("subdepartment_name")
        title_name = self.request.query_params.get("title_name")
        if (department_name and subdepartment_name and title_name) is not None:
            queryset = queryset.filter(
                rule__department__name=department_name,
                rule__subdepartment__name=subdepartment_name,
                rule__title__name=title_name,
            )
        return queryset


class BuildingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows building to be viewed or edited.
    """

    queryset = Building.objects.all()
    serializer_class = BuildingSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class AccessControlRecordViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows access control record to be viewed or edited.
    """

    queryset = AccessControlRecord.objects.all()
    serializer_class = AccessControlRecordSerializer
    permission_classes = [IsSuperUser]


class ControllerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows controller to be viewed or edited.
    """

    queryset = Controller.objects.all()
    serializer_class = ControllerSerializer
    permission_classes = [IsSuperUser]

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class BuildingTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows building type to be viewed or edited.
    """

    queryset = BuildingType.objects.all()
    serializer_class = BuildingTypeSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class BuildingSizeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows building size to be viewed or edited.
    """

    queryset = BuildingSize.objects.all()
    serializer_class = BuildingSizeSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class CurrencyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows currency to be viewed or edited.
    """

    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class PublicIPViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows public ips to be viewed or edited.
    """

    queryset = PublicIP.objects.all()
    serializer_class = PublicIPSerializer

    def perform_create(self, serializer):
        serializer.save(createdByUserId=self.request.user)


class buildingPublicIPsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows building public ips to be viewed or edited.
    """

    queryset = Building.objects.all()
    serializer_class = BuildingPublicIpsSerializer


class ExternalPresenceLogRecordPublicIPsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows external record logs to be viewed or edited.
    """

    queryset = ExternalPresenceLogRecord.objects.all()
    serializer_class = ExternalPresenceLogRecordSerializer
    permission_classes = [IsSuperUser]


class CountryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows country to be viewed or edited.
    """

    queryset = Country.objects.all()
    serializer_class = CountrySerializer
