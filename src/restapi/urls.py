from django.urls import include, path
from rest_framework import routers, permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from restapi.views import (
    ApplicationViewSet,
    CompanyViewSet,
    ConfigurationViewSet,
    ContactViewSet,
    ContractDocumentViewSet,
    ContractViewSet,
    CustomTokenObtainTokenPairView,
    DepartmentViewSet,
    GroupViewSet,
    HardwareCategoryViewSet,
    HardwareViewSet,
    IdentityProviderViewSet,
    PricingViewSet,
    ProviderViewSet,
    ServiceCategoryViewSet,
    ServiceViewSet,
    SubDepartmentViewSet,
    UserViewSet,
    TitleViewSet,
    RuleMappingViewSet,
    HardwareMappingViewSet,
    BuildingViewSet,
    AccessControlRecordViewSet,
    ControllerViewSet,
    CurrencyViewSet,
    BuildingTypeViewSet,
    BuildingSizeViewSet,
    PublicIPViewSet,
    buildingPublicIPsViewSet,
    ExternalPresenceLogRecordPublicIPsViewSet,
    CountryViewSet,
)
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView


router = routers.DefaultRouter()
router.register(r"users", UserViewSet)
router.register(r"groups", GroupViewSet)
router.register(r"departments", DepartmentViewSet)
router.register(r"subdepartments", SubDepartmentViewSet)
router.register(r"providers", ProviderViewSet)
router.register(r"companies", CompanyViewSet)
router.register(r"contracts", ContractViewSet)
router.register(r"contractsdocuments", ContractDocumentViewSet)
router.register(r"pricings", PricingViewSet)
router.register(r"services", ServiceViewSet)
router.register(r"servicecategories", ServiceCategoryViewSet)
router.register(r"configurations", ConfigurationViewSet)
router.register(r"identitiesproviders", IdentityProviderViewSet)
router.register(r"contacts", ContactViewSet)
router.register(r"hardwares", HardwareViewSet)
router.register(r"hardwarecatagories", HardwareCategoryViewSet)
router.register(r"applications", ApplicationViewSet)
router.register(r"titles", TitleViewSet)
router.register(r"rulesmapping", RuleMappingViewSet)
router.register(r"hardwaremapping", HardwareMappingViewSet)
router.register(r"building", BuildingViewSet, basename="building")
router.register(r"accesscontrolrecords", AccessControlRecordViewSet)
router.register(r"controllers", ControllerViewSet)
router.register(r"buildingtype", BuildingTypeViewSet)
router.register(r"buildingzise", BuildingSizeViewSet)
router.register(r"currency", CurrencyViewSet)
router.register(r"publicips", PublicIPViewSet)
router.register(
    r"buildingpublicips", buildingPublicIPsViewSet, basename="buildingpublicips"
)
router.register(
    r"externalpresencelogrecord",
    ExternalPresenceLogRecordPublicIPsViewSet,
)
router.register(r"countries", CountryViewSet)

schema_view = get_schema_view(
    openapi.Info(
        title="Service Catalog API",
        default_version="v1",
        description="Service Catalog API",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path("api/v1/", include(router.urls), name="api_v1"),
    path("api/v1/docs/", schema_view.with_ui(cache_timeout=0), name="swagger-ui"),
    path(
        "api/v1/auth/",
        include("rest_framework.urls", namespace="rest_framework"),
        name="api_v1_auth",
    ),
    path(
        "api/v1/jwt/",
        CustomTokenObtainTokenPairView.as_view(),
        name="token_obtain_pair",
    ),
    path("api/v1/jwt/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("api/v1/jwt/verify/", TokenVerifyView.as_view(), name="token_verify"),
]
