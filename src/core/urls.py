"""service_catalog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from service.views.index import IndexView
from service import urls as service_urls
from hardware import urls as hardware_urls
from scim import urls as scim_urls
from restapi import urls as restapi_urls
from gql import urls as gql_urls
from admin import urls as admin_urls
from application import urls as application_urls
from building import urls as building_urls
from django.conf import settings
from django.conf.urls.static import static
from oauth2_provider.views import (
    AuthorizationView,
    TokenView,
    RevokeTokenView,
    IntrospectTokenView,
)


# swagger doc: https://appliku.com/post/django-rest-framework-swagger-and-typescript-api-c#swagger-documentation
# and https://pypi.org/project/drf-yasg/#installation

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("", include(service_urls)),
    path("", include(scim_urls)),
    path("", include(hardware_urls)),
    path("", include(restapi_urls)),
    path("", include(gql_urls)),
    path("", include(admin_urls)),
    path("", include(application_urls)),
    path("", include(building_urls)),
    path("accounts/", include("allauth.urls")),
    path("hc/", include("health_check.urls")),
    path("oauth/authorize/", AuthorizationView.as_view(), name="authorize"),
    path("oauth/token/", TokenView.as_view(), name="token"),
    path("oauth/revoke_token/", RevokeTokenView.as_view(), name="revoke-token"),
    path("oauth/introspect/", IntrospectTokenView.as_view(), name="introspect"),
]

if settings.OIDC_ACTIVATED:
    urlpatterns.append(
        path("oidc/", include("mozilla_django_oidc.urls")),
    )

if not settings.USE_AWS_S3_FOR_FILES:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
