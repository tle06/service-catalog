from hardware.models import Hardware, HardwareMapping
from service.models import Contract, Pricing
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
    View,
)
from django.urls import reverse_lazy
from core.utils import get_pagination_configuration
from django.db.models import Q
from import_export import resources
from django.http import (
    HttpResponseRedirect,
    HttpResponse,
)
from core.utils import ViewExport
from django.shortcuts import get_object_or_404
from django.db.models import Count


class HardwareListView(LoginRequiredMixin, ListView):
    model = Hardware
    context_object_name = "instance_object"
    template_name = "hardware/hardwares.html"
    extra_context = {
        "custom_title": "Hardwares",
        "create_url": "hardware_creation",
        "update_url": "hardware_update",
        "details_url": "hardware_details",
        "delete_url": "hardware_delete",
        "export_url": "hardware_export",
    }


class HardwareSearchView(LoginRequiredMixin, ListView):
    model = Hardware
    context_object_name = "instance_object"
    template_name = "hardware/hardwares.html"
    extra_context = {
        "custom_title": "Hardwares",
        "create_url": "hardware_creation",
        "update_url": "hardware_update",
        "details_url": "hardware_details",
        "delete_url": "hardware_delete",
        "export_url": "hardware_export",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Hardware.objects.filter(
            Q(name__icontains=query)
            | Q(description__icontains=query)
            | Q(model_name__icontains=query)
            | Q(model_number__icontains=query)
        )
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "hardware_list", kwargs={"query": self.request.GET.get("query")}
        )


class HardwareDetailView(LoginRequiredMixin, DetailView):
    model = Hardware
    template_name = "hardware/hardware.html"
    hardwares = Hardware.objects.all()
    contracts = Contract.objects.all()
    pricings = Pricing.objects.all()
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Hardwares",
        "all_Hardwares": hardwares,
        "all_contracts": contracts,
        "all_pricings": pricings,
        "create_url": "hardware_creation",
        "update_url": "hardware_update",
        "details_url": "hardware_details",
        "delete_url": "hardware_delete",
        "export_url": "hardware_export",
        "list_url": "hardware_list",
    }

    def get_success_url(self):
        tab = self.request.GET.get("tab")
        path = reverse_lazy("hardware_details")
        tab = self.request.GET.get("tab", "main")
        url = f"{path}?tab={tab}"
        return url


class HardwareCreateView(PermissionRequiredMixin, CreateView):
    model = Hardware
    template_name = "hardware/create_update_hardware.html"
    extra_context = {
        "form_action": "create",
        "custom_title": "Hardware",
        "list_url": "hardware_list",
    }
    context_object_name = "instance_object"
    permission_required = "hardware.add_hardware"
    fields = [
        "name",
        "description",
        "provider",
        "status",
        "model_name",
        "model_number",
        "icon",
        "category",
        "support_url",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("hardware_details", kwargs={"pk": self.object.pk})


class HardwareUpdateView(PermissionRequiredMixin, UpdateView):
    model = Hardware
    template_name = "hardware/create_update_hardware.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "hardware",
        "list_url": "hardware_list",
    }
    context_object_name = "instance_object"
    permission_required = "hardware.change_hardware"
    fields = [
        "name",
        "description",
        "provider",
        "status",
        "model_name",
        "model_number",
        "icon",
        "category",
        "support_url",
    ]

    def get_success_url(self):
        return reverse_lazy("hardware_details", kwargs={"pk": self.object.pk})


class HardwareDeleteView(PermissionRequiredMixin, DeleteView):
    model = Hardware
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}
    permission_required = "hardware.delete_hardware"

    def get_success_url(self):
        return reverse_lazy("hardware_list")


# https://stackoverflow.com/questions/24008820/use-django-import-export-with-class-based-views
class HardwareResource(resources.ModelResource):
    class Meta:
        model = Hardware


class HardwareExport(PermissionRequiredMixin, ViewExport):
    filename = "hardware.csv"
    content_type = "csv"
    resource = HardwareResource
    permission_required = "hardware.export_hardware"


class AddContractLinkToHardwareView(PermissionRequiredMixin, View):
    permission_required = "hardware.change_hardware"

    def post(self, request, pk):
        object_relation_id = request.POST.get("relation")
        current_object = get_object_or_404(Hardware, pk=pk)
        current_object.contract.add(object_relation_id)
        path = reverse_lazy("hardware_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "contract")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class RemoveContractLinkToHardwareView(PermissionRequiredMixin, View):
    permission_required = "hardware.change_hardware"

    def post(self, request, pk, lpk):
        request.POST.get("relation")
        current_object = get_object_or_404(Hardware, pk=pk)
        current_object.contract.remove(lpk)
        path = reverse_lazy("hardware_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "contract")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class AddPricingLinkToHardwareView(PermissionRequiredMixin, View):
    permission_required = "hardware.change_hardware"

    def post(self, request, pk):
        object_relation_id = request.POST.get("relation")
        current_object = get_object_or_404(Hardware, pk=pk)
        current_object.pricing.add(object_relation_id)
        path = reverse_lazy("hardware_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "pricing")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class RemovePricingLinkToHardwareView(PermissionRequiredMixin, View):
    permission_required = "hardware.change_hardware"

    def post(self, request, pk, lpk):
        request.POST.get("relation")
        current_object = get_object_or_404(Hardware, pk=pk)
        current_object.pricing.remove(lpk)
        path = reverse_lazy("hardware_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "pricing")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class HardwareMappingListView(LoginRequiredMixin, ListView):
    model = HardwareMapping
    context_object_name = "instance_object"
    paginate_by = get_pagination_configuration("hardware_pagination")
    template_name = "hardware_mapping/hardwares_mappings.html"
    extra_context = {
        "custom_title": "Hardwares Mappings",
        "create_url": "hardware_mapping_creation",
        "update_url": "hardware_mapping_update",
        "delete_url": "hardware_mapping_delete",
        "export_url": "hardware_mapping_export",
    }

    def get_queryset(self):
        object_list = HardwareMapping.objects.annotate(user_count=Count("user"))
        return object_list


class HardwareMappingCreateView(PermissionRequiredMixin, CreateView):
    model = HardwareMapping
    template_name = "hardware_mapping/create_update.html"
    extra_context = {
        "form_action": "create",
        "custom_title": "Hardware Mapping",
        "list_url": "hardware_mapping_list",
    }
    context_object_name = "instance_object"
    permission_required = "hardware.add_hardwaremapping"
    fields = [
        "rule",
        "hardware",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("hardware_mapping_list", kwargs={})


class HardwareMappingUpdateView(PermissionRequiredMixin, UpdateView):
    model = HardwareMapping
    template_name = "hardware_mapping/create_update.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "hardware mapping",
        "list_url": "hardware_mapping_list",
    }
    context_object_name = "instance_object"
    permission_required = "hardware.change_hardwaremapping"
    fields = [
        "hardware",
    ]

    def get_success_url(self):
        return reverse_lazy("hardware_mapping_list", kwargs={})


class HardwareMappingDeleteView(PermissionRequiredMixin, DeleteView):
    model = HardwareMapping
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}
    permission_required = "hardware.delete_hardwaremapping"

    def get_success_url(self):
        return reverse_lazy("hardware_mapping_list")


class HardwareMappingResource(resources.ModelResource):
    class Meta:
        model = HardwareMapping


class HardwareMappingExport(PermissionRequiredMixin, ViewExport):
    filename = "hardware_mapping.csv"
    content_type = "csv"
    resource = HardwareMappingResource
    permission_required = "hardware.export_hardware_mapping"


class HardwareMappingSearchView(LoginRequiredMixin, ListView):
    model = HardwareMapping
    context_object_name = "instance_object"
    paginate_by = get_pagination_configuration("hardware_pagination")
    template_name = "hardware_mapping/hardwares_mappings.html"
    extra_context = {
        "custom_title": "Hardwares Mappings",
        "create_url": "hardware_mapping_creation",
        "update_url": "hardware_mapping_update",
        "delete_url": "hardware_mapping_delete",
        "export_url": "hardware_mapping_export",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        minuser = self.request.GET.get("minuser")

        if query == "missing":
            if minuser:
                object_list = HardwareMapping.objects.annotate(
                    user_count=Count("user")
                ).filter(hardware=None, user_count__gt=minuser)
            else:
                object_list = HardwareMapping.objects.annotate(
                    user_count=Count("user")
                ).filter(hardware=None)
        else:
            object_list = HardwareMapping.objects.filter(
                Q(rule__department__name__icontains=query)
                | Q(rule__subdepartment__name__icontains=query)
                | Q(rule__title__name__icontains=query)
                | Q(hardware__name__icontains=query)
            ).annotate(user_count=Count("user"))
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "hardware_mapping_list", kwargs={"query": self.request.GET.get("query")}
        )


class AddHardwareToHardwareMapping(PermissionRequiredMixin, View):
    permission_required = "hardware.change_hardware_mapping"

    def post(self, request, pk):
        lpk = request.POST.get("relation")
        mapping = get_object_or_404(HardwareMapping, pk=pk)
        if mapping:
            mapping.hardware.add(lpk)

            return HttpResponse(status=200)
        return HttpResponse(status=404)


class RemoveHardwareToHardwareMapping(PermissionRequiredMixin, View):
    permission_required = "hardware.change_hardware_mapping"

    def post(self, request, pk, lpk):
        mapping = get_object_or_404(HardwareMapping, pk=pk)
        if mapping:
            mapping.hardware.remove(lpk)

            return HttpResponse(status=200)
        return HttpResponse(status=404)
