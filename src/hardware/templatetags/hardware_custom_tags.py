from django import template
from hardware.models import Hardware, HardwareMapping, RuleMapping
from service.models import User
from django.db.models import Count

register = template.Library()


@register.simple_tag
def get_total_hardware():
    objects = Hardware.objects.all()
    return objects.count()


@register.simple_tag
def get_total_hardware_mapping():
    objects = HardwareMapping.objects.all()
    return objects.count()


@register.simple_tag
def get_total_hardware_rule_mapping():
    objects = RuleMapping.objects.all()
    return objects.count()


@register.simple_tag
def get_rule_not_set():
    raw_query = """SELECT * FROM hardware_rulemapping AS rm 
    LEFT JOIN hardware_hardwaremapping AS hm 
    ON rm.id = hm.rule_id 
    WHERE hm.id IS NULL;"""
    rules = RuleMapping.objects.raw(raw_query)

    return rules


@register.simple_tag
def get_hardware():
    return Hardware.objects.all()


@register.simple_tag
def get_missing_hardware_mapping(include_zero_user: bool = True):
    print(include_zero_user)
    if include_zero_user:
        return (
            HardwareMapping.objects.annotate(user_count=Count("user"))
            .filter(hardware=None)
            .count()
        )
    return (
        HardwareMapping.objects.annotate(user_count=Count("user"))
        .filter(hardware=None, user_count__gt=0)
        .count()
    )


@register.simple_tag
def get_total_user_for_hardware_mapping_rule(
    department_id: int, subdepartment_id: int, title_id: int
):
    return User.objects.filter(
        is_active=True,
        department__id=department_id,
        subdepartment__id=subdepartment_id,
        title__id=title_id,
    ).count()
