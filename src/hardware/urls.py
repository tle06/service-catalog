from django.urls import path
from hardware.views import (
    AddContractLinkToHardwareView,
    AddPricingLinkToHardwareView,
    HardwareCreateView,
    HardwareDeleteView,
    HardwareDetailView,
    HardwareExport,
    HardwareListView,
    HardwareSearchView,
    HardwareUpdateView,
    RemoveContractLinkToHardwareView,
    RemovePricingLinkToHardwareView,
    HardwareMappingListView,
    HardwareMappingCreateView,
    HardwareMappingUpdateView,
    HardwareMappingDeleteView,
    HardwareMappingExport,
    HardwareMappingSearchView,
    AddHardwareToHardwareMapping,
    RemoveHardwareToHardwareMapping,
)


urlpatterns = [
    path("hardware/", HardwareListView.as_view(), name="hardware_list"),
    path("hardware/<int:pk>", HardwareDetailView.as_view(), name="hardware_details"),
    path("hardware/create", HardwareCreateView.as_view(), name="hardware_creation"),
    path(
        "hardware/update/<int:pk>", HardwareUpdateView.as_view(), name="hardware_update"
    ),
    path(
        "hardware/update/<int:pk>/linkcontract",
        AddContractLinkToHardwareView.as_view(),
        name="hardware_add_linkcontract",
    ),
    path(
        "hardware/update/<int:pk>/removecontract/<int:lpk>",
        RemoveContractLinkToHardwareView.as_view(),
        name="hardware_remove_linkcontract",
    ),
    path(
        "hardware/update/<int:pk>/linkpricing",
        AddPricingLinkToHardwareView.as_view(),
        name="hardware_add_linkpricing",
    ),
    path(
        "hardware/update/<int:pk>/removespricing/<int:lpk>",
        RemovePricingLinkToHardwareView.as_view(),
        name="hardware_remove_linkpricing",
    ),
    path(
        "hardware/delete/<int:pk>", HardwareDeleteView.as_view(), name="hardware_delete"
    ),
    path("hardware/search/", HardwareSearchView.as_view(), name="hardware_search"),
    path("hardware/export/", HardwareExport.as_view(), name="hardware_export"),
    path(
        "hardware/mapping/",
        HardwareMappingListView.as_view(),
        name="hardware_mapping_list",
    ),
    path(
        "hardware/mapping/create",
        HardwareMappingCreateView.as_view(),
        name="hardware_mapping_creation",
    ),
    path(
        "hardware/mapping/update/<int:pk>",
        HardwareMappingUpdateView.as_view(),
        name="hardware_mapping_update",
    ),
    path(
        "hardware/mapping/delete/<int:pk>",
        HardwareMappingDeleteView.as_view(),
        name="hardware_mapping_delete",
    ),
    path(
        "hardware/mapping/export/",
        HardwareMappingExport.as_view(),
        name="hardware_mapping_export",
    ),
    path(
        "hardware/mapping/search/",
        HardwareMappingSearchView.as_view(),
        name="hardware_mapping_search",
    ),
    path(
        "hardware/mapping/update/<int:pk>/linkhardware",
        AddHardwareToHardwareMapping.as_view(),
        name="hardware_mapping_add_hardware",
    ),
    path(
        "hardware/mapping/update/<int:pk>/removehardware/<int:lpk>",
        RemoveHardwareToHardwareMapping.as_view(),
        name="hardware_mapping_remove_hardware",
    ),
]
