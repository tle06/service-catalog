from service.models import Pricing, Application, Service, Provider
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
    View,
)
from django.urls import reverse_lazy
from core.utils import get_pagination_configuration
from django.db.models import Q
from import_export import resources
from django.http import (
    HttpResponse,
    HttpResponseRedirect,
)
from core.utils import ViewExport
from django.shortcuts import get_object_or_404
from core.utils import is_ajax
from service.views.service import ServiceCreateView
from service.views.pricing import PricingCreateView
from building.models import Country


class ApplicationListView(LoginRequiredMixin, ListView):
    model = Application
    context_object_name = "instance_object"
    paginate_by = get_pagination_configuration("application_pagination")
    template_name = "application/applications.html"
    all_services = Service.objects.all()
    all_pricings = Pricing.objects.all()
    extra_context = {
        "custom_title": "Applications",
        "create_url": "application_creation",
        "update_url": "application_update",
        "details_url": "application_details",
        "delete_url": "application_delete",
        "export_url": "application_export",
        "all_services": all_services,
        "all_pricings": all_pricings,
    }


class ApplicationSearchView(LoginRequiredMixin, ListView):
    model = Application
    context_object_name = "applications"
    paginate_by = get_pagination_configuration("application_pagination")
    template_name = "application/applications.html"
    extra_context = {
        "custom_title": "Applications",
        "create_url": "application_creation",
        "update_url": "application_update",
        "details_url": "application_details",
        "delete_url": "application_delete",
        "export_url": "application_export",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Application.objects.filter(
            Q(name__icontains=query) | Q(app_id=query)
        )
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "application_list", kwargs={"query": self.request.GET.get("query")}
        )


class ApplicationDetailView(LoginRequiredMixin, DetailView):
    model = Application
    template_name = "application/application.html"
    pricings = Pricing.objects.all()
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Applications",
        "all_pricings": pricings,
        "create_url": "application_creation",
        "update_url": "application_update",
        "details_url": "application_details",
        "delete_url": "application_delete",
        "export_url": "application_export",
        "list_url": "application_list",
    }

    def get_success_url(self):
        tab = self.request.GET.get("tab")
        path = reverse_lazy("application_details")
        tab = self.request.GET.get("tab", "main")
        url = f"{path}?tab={tab}"
        return url


class ApplicationCreateView(PermissionRequiredMixin, CreateView):
    model = Application
    template_name = "application/create_update.html"
    extra_context = {
        "form_action": "create",
        "custom_title": "application",
        "list_url": "application_list",
    }
    context_object_name = "instance_object"
    permission_required = "service.add_application"
    fields = [
        "name",
        "app_id",
        "icon_url",
        "idp",
        "pricing",
        "provider_location",
        "data_location",
        "owner",
        "administrator",
        "provider",
        "status",
        "sso_type",
        "sso_manage_by_provider",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("application_details", kwargs={"pk": self.object.pk})


class ApplicationUpdateView(PermissionRequiredMixin, UpdateView):
    model = Application
    template_name = "application/create_update.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "Application",
        "list_url": "application_list",
    }
    context_object_name = "instance_object"
    permission_required = "service.change_application"
    fields = [
        "name",
        "app_id",
        "icon_url",
        "idp",
        "pricing",
        "provider_location",
        "data_location",
        "owner",
        "administrator",
        "provider",
        "status",
        "sso_type",
        "sso_manage_by_provider",
    ]

    def get_success_url(self):
        return reverse_lazy("application_details", kwargs={"pk": self.object.pk})


class ApplicationDeleteView(PermissionRequiredMixin, DeleteView):
    model = Application
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}
    permission_required = "application.delete_application"

    def get_success_url(self):
        return reverse_lazy("application_list")


# https://stackoverflow.com/questions/24008820/use-django-import-export-with-class-based-views
class ApplicationResource(resources.ModelResource):
    class Meta:
        model = Application


class ApplicationExport(PermissionRequiredMixin, ViewExport):
    filename = "application.csv"
    content_type = "csv"
    resource = ApplicationResource
    permission_required = "service.export_application"


class AddPricingLinkToApplicationView(PermissionRequiredMixin, View):
    permission_required = "service.change_application"

    def post(self, request, pk):
        lpk = request.POST.get("relation")
        current_object = get_object_or_404(Application, pk=pk)
        pricing = Pricing.objects.filter(id=lpk).first()
        current_object.pricing = pricing
        current_object.save()
        if is_ajax(request):
            return HttpResponse(status=200)

        path = reverse_lazy("application_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "pricing")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class RemovePricingLinkToApplicationView(PermissionRequiredMixin, View):
    permission_required = "service.change_application"

    def post(self, request, pk, lpk):
        current_object = get_object_or_404(Application, pk=pk)
        current_object.pricing = None
        current_object.save()

        if is_ajax(request):
            return HttpResponse(status=200)

        path = reverse_lazy("application_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "pricing")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class AddApplicationToService(PermissionRequiredMixin, View):
    permission_required = "service.change_service"

    def post(self, request, pk):
        lpk = request.POST.get("relation")
        service = get_object_or_404(Service, pk=lpk)
        if service:
            service.application.add(pk)

            return HttpResponse(status=200)
        return HttpResponse(status=404)


class RemoveApplicationToService(PermissionRequiredMixin, View):
    permission_required = "service.change_service"

    def post(self, request, pk, lpk):
        service = get_object_or_404(Service, pk=lpk)
        if service:
            service.application.remove(pk)

            return HttpResponse(status=200)
        return HttpResponse(status=404)


class ApplicationServiceCreateView(ServiceCreateView):
    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        self.object = form.save()

        pk = self.kwargs.get("pk")
        app = get_object_or_404(Application, pk=pk)
        self.object.application.add(app)
        self.object.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("application_list", kwargs={})


class ApplicationPricingCreateView(PricingCreateView):
    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        self.object = form.save()
        pk = self.kwargs.get("pk")
        pricing = get_object_or_404(Pricing, pk=self.object.id)

        app = get_object_or_404(Application, pk=pk)
        app.pricing = pricing
        app.save()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("application_list", kwargs={})


class ApplicationServicePricingQuickCreateView(PermissionRequiredMixin, View):
    permission_required = ("service.add_service", "service.add_pricing")

    def post(self, request, pk):
        app = Application.objects.filter(pk=pk).first()
        provider = Provider(
            name=app.name, createdByUserId=self.request.user, currency_preffered="EUR"
        )
        provider.save()
        pricing = Pricing(
            name=f"{app.name} licenses",
            currency="EUR",
            createdByUserId=self.request.user,
        )
        pricing.save()
        service = Service(
            name=app.name,
            createdByUserId=self.request.user,
            description=app.name,
            identity_provider=app.idp,
            provider=provider,
        )
        service.save()
        service.application.add(app)
        app.pricing = pricing
        app.provider = provider
        app.save()

        return HttpResponseRedirect(reverse_lazy("application_list", kwargs={}))


class AddDataLocationToApplication(PermissionRequiredMixin, View):
    permission_required = "service.change_application"

    def post(self, request, pk):
        app = get_object_or_404(Application, pk=pk)
        lpk = request.POST.get("relation")
        country = get_object_or_404(Country, pk=lpk)
        if country:
            app.data_location = country
            app.save()

            return HttpResponse(status=200)
        return HttpResponse(status=404)


class RemoveDataLocationToApplication(PermissionRequiredMixin, View):
    permission_required = "service.change_application"

    def post(self, request, pk, lpk):
        application = Application.objects.filter(pk=pk).first()
        country = get_object_or_404(Country, pk=lpk)
        if country:
            application.data_location = None
            application.save()

            return HttpResponse(status=200)
        return HttpResponse(status=404)


class AddProviderLocationToApplication(PermissionRequiredMixin, View):
    permission_required = "service.change_application"

    def post(self, request, pk):
        app = get_object_or_404(Application, pk=pk)
        lpk = request.POST.get("relation")
        country = get_object_or_404(Country, pk=lpk)
        if country:
            app.provider_location = country
            app.save()

            return HttpResponse(status=200)
        return HttpResponse(status=404)


class RemoveProviderLocationToApplication(PermissionRequiredMixin, View):
    permission_required = "service.change_application"

    def post(self, request, pk, lpk):
        app = get_object_or_404(Application, pk=pk)
        country = get_object_or_404(Country, pk=lpk)
        if country:
            app.provider_location = None
            app.save()

            return HttpResponse(status=200)
        return HttpResponse(status=404)
