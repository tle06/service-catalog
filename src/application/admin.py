from django.contrib import admin
from service.models import Application
from import_export.admin import ImportExportModelAdmin
from import_export import resources


class ApplicationResource(resources.ModelResource):
    class Meta:
        model = Application


class ApplicationAdmin(ImportExportModelAdmin):
    resource_class = ApplicationResource

    search_fields = ("name", "app_id")
    list_display = ("name", "app_id")
    list_filter = ()
    filter_horizontal = ()


admin.site.register(Application, ApplicationAdmin)
