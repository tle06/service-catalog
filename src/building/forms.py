from django import forms
from service.models import ContractDocument


class UploadBuildingDocumentFrom(forms.ModelForm):
    class Meta:
        model = ContractDocument
        fields = ["file"]
