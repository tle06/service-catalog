# Generated by Django 4.1.13 on 2024-01-25 10:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("building", "0013_rename_amphitheater_quanity_building_amphitheater_quantity"),
    ]

    operations = [
        migrations.AlterField(
            model_name="currency",
            name="createdByUserId",
            field=models.ForeignKey(
                auto_created=True,
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="FK_currency_created_by_user_id",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.CreateModel(
            name="PublicIP",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("ip", models.CharField(max_length=255, null=True)),
                ("creationDate", models.DateTimeField(auto_now_add=True)),
                (
                    "createdByUserId",
                    models.ForeignKey(
                        auto_created=True,
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="FK_public_ip_created_by_user_id",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "verbose_name": "public IP",
                "verbose_name_plural": "Public IPs",
                "ordering": ["id"],
                "permissions": [("export_public_ip", "Can export public IP")],
            },
        ),
        migrations.AddField(
            model_name="building",
            name="public_ips",
            field=models.ManyToManyField(blank=True, to="building.publicip"),
        ),
    ]
