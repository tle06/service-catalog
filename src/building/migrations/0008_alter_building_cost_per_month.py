# Generated by Django 4.0.1 on 2023-11-05 19:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('building', '0007_alter_building_square_meters'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building',
            name='cost_per_month',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
