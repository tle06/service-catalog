from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.core.files.storage import default_storage
from service.storages import AWSPrivateMediaStorage
import os
from service.models import Company, ContractDocument


class Country(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    country_code = models.CharField(max_length=2, blank=True, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_country_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("country")
        verbose_name_plural = _("countries")
        ordering = ["id"]
        permissions = [
            ("export_country", "Can export country"),
        ]

    def __str__(self):
        return self.name


class Currency(models.Model):
    name = models.CharField(max_length=3, blank=False, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_currency_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("currency")
        verbose_name_plural = _("currencies")
        ordering = ["id"]
        permissions = [
            ("export_currency", "Can export currency"),
        ]

    def __str__(self):
        return self.name


class BuildingType(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_building_type_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("building type")
        verbose_name_plural = _("building types")
        ordering = ["id"]
        permissions = [
            ("export_building_type", "Can export building type"),
        ]

    def __str__(self):
        return self.name


class BuildingSize(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_building_size_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("building size")
        verbose_name_plural = _("building size")
        ordering = ["id"]
        permissions = [
            ("export_building_size", "Can export building size"),
        ]

    def __str__(self):
        return self.name


class Building(models.Model):
    STATUS = (
        ("active", "Active"),
        ("suspended", "Suspended"),
        ("decommissioned", "Decommissioned"),
    )

    def get_upload_path(instance, filename):
        name, ext = os.path.splitext(filename)
        return os.path.join("building", str(instance.id), name + ext)

    def get_storage():
        storage = default_storage
        if settings.USE_AWS_S3_FOR_FILES:
            storage = AWSPrivateMediaStorage()
        return storage

    name = models.CharField(max_length=255, blank=False, null=False)
    target_presence = models.IntegerField(blank=True, null=True)

    building_manager = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, related_name="FK_building_manager_user_id"
    )
    it_support = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_building_it_support_user_id",
    )
    general_manager = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_building_general_manager_user_id",
    )
    culture_embassador = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_building_culture_ambassador_user_id",
    )
    people_director = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_building_people_director_user_id",
    )
    people_partner = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="FK_building_people_partner_user_id",
    )
    address = models.CharField(max_length=255, blank=True, null=True)
    street_number = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    zip_code = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)
    company = models.ForeignKey(
        Company, on_delete=models.SET_NULL, blank=True, null=True
    )

    capacity = models.IntegerField(null=True, blank=True)
    square_meters = models.FloatField(null=True, blank=True)
    document = models.ManyToManyField(ContractDocument, blank=True)
    reception = models.BooleanField(default=False, null=False, blank=False)
    type = models.ForeignKey(
        BuildingType, on_delete=models.SET_NULL, blank=True, null=True
    )
    phone_number = models.CharField(null=True, blank=True, max_length=30)
    size = models.ForeignKey(
        BuildingSize, on_delete=models.SET_NULL, blank=True, null=True
    )
    renting = models.BooleanField(default=False, null=False, blank=False)
    start_rending_date = models.DateTimeField(null=True, blank=True)
    cost_per_month = models.FloatField(null=True, blank=True)
    currency = models.ForeignKey(
        Currency, on_delete=models.SET_NULL, blank=True, null=True
    )
    banner = models.ImageField(
        upload_to=get_upload_path, null=True, blank=True, storage=get_storage()
    )
    status = models.CharField(max_length=30, choices=STATUS, default="active")
    meeting_rom_quantity = models.IntegerField(null=True, blank=True)
    phonebooth_quantity = models.IntegerField(null=True, blank=True)
    amphitheater_quantity = models.IntegerField(null=True, blank=True)

    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_building_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("building")
        verbose_name_plural = _("buildings")
        ordering = ["id"]
        permissions = [
            ("export_building", "Can export building"),
            ("add_document", "Can add document to building"),
            ("delete_document", "Can delete document from building"),
            ("view_document", "Can view document from building"),
        ]

    def __str__(self):
        return self.name


class PublicIP(models.Model):
    ip = models.GenericIPAddressField(blank=False, null=True, unique=True)
    building = models.ForeignKey(
        Building,
        on_delete=models.SET_NULL,
        related_name="public_ips",
        blank=True,
        null=True,
    )
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_public_ip_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("public IP")
        verbose_name_plural = _("Public IPs")
        ordering = ["id"]
        permissions = [
            ("export_public_ip", "Can export public IP"),
        ]

    def __str__(self):
        return self.ip


class Controller(models.Model):
    TYPE = (
        ("entrance", "Entrance"),
        ("exit", "Exit"),
        ("storage_room", "Storage room"),
        ("rack_room", "Rack room"),
        ("it_storage", "IT Storage room"),
    )
    name = models.CharField(max_length=255, blank=False, null=False)
    external_id = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=30, choices=TYPE, blank=True, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_controller_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("controller")
        verbose_name_plural = _("controllers")
        ordering = ["id"]
        permissions = [
            ("export_controller", "Can export controller"),
        ]

    def __str__(self):
        return self.name


class AccessControlRecord(models.Model):
    source_id = models.IntegerField(blank=False, null=False, unique=True)
    building = models.ForeignKey(
        Building,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=False,
    )
    date = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_access_control_user_id",
    )
    card = models.CharField(max_length=255, blank=True, null=True)
    controller = models.ForeignKey(
        Controller, on_delete=models.SET_NULL, blank=True, null=True
    )
    user_type = models.CharField(max_length=255, blank=True, null=True)
    import_date = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("access control record")
        verbose_name_plural = _("access control records")
        ordering = ["id"]
        permissions = [
            ("export_access_control_record", "Can export access control record"),
        ]

    def __str__(self):
        return str(self.id)


class ExternalPresenceLogRecord(models.Model):
    source_id = models.CharField(max_length=255, blank=True, null=True)
    source_name = models.CharField(max_length=255, blank=True, null=True)

    building = models.ForeignKey(
        Building,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=False,
    )
    date = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        auto_created=True,
        related_name="FK_external_presence_log_record_user_id",
    )

    import_date = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("external presence log record")
        verbose_name_plural = _("external presence log records")
        ordering = ["id"]
        permissions = [
            (
                "export_external_presence_log_record",
                "Can export external presence log record",
            ),
        ]

    def __str__(self):
        return str(self.id)
