from django.apps import AppConfig


class BuildingConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "building"
    name = "building"
    label = "building"
    verbose_name = "building"
