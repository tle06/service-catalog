import requests
import logging
from service.models import Application

logger = logging.getLogger(__name__)


class GenericIDP:
    client_id = None
    client_secret = None
    root_url = None
    token = None
    field_mapping = None

    def __init__(self, client_id: str, client_secret: str, root_url: str):
        self.client_id = client_id
        self.client_secret = client_secret
        self.root_url = root_url
        self.generate_token()

    def generate_token(self):
        pass

    def generate_url(self, endpoint: str):
        gen_url = f"{self.root_url}/{endpoint}"
        return gen_url

    def generate_header(self):
        header = {"Authorization": f"bearer {self.token}"}
        return header

    def get_request(self, endpoint: str):
        r = requests.get(
            self.generate_url(endpoint=endpoint), headers=self.generate_header()
        )
        response = r.json()
        return response

    def post_request(
        self, endpoint: str, payload: dict, headers=None, auth: tuple = None
    ):
        if not headers:
            headers = self.generate_header()

        r = requests.post(
            self.generate_url(endpoint=endpoint),
            headers=headers,
            data=payload,
            auth=auth,
        )
        response = r.json()
        return response

    def get_user_apps(self, user_email: str):
        pass

    def get_user_id_from_email(self, user_email: str):
        pass

    def map_app_to_model(self, data):
        applications = []
        for d in data:
            mapped_data = {
                self.field_mapping[key]: value
                for key, value in d.items()
                if key in self.field_mapping
            }
            app = Application(**mapped_data)
            applications.append(app)

        return applications
