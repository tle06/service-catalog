import logging
from scim.utils.generic_idp import GenericIDP

logger = logging.getLogger(__name__)


class OLClient(GenericIDP):
    field_mapping = {
        "name": "name",
        "id": "app_id",
        "icon_url": "icon_url",
    }

    def generate_token(self):
        r = self.post_request(
            endpoint="auth/oauth2/v2/token",
            payload={"grant_type": "client_credentials"},
            auth=(self.client_id, self.client_secret),
        )

        try:
            self.token = r["access_token"]
        except Exception:
            logger.error(f"IDP access token not found, error logging:{r}")

    def get_app_for_user_id(self, user_id: int):
        data = self.get_request(endpoint=f"api/2/users/{user_id}/apps")
        map = self.map_app_to_model(data=data)
        return map

    def get_user_apps(self, user_email: str):
        user_id = self.get_user_id_from_email(user_email=user_email)
        data = self.get_request(endpoint=f"api/2/users/{user_id}/apps")
        map = self.map_app_to_model(data=data)
        return map

    def get_user_id_from_email(self, user_email: str):
        r = self.get_request(endpoint=f"api/2/users?email={user_email}")

        return r[0]["id"]
