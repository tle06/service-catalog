from scim.utils.onelogin_client import OLClient
from service.models import (
    Application,
    IdentityProvider,
    Department,
    SubDepartment,
    Title,
)
from django.conf import settings
from core.settings import logger
from hardware.models import RuleMapping, HardwareMapping


def get_idp_client():
    idp_client = None
    if settings.ACTIVATE_DUMP_APP_FROM_IDP:
        if settings.IDP_PROVIDER_NAME == "onelogin":
            idp_client = OLClient(
                client_id=settings.IDP_CLIENT_ID,
                client_secret=settings.IDP_CLIENT_SECRET,
                root_url=f"{settings.IDP_ROOT_URL.scheme}://{settings.IDP_ROOT_URL.netloc}",
            )

    return idp_client


def create_update_app(user_email: str, current_user):
    idp_client = get_idp_client()
    result = []
    if idp_client:
        idp_name = IdentityProvider.objects.filter(
            name=settings.IDP_PROVIDER_NAME.lower()
        ).first()

        for app in idp_client.get_user_apps(user_email=user_email):
            exist_app = Application.objects.filter(app_id=app.app_id).first()

            if not exist_app:
                new_app = Application(
                    name=app,
                    app_id=app.app_id,
                    icon_url=app.icon_url,
                    createdByUserId=current_user,
                    idp=idp_name,
                )
                new_app.save()
                logger.info(f"new app added:{new_app.name}")
                result.append(new_app.id)

            if exist_app:
                result.append(exist_app.id)
                changes = False
                if not exist_app.name == app.name:
                    exist_app.name = app.name
                    changes = True
                if not exist_app.icon_url == app.icon_url:
                    exist_app.icon_url = app.icon_url
                    changes = True
                if not exist_app.idp == idp_name:
                    exist_app.idp = idp_name
                    changes = True
                if changes:
                    exist_app.save()

    return result


def create_rule_mapping(
    department_id: int, subdepartment_id: int, title_id: int, current_user
):
    dep_object = Department.objects.filter(pk=department_id).first()
    subdep_object = SubDepartment.objects.filter(pk=subdepartment_id).first()
    title_object = Title.objects.filter(pk=title_id).first()

    existing_rule = RuleMapping.objects.filter(
        department=dep_object, subdepartment=subdep_object, title=title_object
    ).first()

    if not existing_rule:
        if dep_object and subdep_object and title_object:
            existing_rule = RuleMapping(
                department=dep_object,
                subdepartment=subdep_object,
                title=title_object,
                createdByUserId=current_user,
            )
            existing_rule.save()
            logger.info(f"New rule added:{existing_rule}")

            new_hardware_mapping = HardwareMapping(
                rule=existing_rule, createdByUserId=current_user
            )
            new_hardware_mapping.save()
            logger.info(f"Hardware Mapping created:{new_hardware_mapping}")

        else:
            logger.info(
                f"One of the value department,subdepartment, title is not set:{department_id},{subdepartment_id},{title_id}"
            )
    else:
        logger.info(f"Mapping rule already exist:{existing_rule}")
        hardware_mapping = HardwareMapping.objects.filter(rule=existing_rule).first()

        if not hardware_mapping:
            new_hardware_mapping = HardwareMapping(
                rule=existing_rule, createdByUserId=current_user
            )
            new_hardware_mapping.save()
            logger.info(f"Hardware Mapping created:{new_hardware_mapping}")
            return new_hardware_mapping
        return hardware_mapping
