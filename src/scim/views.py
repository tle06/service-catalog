from django.contrib.auth.models import Group
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from scim.serializers import SCIMGroupSerializer, SCIMUserSerializer
from service.models import User, Company, Department, SubDepartment, Title
from scim.utils.utils import create_update_app, create_rule_mapping
import re
from building.models import Building
from core.settings import logger


class SCIMGeneriqueViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAdminUser]

    def build_header_payload(self, count):
        payload = {
            "totalResults": count,
            "itemsPerPage": count,
            "startIndex": 1,
            "schemas": [
                "urn:ietf:params:scim:schemas:core:2.0:User",
                "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",
                "urn:scim:my:custom:schema",
            ],
            "Resources": [],
        }

        return payload

    def get_queryset(self):
        filter = self.request.query_params.get("filter", None)

        if filter is not None:
            params = filter.split(" ")
            params[0]
            params[1]
            value = params[2].strip(' " " ')

            if value is not None:
                return super().get_queryset().filter(email=value)
        return super().get_queryset()


class SCIMUserViewSet(SCIMGeneriqueViewSet):
    """
    SCIM endpoint that allows users to be viewed or edited.
    """

    queryset = User.objects.all()
    serializer_class = SCIMUserSerializer

    def list(self, request, *args, **kwargs):
        res = super(SCIMUserViewSet, self).list(request, *args, **kwargs)
        data = res.data["results"]
        count = res.data["count"]
        payload = self.build_header_payload(count=count)
        payload["Resources"] = data

        return Response(payload)

    def create(self, request, *args, **kwargs):
        data = request.data
        serial_data = {}
        enterprise_data = data.get("urn:scim:schemas:extension:enterprise:2.0")
        custom_shema = data.get("urn:scim:my:custom:schema")

        firstname = data.get("name").get("givenName")
        lastname = data.get("name").get("familyName")
        email = data.get("emails")[0].get("value")

        if data.get("groups"):
            groups = data.get("groups")
            serial_data.update({"groups": groups})

        if data.get("photo"):
            photo = data.get("photo")[0].get("value")
            serial_data.update({"photo": photo})

        if data.get("title"):
            title = data.get("title")
            existing_title = Title.objects.filter(name=title).first()
            title_object = existing_title

            if not existing_title:
                title_object = Title(name=title, createdByUserId=self.request.user)
                title_object.save()
            serial_data.update({"title": title_object.id})

        if enterprise_data:
            if enterprise_data.get("employeeNumber"):
                employeeNumber = enterprise_data.get("employeeNumber")
                serial_data.update({"employeeNumber": employeeNumber})

            department_object = None
            if enterprise_data.get("department"):
                department = enterprise_data.get("department")
                existing_dep = Department.objects.filter(name=department).first()
                department_object = existing_dep

                if not existing_dep:
                    department_object = Department(
                        name=department, createdByUserId=self.request.user
                    )
                    department_object.save()

                serial_data.update({"department": department_object.id})

            if enterprise_data.get("organization"):
                organization = enterprise_data.get("organization")
                existing_company = Company.objects.filter(name=organization).first()
                company_object = existing_company

                if not existing_company:
                    company_object = Company(
                        name=organization, createdByUserId=self.request.user
                    )
                    company_object.save()

                serial_data.update({"company": company_object.id})

            if enterprise_data.get("division"):
                subdepartment = enterprise_data.get("division")
                existing_sub_dep = SubDepartment.objects.filter(
                    name=subdepartment
                ).first()
                sub_dep_object = existing_sub_dep

                if not existing_sub_dep and department_object:
                    sub_dep_object = SubDepartment(
                        name=subdepartment,
                        createdByUserId=self.request.user,
                        department=department_object,
                    )
                    sub_dep_object.save()

                serial_data.update({"subdepartment": sub_dep_object.id})

            if enterprise_data.get("manager"):
                manager_id = enterprise_data.get("manager").get("value")

                if manager_id:
                    existing_user = User.objects.filter(pk=manager_id).first()

                    if existing_user:
                        serial_data.update({"manager": existing_user.id})

        is_active = data.get("active")
        try:
            create_update_app(current_user=self.request.user, user_email=email)
        except Exception:
            logger.error(msg="Can't create application from IDP")

        try:
            hardware_mapping = create_rule_mapping(
                department_id=serial_data.get("department"),
                subdepartment_id=serial_data.get("subdepartment"),
                title_id=serial_data.get("title"),
                current_user=self.request.user,
            )
            serial_data.update({"hardware_mapping": hardware_mapping})
        except Exception:
            logger.error(msg="Can't create rule mapping from IDP")

        if custom_shema:
            building_id = custom_shema.get("building_id")

            if building_id:
                existing_building_id = Building.objects.filter(name=building_id).first()

                if not existing_building_id:
                    building_id = Building(
                        name=building_id, createdByUserId=self.request.user
                    )
                    building_id.save()
                    serial_data.update({"building_id": building_id.id})
                serial_data.update({"building_id": existing_building_id.id})

        serial_data.update(
            {
                "email": email,
                "first_name": firstname,
                "last_name": lastname,
                "is_active": is_active,
            }
        )

        serializer = self.get_serializer(data=serial_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        data = request.data

        enterprise_data = data["urn:scim:schemas:extension:enterprise:2.0"]
        custom_shema = data.get("urn:scim:my:custom:schema")

        serial_data = {}
        department_object = None

        firstname = data.get("name").get("givenName")
        lastname = data.get("name").get("familyName")
        email = data.get("emails")[0].get("value")

        if data.get("groups"):
            groups = data.get("groups")
            serial_data.update({"groups": groups})

        if data.get("photo"):
            photo = data.get("photo")[0].get("value")
            serial_data.update({"photo": photo})

        if data.get("title"):
            title = data.get("title")
            existing_title = Title.objects.filter(name=title).first()
            title_object = existing_title

            if not existing_title:
                title_object = Title(name=title, createdByUserId=self.request.user)
                title_object.save()
            instance.title = title_object

        if enterprise_data:
            if enterprise_data.get("employeeNumber"):
                employeeNumber = enterprise_data.get("employeeNumber")
                instance.employeeNumber = employeeNumber

            department_object = None
            if enterprise_data.get("department"):
                department = enterprise_data.get("department")
                existing_dep = Department.objects.filter(name=department).first()
                department_object = existing_dep

                if not existing_dep:
                    department_object = Department(
                        name=department, createdByUserId=self.request.user
                    )
                    department_object.save()

                instance.department = department_object

            if enterprise_data.get("organization"):
                organization = enterprise_data.get("organization")
                existing_company = Company.objects.filter(name=organization).first()

                if not existing_company:
                    new_company = Company(
                        name=organization, createdByUserId=self.request.user
                    )
                    new_company.save()
                    instance.company = new_company

                instance.company = existing_company

            if enterprise_data.get("division"):
                subdepartment = enterprise_data.get("division")
                existing_sub_dep = SubDepartment.objects.filter(
                    name=subdepartment
                ).first()

                if not existing_sub_dep and department_object:
                    new_sub_dep = SubDepartment(
                        name=subdepartment,
                        createdByUserId=self.request.user,
                        department=department_object,
                    )
                    new_sub_dep.save()
                    instance.subdepartment = new_sub_dep

                instance.subdepartment = existing_sub_dep

            if enterprise_data.get("manager"):
                if enterprise_data.get("manager").get("value"):
                    manager_id = enterprise_data.get("manager").get("value")
                    if manager_id:
                        existing_user = User.objects.filter(pk=manager_id).first()

                        if existing_user:
                            instance.manager = existing_user
            try:
                hardware_mapping = create_rule_mapping(
                    department_id=instance.department.id,
                    subdepartment_id=instance.subdepartment.id,
                    title_id=instance.title.id,
                    current_user=self.request.user,
                )
                instance.hardware_mapping = hardware_mapping
            except Exception as e:
                logger.error(msg=f"Can't create rule mapping from IDP:{e}")

        if custom_shema:
            building_id = custom_shema.get("building_id")

            if building_id:
                existing_building_id = Building.objects.filter(name=building_id).first()

                if not existing_building_id:
                    building_id = Building(
                        name=building_id, createdByUserId=self.request.user
                    )
                    building_id.save()

                    instance.building_id = building_id
                instance.building_id = existing_building_id

        app_ids = []
        try:
            app_ids = create_update_app(
                current_user=self.request.user, user_email=email
            )
        except Exception as e:
            logger.error(msg=f"Can't create application from IDP:{e}")

        for id in app_ids:
            instance.application.add(id)

        current_user_apps = instance.application.all()
        for app in current_user_apps:
            if app.id not in app_ids:
                instance.application.remove(app.id)

        is_active = data.get("active")

        serial_data.update(
            {
                "email": email,
                "first_name": firstname,
                "last_name": lastname,
                "is_active": is_active,
            }
        )

        serializer = self.get_serializer(instance, data=serial_data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class SCIMGroupViewSet(SCIMGeneriqueViewSet):
    """
    SCIM endpoint that allows groups to be viewed or edited.
    """

    queryset = Group.objects.all()
    serializer_class = SCIMGroupSerializer

    def list(self, request, *args, **kwargs):
        res = super(SCIMGroupViewSet, self).list(request, *args, **kwargs)
        data = res.data["results"]
        count = res.data["count"]
        payload = self.build_header_payload(count=count)
        payload["Resources"] = data

        return Response(payload)

    def create(self, request, *args, **kwargs):
        data = request.data
        serial_data = {}
        name = data["displayName"]

        serial_data.update({"name": name})

        serializer = self.get_serializer(data=serial_data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        data = request.data

        serial_data = {"name": instance.name}

        operations = data["Operations"]

        if operations:
            for operation in operations:
                action = None
                path = None
                user_id = None
                user = None

                if "op" in operation:
                    action = operation["op"]

                if "value" in operation:
                    operation["value"]

                if "path" in operation:
                    path = operation["path"]
                    user_id = re.findall(r'"([^"]*)"', path)
                    user_id = user_id[0]

                if user_id:
                    user = User.objects.filter(pk=user_id).first()

                if user:
                    user.groups.add(instance)

                if action == "add" and user:
                    user.groups.add(instance)

                if action == "remove":
                    user.groups.remove(instance)

        serializer = self.get_serializer(instance, data=serial_data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
