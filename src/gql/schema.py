import graphene
from graphene import ObjectType
from graphene_django import DjangoListField
from gql.types import (
    HardwareType,
    HardwareCategoryType,
    ContractType,
    GroupType,
    UserType,
    DepartmentType,
    SubDepartmentType,
    PricingType,
    ProviderType,
    ConfigurationType,
    ContactType,
    ApplicationType,
    ContractDocumentType,
    IdentityProviderType,
    ServiceType,
    ServiceCategoryType,
    TitleType,
    RuleMappingType,
    HardwareMappingType,
    BuildingType,
    AccessControlRecordType,
    ControllerDeviceType,
)
from gql.mutations import (
    CreateUserMutation,
    CreateDepartmentMutation,
    UpdateDepartmentMutation,
    DeleteDepartmentMutation,
    CreateSubDepartmentMutation,
    UpdateSubDepartmentMutation,
    DeleteSubDepartmentMutation,
    CreateConfigurationMutation,
    UpdateConfigurationMutation,
    DeleteConfigurationMutation,
    CreateServiceCategoryMutation,
    UpdateServiceCategoryMutation,
    DeleteServiceCategoryMutation,
    CreateIdentityProviderMutation,
    UpdateIdentityProviderMutation,
    DeleteIdentityProviderMutation,
    CreateProviderMutation,
    UpdateProviderMutation,
    DeleteProviderMutation,
    CreateCompanyMutation,
    UpdateCompanyMutation,
    DeleteCompanyMutation,
    CreateContractMutation,
    UpdateContractMutation,
    DeleteContractMutation,
    DeleteContractDocumentMutation,
    CreatePricingMutation,
    UpdatePricingMutation,
    DeletePricingMutation,
    CreateServiceMutation,
    UpdateServiceMutation,
    DeleteServiceMutation,
    CreateContactMutation,
    UpdateContactMutation,
    DeleteContactMutation,
    CreateHardwareCategoryMutation,
    UpdateHardwareCategoryMutation,
    DeleteHardwareCategoryMutation,
    CreateHardwareMutation,
    UpdateHardwareMutation,
    DeleteHardwareMutation,
    CreateApplicationMutation,
    UpdateApplicationMutation,
    DeleteApplicationMutation,
    CreateTitleMutation,
    UpdateTitleMutation,
    DeleteTitleMutation,
    CreateRuleMappingMutation,
    UpdateRuleMappingMutation,
    DeleteRuleMappingMutation,
    CreateHardwareMappingMutation,
    UpdateHardwareMappingMutation,
    DeleteHardwareMappingMutation,
    CreateBuildingMutation,
    UpdateBuildingMutation,
    DeleteBuildingMutation,
    CreateAccessControlRecordMutation,
    UpdateAccessControlRecordMutation,
    DeleteAccessControlRecordMutation,
    CreateControllerMutation,
    UpdateControllerMutation,
    DeleteControllerMutation,
)

from service.models import (
    User,
    Department,
    SubDepartment,
    Pricing,
    Provider,
    Service,
    Application,
    ContractDocument,
    Configuration,
    Contact,
    Contract,
    ServiceCategory,
    IdentityProvider,
    Title,
)
from django.contrib.auth.models import Group
from building.models import Building, AccessControlRecord, Controller
from hardware.models import Hardware, HardwareCategory, RuleMapping, HardwareMapping
from graphql_jwt.decorators import login_required, superuser_required
import graphql_jwt


class Query(ObjectType):
    viewer = graphene.Field(UserType, token=graphene.String(required=True))

    users = DjangoListField(UserType)
    user = graphene.Field(UserType, id=graphene.ID())

    groups = DjangoListField(GroupType)
    group = graphene.Field(GroupType, id=graphene.ID())

    departments = DjangoListField(DepartmentType)
    department = graphene.Field(DepartmentType, id=graphene.ID())

    subdepartments = DjangoListField(SubDepartmentType)
    subdepartment = graphene.Field(SubDepartmentType, id=graphene.ID())

    titles = DjangoListField(TitleType)
    title = graphene.Field(TitleType, id=graphene.ID())

    services = DjangoListField(ServiceType)
    service = graphene.Field(ServiceType, id=graphene.ID())

    providers = DjangoListField(ProviderType)
    provider = graphene.Field(ProviderType, id=graphene.ID())

    contracts = DjangoListField(ContractType)
    contract = graphene.Field(ContractType, id=graphene.ID())

    contracts_documents = DjangoListField(ContractDocumentType)
    contract_document = graphene.Field(ContractDocumentType, id=graphene.ID())

    pricings = DjangoListField(PricingType)
    pricing = graphene.Field(PricingType, id=graphene.ID())

    configurationa = DjangoListField(ConfigurationType)
    configuration = graphene.Field(ConfigurationType, id=graphene.ID())

    services_categories = DjangoListField(ServiceCategoryType)
    service_category = graphene.Field(ServiceCategoryType, id=graphene.ID())

    identities_providers = DjangoListField(IdentityProviderType)
    identity_provider = graphene.Field(IdentityProviderType, id=graphene.ID())

    contacts = DjangoListField(ContactType)
    contact = graphene.Field(ContactType, id=graphene.ID())

    hardwares_categories = DjangoListField(HardwareCategoryType)
    hardware_category = graphene.Field(HardwareCategoryType, id=graphene.ID())

    hardwares = DjangoListField(HardwareType)
    hardware = graphene.Field(HardwareType, id=graphene.ID())

    applications = DjangoListField(ApplicationType)
    application = graphene.Field(ApplicationType, id=graphene.ID())

    rules_mapping = DjangoListField(RuleMappingType)
    rule_mapping = graphene.Field(RuleMappingType, id=graphene.ID())

    hardwares_mappings = DjangoListField(HardwareMappingType)
    hardware_mapping = graphene.Field(HardwareMappingType, id=graphene.ID())

    buildings = DjangoListField(BuildingType)
    building = graphene.Field(BuildingType, id=graphene.ID())

    access_control_records = DjangoListField(AccessControlRecordType)
    access_control_record = graphene.Field(AccessControlRecordType, id=graphene.ID())

    controllers = DjangoListField(ControllerDeviceType)
    controller = graphene.Field(ControllerDeviceType, id=graphene.ID())

    @login_required
    def resolve_viewer(self, info, **kwargs):
        return info.context.user

    @superuser_required
    def resolve_users(root, info, **kwargs):
        return User.objects.all()

    @superuser_required
    def resolve_user(root, info, id, **kwargs):
        return User.objects.get(pk=id)

    @superuser_required
    def resolve_groups(root, info, **kwargs):
        return Group.objects.all()

    @superuser_required
    def resolve_group(root, info, id, **kwargs):
        return Group.objects.get(pk=id)

    @login_required
    def resolve_departments(root, info, **kwargs):
        return Department.objects.all()

    @login_required
    def resolve_department(root, info, id, **kwargs):
        return Department.objects.get(pk=id)

    @login_required
    def resolve_subdepartments(root, info, **kwargs):
        return SubDepartment.objects.all()

    @login_required
    def resolve_subdepartment(root, info, id, **kwargs):
        return SubDepartment.objects.get(pk=id)

    @login_required
    def resolve_services(root, info, **kwargs):
        return Service.objects.all()

    @login_required
    def resolve_service(root, info, id, **kwargs):
        return Service.objects.get(pk=id)

    @login_required
    def resolve_providers(root, info, **kwargs):
        return Provider.objects.all()

    @login_required
    def resolve_provider(root, info, id, **kwargs):
        return Provider.objects.get(pk=id)

    @login_required
    def resolve_contracts(root, info, **kwargs):
        return Contract.objects.all()

    @login_required
    def resolve_contract(root, info, id, **kwargs):
        return Contract.objects.get(pk=id)

    @login_required
    def resolve_contracts_documents(root, info, **kwargs):
        return ContractDocument.objects.all()

    @login_required
    def resolve_contract_document(root, info, id, **kwargs):
        return ContractDocument.objects.get(pk=id)

    @login_required
    def resolve_pricings(root, info, **kwargs):
        return Pricing.objects.all()

    @login_required
    def resolve_pricing(root, info, id, **kwargs):
        return Pricing.objects.get(pk=id)

    @login_required
    def resolve_services_categories(root, info, **kwargs):
        return ServiceCategory.objects.all()

    @login_required
    def resolve_service_category(root, info, id, **kwargs):
        return ServiceCategory.objects.get(pk=id)

    @login_required
    def resolve_configurations(root, info, **kwargs):
        return Configuration.objects.all()

    @login_required
    def resolve_configuration(root, info, id, **kwargs):
        return Configuration.objects.get(pk=id)

    @login_required
    def resolve_services(root, info, **kwargs):
        return Service.objects.all()

    @login_required
    def resolve_service(root, info, id, **kwargs):
        return Service.objects.get(pk=id)

    @login_required
    def resolve_identities_providers(root, info, **kwargs):
        return IdentityProvider.objects.all()

    @login_required
    def resolve_identity_provider(root, info, id, **kwargs):
        return IdentityProvider.objects.get(pk=id)

    @login_required
    def resolve_contacts(root, info, **kwargs):
        return Contact.objects.all()

    @login_required
    def resolve_contact(root, info, id, **kwargs):
        return Contact.objects.get(pk=id)

    @login_required
    def resolve_hardwares_categories(root, info, **kwargs):
        return HardwareCategory.objects.all()

    @login_required
    def resolve_hardware_category(root, info, id, **kwargs):
        return HardwareCategory.objects.get(pk=id)

    @login_required
    def resolve_hardwares(root, info, **kwargs):
        return Hardware.objects.all()

    @login_required
    def resolve_hardware(root, info, id, **kwargs):
        return Hardware.objects.get(pk=id)

    @superuser_required
    def resolve_applications(root, info, **kwargs):
        return Application.objects.all()

    @superuser_required
    def resolve_application(root, info, id, **kwargs):
        return Application.objects.get(pk=id)

    @superuser_required
    def resolve_titles(root, info, **kwargs):
        return Title.objects.all()

    @superuser_required
    def resolve_title(root, info, id, **kwargs):
        return Title.objects.get(pk=id)

    @superuser_required
    def resolve_rules_mapping(root, info, **kwargs):
        return RuleMapping.objects.all()

    @superuser_required
    def resolve_rule_mapping(root, info, id, **kwargs):
        return RuleMapping.objects.get(pk=id)

    @superuser_required
    def resolve_hardwares_mappings(root, info, **kwargs):
        return HardwareMapping.objects.all()

    @superuser_required
    def resolve_hardware_mapping(root, info, id, **kwargs):
        return HardwareMapping.objects.get(pk=id)

    @superuser_required
    def resolve_buildings(root, info, **kwargs):
        return Building.objects.all()

    @superuser_required
    def resolve_building(root, info, id, **kwargs):
        return Building.objects.get(pk=id)

    @superuser_required
    def resolve_access_control_records(root, info, **kwargs):
        return AccessControlRecord.objects.all()

    @superuser_required
    def resolve_access_control_record(root, info, id, **kwargs):
        return AccessControlRecord.objects.get(pk=id)

    @superuser_required
    def resolve_controllers(root, info, **kwargs):
        return Controller.objects.all()

    @superuser_required
    def resolve_controller(root, info, id, **kwargs):
        return Controller.objects.get(pk=id)


class Mutation(ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

    create_user = CreateUserMutation.Field()

    create_department = CreateDepartmentMutation.Field()
    update_department = UpdateDepartmentMutation.Field()
    delete_department = DeleteDepartmentMutation.Field()

    create_subdepartment = CreateSubDepartmentMutation.Field()
    update_subdepartment = UpdateSubDepartmentMutation.Field()
    delete_subdepartment = DeleteSubDepartmentMutation.Field()

    create_configuration = CreateConfigurationMutation.Field()
    update_configuration = UpdateConfigurationMutation.Field()
    delete_configuration = DeleteConfigurationMutation.Field()

    create_service_category = CreateServiceCategoryMutation.Field()
    update_service_category = UpdateServiceCategoryMutation.Field()
    delete_service_category = DeleteServiceCategoryMutation.Field()

    create_identity_provider = CreateIdentityProviderMutation.Field()
    update_identity_provider = UpdateIdentityProviderMutation.Field()
    delete_identity_provider = DeleteIdentityProviderMutation.Field()

    create_provider = CreateProviderMutation.Field()
    update_provider = UpdateProviderMutation.Field()
    delete_provider = DeleteProviderMutation.Field()

    create_company = CreateCompanyMutation.Field()
    update_company = UpdateCompanyMutation.Field()
    delete_company = DeleteCompanyMutation.Field()

    create_contract = CreateContractMutation.Field()
    update_contract = UpdateContractMutation.Field()
    delete_contract = DeleteContractMutation.Field()

    delete_contract_document = DeleteContractDocumentMutation.Field()

    create_pricing = CreatePricingMutation.Field()
    update_pricing = UpdatePricingMutation.Field()
    delete_pricing = DeletePricingMutation.Field()

    create_service = CreateServiceMutation.Field()
    update_service = UpdateServiceMutation.Field()
    delete_service = DeleteServiceMutation.Field()

    create_contact = CreateContactMutation.Field()
    update_contact = UpdateContactMutation.Field()
    delete_contact = DeleteContactMutation.Field()

    create_hardware_category = CreateHardwareCategoryMutation.Field()
    update_hardware_category = UpdateHardwareCategoryMutation.Field()
    delete_hardware_category = DeleteHardwareCategoryMutation.Field()

    create_hardware = CreateHardwareMutation.Field()
    update_hardware = UpdateHardwareMutation.Field()
    delete_hardware = DeleteHardwareMutation.Field()

    create_application = CreateApplicationMutation.Field()
    update_application = UpdateApplicationMutation.Field()
    delete_application = DeleteApplicationMutation.Field()

    create_title = CreateTitleMutation.Field()
    update_title = UpdateTitleMutation.Field()
    delete_title = DeleteTitleMutation.Field()

    create_rule_mapping = CreateRuleMappingMutation.Field()
    update_rule_mapping = UpdateRuleMappingMutation.Field()
    delete_rule_mapping = DeleteRuleMappingMutation.Field()

    create_hardware_mapping = CreateHardwareMappingMutation.Field()
    update_hardware_mapping = UpdateHardwareMappingMutation.Field()
    delete_hardware_mapping = DeleteHardwareMappingMutation.Field()

    create_building = CreateBuildingMutation.Field()
    update_building = UpdateBuildingMutation.Field()
    delete_building = DeleteBuildingMutation.Field()

    create_access_control_record = CreateAccessControlRecordMutation.Field()
    update_access_control_record = UpdateAccessControlRecordMutation.Field()
    delete_access_control_record = DeleteAccessControlRecordMutation.Field()

    create_controller = CreateControllerMutation.Field()
    update_controller = UpdateControllerMutation.Field()
    delete_controller = DeleteControllerMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
