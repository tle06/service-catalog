import graphene

from service.models import (
    User,
    Department,
    SubDepartment,
    Pricing,
    Provider,
    Service,
    Application,
    ContractDocument,
    Configuration,
    Contact,
    Contract,
    ServiceCategory,
    IdentityProvider,
    Company,
    Title,
)
from hardware.models import Hardware, HardwareCategory, RuleMapping, HardwareMapping
from building.models import Building, AccessControlRecord, Controller

from gql.types import (
    HardwareType,
    HardwareCategoryType,
    ContractType,
    UserType,
    DepartmentType,
    SubDepartmentType,
    PricingType,
    ProviderType,
    ConfigurationType,
    ContactType,
    ApplicationType,
    ContractDocumentType,
    IdentityProviderType,
    ServiceType,
    ServiceCategoryType,
    CompanyType,
    TitleType,
    RuleMappingType,
    HardwareMappingType,
    BuildingType,
    AccessControlRecordType,
    ControllerDeviceType,
)
from gql.inputs import (
    UserInput,
    DepartmentInput,
    SubDepartmentInput,
    ServiceInput,
    ServiceCategoryInput,
    PricingInput,
    ProviderInput,
    IdentityProviderInput,
    CompanyInput,
    ConfigurationInput,
    ContactInput,
    ContractInput,
    HardwareCategoryInput,
    HardwareInput,
    ApplicationInput,
    TitleInput,
    RuleMappingInput,
    HardwareMappingInput,
    BuildingInput,
    AccessControlRecordInput,
    ControllerInput,
)
from graphql_jwt.decorators import login_required, superuser_required

# https://www.twilio.com/blog/graphql-apis-django-graphene
# ---------------------------------------------------------------------------- #
#                                     user                                     #
# ---------------------------------------------------------------------------- #


class CreateUserMutation(graphene.Mutation):
    class Arguments:
        object_data = UserInput()

    user = graphene.Field(UserType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = User.objects.create(
            email=object_data.email,
            first_name=object_data.first_name,
            last_name=object_data.last_name,
            is_active=object_data.is_active,
            is_staff=object_data.is_staff,
            is_admin=object_data.is_admin,
        )

        if object_data.applications is not None:
            relation_set = []
            for relation_id in object_data.application:
                relation_object = Application.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.application.set(relation_set)

        return CreateUserMutation(user=object_instance)


class UpdateUserMutation(graphene.Mutation):
    class Arguments:
        object_data = UserInput()

    user = graphene.Field(UserType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = User.objects.get(pk=object_data.id)
        if object_instance:
            object_instance.email = object_data.email
            object_instance.first_name = object_data.first_name
            object_instance.last_name = object_data.last_name

            if object_data.application is not None:
                relation_set = []
                for relation_id in object_data.application:
                    relation_object = Application.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.application.set(relation_set)
            object_instance.save()
            return UpdateUserMutation(user=object_instance)
        return None


class DeleteUserMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    user = graphene.Field(UserType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = User.objects.get(pk=id)
        object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                  department                                  #
# ---------------------------------------------------------------------------- #


class CreateDepartmentMutation(graphene.Mutation):
    class Arguments:
        object_data = DepartmentInput()

    department = graphene.Field(DepartmentType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Department.objects.create(
            name=object_data.name, createdByUserId=info.context.user
        )

        return CreateDepartmentMutation(department=object_instance)


class UpdateDepartmentMutation(graphene.Mutation):
    class Arguments:
        object_data = DepartmentInput()

    department = graphene.Field(DepartmentType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Department.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.save()
            return UpdateDepartmentMutation(department=object_instance)
        return UpdateDepartmentMutation(department=None)


class DeleteDepartmentMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    department = graphene.Field(DepartmentType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = Department.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                sub-department                                #
# ---------------------------------------------------------------------------- #


class CreateSubDepartmentMutation(graphene.Mutation):
    class Arguments:
        object_data = SubDepartmentInput()

    subdepartment = graphene.Field(SubDepartmentType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = SubDepartment.objects.create(
            name=object_data.name, createdByUserId=info.context.user
        )

        if object_data.department is not None:
            department = Department.objects.get(pk=object_data.department)
            if department:
                object_instance.department = department

        return CreateSubDepartmentMutation(subdepartment=object_instance)


class UpdateSubDepartmentMutation(graphene.Mutation):
    class Arguments:
        object_data = SubDepartmentInput()

    subdepartment = graphene.Field(SubDepartmentType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = SubDepartment.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.department = object_data.department
            object_instance.save()
            return UpdateSubDepartmentMutation(subdepartment=object_instance)
        return None


class DeleteSubDepartmentMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    subdepartment = graphene.Field(SubDepartmentType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = SubDepartment.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                     Title                                    #
# ---------------------------------------------------------------------------- #


class CreateTitleMutation(graphene.Mutation):
    class Arguments:
        object_data = TitleInput()

    title = graphene.Field(TitleType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Title.objects.create(
            name=object_data.name, createdByUserId=info.context.user
        )

        return CreateTitleMutation(title=object_instance)


class UpdateTitleMutation(graphene.Mutation):
    class Arguments:
        object_data = TitleInput()

    title = graphene.Field(TitleType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Title.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.save()
            return UpdateTitleMutation(title=object_instance)
        return UpdateTitleMutation(title=None)


class DeleteTitleMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    title = graphene.Field(TitleType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = Title.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                 Configuration                                #
# ---------------------------------------------------------------------------- #


class CreateConfigurationMutation(graphene.Mutation):
    class Arguments:
        object_data = ConfigurationInput()

    configuration = graphene.Field(ConfigurationType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Configuration.objects.create(
            name=object_data.name,
            description=object_data.description,
            category=object_data.category,
            key=object_data.key,
            value=object_data.value,
            enabled=object_data.enabled,
        )

        return CreateConfigurationMutation(configuration=object_instance)


class UpdateConfigurationMutation(graphene.Mutation):
    class Arguments:
        object_data = ConfigurationInput()

    configuration = graphene.Field(ConfigurationType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Configuration.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.description = object_data.description
            object_instance.category = object_data.category
            object_instance.key = object_data.key
            object_instance.value = object_data.value
            object_instance.enabled = object_data.enabled
            object_instance.save()
            return UpdateConfigurationMutation(configuration=object_instance)
        return None


class DeleteConfigurationMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    configuration = graphene.Field(ConfigurationType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = Configuration.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                               Service category                               #
# ---------------------------------------------------------------------------- #


class CreateServiceCategoryMutation(graphene.Mutation):
    class Arguments:
        object_data = ServiceCategoryInput()

    service_category = graphene.Field(ServiceCategoryType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = ServiceCategory.objects.create(
            name=object_data.name, createdByUserId=info.context.user
        )

        return CreateServiceCategoryMutation(service_category=object_instance)


class UpdateServiceCategoryMutation(graphene.Mutation):
    class Arguments:
        object_data = ServiceCategoryInput()

    service_catagory = graphene.Field(ServiceCategoryType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = ServiceCategory.objects.get(pk=object_data.id)
        if object_instance:
            object_instance.name = object_data.name
            object_instance.save()
            return UpdateServiceCategoryMutation(service_catagory=object_instance)
        return None


class DeleteServiceCategoryMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    service_category = graphene.Field(ServiceCategoryType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = ServiceCategory.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                               Identity provider                              #
# ---------------------------------------------------------------------------- #


class CreateIdentityProviderMutation(graphene.Mutation):
    class Arguments:
        object_data = IdentityProviderInput()

    identity_provider = graphene.Field(IdentityProviderType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = IdentityProvider.objects.create(
            name=object_data.name,
            icon_url=object_data.icon_url,
            service_url=object_data.service_url,
            createdByUserId=info.context.user,
        )

        return CreateIdentityProviderMutation(identity_provider=object_instance)


class UpdateIdentityProviderMutation(graphene.Mutation):
    class Arguments:
        object_data = IdentityProviderInput()

    identity_provider = graphene.Field(IdentityProviderType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = IdentityProvider.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.icon_url = object_data.icon_url
            object_instance.service_url = object_data.service_url
            object_instance.save()
            return UpdateIdentityProviderMutation(identity_provider=object_instance)
        return None


class DeleteIdentityProviderMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    identity_provider = graphene.Field(IdentityProviderType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = IdentityProvider.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                    Povider                                   #
# ---------------------------------------------------------------------------- #


class CreateProviderMutation(graphene.Mutation):
    class Arguments:
        object_data = ProviderInput()

    provider = graphene.Field(ProviderType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Provider.objects.create(
            name=object_data.name,
            email=object_data.email,
            phone=object_data.phone,
            vat_number=object_data.vat_number,
            address=object_data.address,
            street_number=object_data.street_number,
            city=object_data.city,
            zip_code=object_data.zip_code,
            country=object_data.country,
            createdByUserId=info.context.user,
        )

        if object_data.contact is not None:
            relation_set = []
            for relation_id in object_data.contact:
                relation_object = Contact.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.contact.set(relation_set)

        return CreateProviderMutation(provider=object_instance)


class UpdateProviderMutation(graphene.Mutation):
    class Arguments:
        object_data = ProviderInput()

    provider = graphene.Field(ProviderType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Provider.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.email = object_data.email
            object_instance.phone = object_data.phone
            object_instance.vat_number = object_data.vat_number
            object_instance.address = object_data.address
            object_instance.street_number = object_data.street_number
            object_instance.city = object_data.city
            object_instance.zip_code = object_data.zip_code
            object_instance.country = object_data.country

            if object_data.contact is not None:
                relation_set = []
                for relation_id in object_data.contact:
                    relation_object = Contact.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.contact.set(relation_set)
            object_instance.save()
            return UpdateProviderMutation(provider=object_instance)
        return None


class DeleteProviderMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    provider = graphene.Field(ProviderType)

    @classmethod
    @login_required
    def mutate(cls, root, info, id):
        object_instance = Provider.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                    Company                                   #
# ---------------------------------------------------------------------------- #


class CreateCompanyMutation(graphene.Mutation):
    class Arguments:
        object_data = CompanyInput()

    company = graphene.Field(CompanyType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Company.objects.create(name=object_data.name)

        return CreateCompanyMutation(company=object_instance)


class UpdateCompanyMutation(graphene.Mutation):
    class Arguments:
        object_data = CompanyInput()

    company = graphene.Field(CompanyType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Company.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.save()
            return UpdateCompanyMutation(company=object_instance)
        return None


class DeleteCompanyMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    company = graphene.Field(CompanyType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = Company.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                   Contract                                   #
# ---------------------------------------------------------------------------- #
class CreateContractMutation(graphene.Mutation):
    class Arguments:
        object_data = ContractInput()

    contract = graphene.Field(ContractType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Contract.objects.create(
            name=object_data.name,
            provider=object_data.provider,
            start_date=object_data.start_date,
            end_date=object_data.end_date,
            renewal_date=object_data.renewal_date,
            duration=object_data.duration,
            duration_unit=object_data.duration_unit,
            total_cost=object_data.total_cost,
            cost_unit=object_data.cost_unit,
            volume=object_data.volume,
            currency=object_data.currency,
            status=object_data.status,
            company=object_data.company,
            createdByUserId=info.context.user,
        )

        return CreateContractMutation(contract=object_instance)


class UpdateContractMutation(graphene.Mutation):
    class Arguments:
        object_data = ContractInput()

    Contract = graphene.Field(ContractType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Contract.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.provider = object_data.provider
            object_instance.start_date = object_data.start_date
            object_instance.end_date = object_data.end_date
            object_instance.renewal_date = object_data.renewal_date
            object_instance.duration = object_data.duration
            object_instance.duration_unit = object_data.duration_unit
            object_instance.total_cost = object_data.total_cost
            object_instance.cost_unit = object_data.cost_unit
            object_instance.volume = object_data.volume
            object_instance.currency = object_data.currency
            object_instance.status = object_data.status
            object_instance.company = object_data.company
            object_instance.save()
            return UpdateContractMutation(contract=object_instance)
        return None


class DeleteContractMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    contract = graphene.Field(ContractType)

    @classmethod
    @login_required
    def mutate(cls, root, info, id):
        object_instance = Contract.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                               ContractDocument                               #
# ---------------------------------------------------------------------------- #


class DeleteContractDocumentMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    contract_document = graphene.Field(ContractDocumentType)

    @classmethod
    @login_required
    def mutate(cls, root, info, id):
        object_instance = ContractDocument.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                    Pricing                                   #
# ---------------------------------------------------------------------------- #


class CreatePricingMutation(graphene.Mutation):
    class Arguments:
        object_data = PricingInput()

    pricing = graphene.Field(PricingType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Pricing.objects.create(
            name=object_data.name,
            cost_unit=object_data.cost_unit,
            total_cost=object_data.total_cost,
            volume=object_data.volume,
            pricing_date=object_data.pricing_date,
            currency=object_data.currency,
            status=object_data.status,
            time_unit=object_data.time_unit,
            createdByUserId=info.context.user,
        )

        return CreatePricingMutation(pricing=object_instance)


class UpdatePricingMutation(graphene.Mutation):
    class Arguments:
        object_data = PricingInput()

    pricing = graphene.Field(PricingType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Pricing.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.cost_unit = object_data.cost_unit
            object_instance.total_cost = object_data.total_cost
            object_instance.volume = object_data.volume
            object_instance.pricing_date = object_data.pricing_date
            object_instance.currency = object_data.currency
            object_instance.status = object_data.status
            object_instance.time_unit = object_data.time_unit
            object_instance.save()
            return UpdatePricingMutation(pricing=object_instance)
        return None


class DeletePricingMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    pricing = graphene.Field(PricingType)

    @classmethod
    @login_required
    def mutate(cls, root, info, id):
        object_instance = Pricing.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                    Service                                   #
# ---------------------------------------------------------------------------- #


class CreateServiceMutation(graphene.Mutation):
    class Arguments:
        object_data = ServiceInput()

    service = graphene.Field(ServiceType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Service.objects.create(
            name=object_data.name,
            description=object_data.description,
            department=object_data.department,
            subdepartment=object_data.subdepartment,
            supported_by=object_data.supported_by,
            tier=object_data.tier,
            provider=object_data.provider,
            status=object_data.status,
            health_check=object_data.health_check,
            icon_url=object_data.icon_url,
            company=object_data.company,
            identity_provider=object_data.identity_provider,
            service_url=object_data.service_url,
            service_website=object_data.service_website,
            category=object_data.category,
            createdByUserId=info.context.user,
            support_url=info.context.support_url,
        )

        # https://medium.com/analytics-vidhya/graphql-with-django-simple-yet-powerful-crud-part-2-bacce3668e35

        if object_data.service_relationships is not None:
            relation_set = []
            for relation_id in object_data.service_relationships:
                relation_object = Service.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.service_relationships.set(relation_set)

        if object_data.contract is not None:
            relation_set = []
            for relation_id in object_data.contract:
                relation_object = Contract.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.contract.set(relation_set)

        if object_data.pricing is not None:
            relation_set = []
            for relation_id in object_data.pricing:
                relation_object = Pricing.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.pricing.set(relation_set)

        if object_data.owner is not None:
            relation_set = []
            for relation_id in object_data.owner:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)
            object_instance.owner.set(relation_set)

        if object_data.administrator is not None:
            relation_set = []
            for relation_id in object_data.administrator:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)
            object_instance.administrator.set(relation_set)

        if object_data.procurement is not None:
            relation_set = []
            for relation_id in object_data.procurement:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)
            object_instance.procurement.set(relation_set)

        return CreateServiceMutation(service=object_instance)


class UpdateServiceMutation(graphene.Mutation):
    class Arguments:
        object_data = ServiceInput()

    service = graphene.Field(ServiceType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Service.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.description = object_data.description
            object_instance.department = object_data.department
            object_instance.subdepartment = object_data.subdepartment
            object_instance.supported_by = object_data.supported_by
            object_instance.tier = object_data.tier
            object_instance.provider = object_data.provider
            object_instance.status = object_data.status
            object_instance.health_check = object_data.health_check
            object_instance.icon_url = object_data.icon_url
            object_instance.company = object_data.company
            object_instance.identity_provider = object_data.identity_provider
            object_instance.service_url = object_data.service_url
            object_instance.service_website = object_data.service_website
            object_instance.category = object_data.category
            object_instance.support_url = object_data.support_url

            if object_data.service_relationships is not None:
                relation_set = []
                for relation_id in object_data.service_relationships:
                    relation_object = Service.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.service_relationships.set(relation_set)

            if object_data.contract is not None:
                relation_set = []
                for relation_id in object_data.contract:
                    relation_object = Contract.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.contract.set(relation_set)

            if object_data.pricing is not None:
                relation_set = []
                for relation_id in object_data.pricing:
                    relation_object = Pricing.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.pricing.set(relation_set)

            if object_data.owner is not None:
                relation_set = []
                for relation_id in object_data.owner:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.pricing.set(relation_set)

            if object_data.administrator is not None:
                relation_set = []
                for relation_id in object_data.administrator:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.administrator.set(relation_set)

            if object_data.procurement is not None:
                relation_set = []
                for relation_id in object_data.procurement:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.procurement.set(relation_set)

            object_instance.save()
            return UpdateServiceMutation(service=object_instance)
        return None


class DeleteServiceMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    service = graphene.Field(ServiceType)

    @classmethod
    @login_required
    def mutate(cls, root, info, id):
        object_instance = Service.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                    Contact                                   #
# ---------------------------------------------------------------------------- #


class CreateContactMutation(graphene.Mutation):
    class Arguments:
        object_data = ContactInput()

    contact = graphene.Field(ContactType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Contact.objects.create(
            name=object_data.name,
            email=object_data.email,
            firstname=object_data.firstname,
            lastname=object_data.lastname,
            phone=object_data.phone,
            createdByUserId=info.context.user,
        )

        return CreateContactMutation(contact=object_instance)


class UpdateContactMutation(graphene.Mutation):
    class Arguments:
        object_data = ContactInput()

    contact = graphene.Field(ContactType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Contact.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.email = object_data.email
            object_instance.firstname = object_data.firstname
            object_instance.lastname = object_data.lastname
            object_instance.phone = object_data.phone
            object_instance.save()
            return UpdateContactMutation(contact=object_instance)
        return None


class DeleteContactMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    contact = graphene.Field(ContactType)

    @classmethod
    @login_required
    def mutate(cls, root, info, id):
        object_instance = Contact.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                               Hardware category                              #
# ---------------------------------------------------------------------------- #


class CreateHardwareCategoryMutation(graphene.Mutation):
    class Arguments:
        object_data = HardwareCategoryInput()

    hardware_category = graphene.Field(HardwareCategoryType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = HardwareCategory.objects.create(name=object_data.name)

        return CreateHardwareCategoryMutation(hardware_category=object_instance)


class UpdateHardwareCategoryMutation(graphene.Mutation):
    class Arguments:
        object_data = HardwareCategoryInput()

    hardware_catagory = graphene.Field(HardwareCategoryType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = HardwareCategory.objects.get(pk=object_data.id)
        if object_instance:
            object_instance.name = object_data.name
            object_instance.save()
            return UpdateHardwareCategoryMutation(hardware_catagory=object_instance)
        return None


class DeleteHardwareCategoryMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    hardware_category = graphene.Field(HardwareCategoryType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = HardwareCategory.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                   Hardware                                   #
# ---------------------------------------------------------------------------- #
class CreateHardwareMutation(graphene.Mutation):
    class Arguments:
        object_data = HardwareInput()

    hardware = graphene.Field(HardwareType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Hardware.objects.create(
            name=object_data.name,
            description=object_data.description,
            provider=object_data.provider,
            status=object_data.status,
            category=object_data.category,
            model_name=object_data.model_name,
            model_number=object_data.model_number,
            support_url=object_data.support_url,
            createdByUserId=info.context.user,
        )

        # https://medium.com/analytics-vidhya/graphql-with-django-simple-yet-powerful-crud-part-2-bacce3668e35

        if object_data.contract is not None:
            relation_set = []
            for relation_id in object_data.contract:
                relation_object = Contract.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.contract.set(relation_set)

        if object_data.pricing is not None:
            relation_set = []
            for relation_id in object_data.pricing:
                relation_object = Pricing.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.pricing.set(relation_set)

        return CreateHardwareMutation(hardware=object_instance)


class UpdateHardwareMutation(graphene.Mutation):
    class Arguments:
        object_data = HardwareInput()

    hardware = graphene.Field(HardwareType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Hardware.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.description = object_data.description
            object_instance.provider = object_data.provider
            object_instance.status = object_data.status
            object_instance.category = object_data.category
            object_instance.model_name = object_data.model_name
            object_instance.model_number = object_data.model_number
            object_instance.support_url = object_data.support_url

            if object_data.contract is not None:
                relation_set = []
                for relation_id in object_data.contract:
                    relation_object = Contract.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.contract.set(relation_set)

            if object_data.pricing is not None:
                relation_set = []
                for relation_id in object_data.pricing:
                    relation_object = Pricing.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.pricing.set(relation_set)
            object_instance.save()
            return UpdateHardwareMutation(hardware=object_instance)
        return None


class DeleteHardwareMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    hardware = graphene.Field(HardwareType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = Hardware.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                  Application                                 #
# ---------------------------------------------------------------------------- #
class CreateApplicationMutation(graphene.Mutation):
    class Arguments:
        object_data = ApplicationInput()

    application = graphene.Field(ApplicationType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Application.objects.create(
            name=object_data.name,
            app_id=object_data.app_id,
            icon_url=object_data.icon_url,
            idp=object_data.idp,
            data_location=object_data.data_location,
            provider_location=object_data.provider,
            status=object_data.status,
            sso_manage_by_provider=object_data.sso_manage_by_provider,
            sso_type=object_data.sso_type,
            provider=object_data.provider,
            pricing=object_data.pricing,
            createdByUserId=info.context.user,
        )

        if object_data.owner is not None:
            relation_set = []
            for relation_id in object_data.owner:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)
            object_instance.owner.set(relation_set)

        if object_data.administrator is not None:
            relation_set = []
            for relation_id in object_data.administrator:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)
            object_instance.administrator.set(relation_set)
            s
        return CreateApplicationMutation(application=object_instance)


class UpdateApplicationMutation(graphene.Mutation):
    class Arguments:
        object_data = ApplicationInput()

    application = graphene.Field(ApplicationType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Application.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.app_id = object_data.app_id
            object_instance.icon_url = object_data.icon_url
            object_instance.idp = object_data.idp
            object_instance.pricing = object_data.pricing
            object_instance.data_location = object_data.data_location
            object_instance.provider_location = object_data.provider
            object_instance.status = object_data.status
            object_instance.sso_manage_by_provider = object_data.sso_manage_by_provider
            object_instance.sso_type = object_data.sso_type
            object_instance.provider = object_data.provider
            object_instance.save()

            if object_data.owner is not None:
                relation_set = []
                for relation_id in object_data.owner:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.pricing.set(relation_set)

            if object_data.administrator is not None:
                relation_set = []
                for relation_id in object_data.administrator:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.administrator.set(relation_set)
            return UpdateApplicationMutation(application=object_instance)
        return None


class DeleteApplicationMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    application = graphene.Field(ApplicationType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = Application.objects.get(
            pk=id, createdByUserId=info.context.user
        )

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                 Rule Mapping                                 #
# ---------------------------------------------------------------------------- #


class CreateRuleMappingMutation(graphene.Mutation):
    class Arguments:
        object_data = RuleMappingInput()

    rule_mapping = graphene.Field(RuleMappingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        department = Department.objects.filter(pk=object_data.department).first()
        subdepartment = SubDepartment.objects.filter(
            pk=object_data.subdepartment
        ).first()
        title = Title.objects.filter(pk=object_data.title).first()
        object_instance = RuleMapping.objects.create(
            department=department,
            subdepartment=subdepartment,
            title=title,
            createdByUserId=info.context.user,
        )
        return CreateRuleMappingMutation(rule_mapping=object_instance)


class UpdateRuleMappingMutation(graphene.Mutation):
    class Arguments:
        object_data = RuleMappingInput()

    rule_mapping = graphene.Field(RuleMappingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = RuleMapping.objects.get(pk=object_data.id)
        department = Department.objects.filter(pk=object_data.department).first()
        subdepartment = SubDepartment.objects.filter(
            pk=object_data.subdepartment
        ).first()
        title = Title.objects.filter(pk=object_data.title).first()

        if object_instance:
            object_instance.department = department
            object_instance.subdepartment = subdepartment
            object_instance.title = title
            object_instance.save()

            return UpdateRuleMappingMutation(rule_mapping=object_instance)
        return None


class DeleteRuleMappingMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    rule_mapping = graphene.Field(RuleMappingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = RuleMapping.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                               Hardware mapping                               #
# ---------------------------------------------------------------------------- #


class CreateHardwareMappingMutation(graphene.Mutation):
    class Arguments:
        object_data = HardwareMappingInput()

    hardware_mapping = graphene.Field(HardwareMappingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        rule = RuleMapping.objects.filter(pk=object_data.rule).first()
        object_instance = HardwareMapping.objects.create(
            rule=rule,
            createdByUserId=info.context.user,
        )

        if object_data.hardware is not None:
            relation_set = []
            for relation_id in object_data.hardware:
                relation_object = Hardware.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.hardware.set(relation_set)

        return CreateHardwareMappingMutation(hardware_mapping=object_instance)


class UpdateHardwareMappingMutation(graphene.Mutation):
    class Arguments:
        object_data = HardwareMappingInput()

    hardware_mapping = graphene.Field(HardwareMappingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = HardwareMapping.objects.get(pk=object_data.id)
        rule = RuleMapping.objects.filter(pk=object_data.rule).first()

        if object_instance:
            object_instance.rule = rule

            if object_data.hadrware is not None:
                relation_set = []
                for relation_id in object_data.hardware:
                    relation_object = Hardware.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.hardware.set(relation_set)
            object_instance.save()

            return UpdateHardwareMappingMutation(hardware_mapping=object_instance)
        return None


class DeleteHardwareMappingMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    hardware_mapping = graphene.Field(HardwareMappingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = HardwareMapping.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                   Building                                   #
# ---------------------------------------------------------------------------- #


class CreateBuildingMutation(graphene.Mutation):
    class Arguments:
        object_data = BuildingInput()

    building = graphene.Field(BuildingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Building.objects.create(
            name=object_data.name,
            createdByUserId=info.context.user,
            target_presence=object_data.target_presence,
            address=object_data.address,
            street_number=object_data.street_number,
            city=object_data.city,
            zip_code=object_data.zip_code,
            country=object_data.country,
            company=object_data.company,
            capacity=object_data.capacity,
            square_meters=object_data.square_meters,
            reception=object_data.reception,
            type=object_data.type,
            phone_number=object_data.phone_number,
            size=object_data.size,
            renting=object_data.renting,
            start_rending_date=object_data.start_rending_date,
            cost_per_month=object_data.cost_per_month,
            currency=object_data.currency,
            status=object_data.status,
            meeting_rom_quantity=object_data.meeting_rom_quantity,
            phonebooth_quantity=object_data.phonebooth_quantity,
            amphitheater_quantity=object_data.amphitheater_quantity,
        )

        if object_data.building_manager is not None:
            relation_set = []
            for relation_id in object_data.building_manager:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.building_manager.set(relation_set)

        if object_data.it_support is not None:
            relation_set = []
            for relation_id in object_data.it_support:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.it_support.set(relation_set)

        if object_data.general_manager is not None:
            relation_set = []
            for relation_id in object_data.general_manager:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.general_manager.set(relation_set)

        if object_data.culture_embassador is not None:
            relation_set = []
            for relation_id in object_data.culture_embassador:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.culture_embassador.set(relation_set)

        if object_data.people_director is not None:
            relation_set = []
            for relation_id in object_data.people_director:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.people_director.set(relation_set)

        if object_data.people_partner is not None:
            relation_set = []
            for relation_id in object_data.people_partner:
                relation_object = User.objects.get(pk=relation_id)
                relation_set.append(relation_object)

            object_instance.people_partner.set(relation_set)

        return CreateBuildingMutation(building=object_instance)


class UpdateBuildingMutation(graphene.Mutation):
    class Arguments:
        object_data = BuildingInput()

    building = graphene.Field(BuildingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Building.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.target_presence = object_data.target_presence
            object_instance.address = object_data.address
            object_instance.street_number = object_data.street_number
            object_instance.city = object_data.city
            object_instance.zip_code = object_data.zip_code
            object_instance.country = object_data.country
            object_instance.company = object_data.company
            object_instance.capacity = object_data.capacity
            object_instance.square_meters = object_data.square_meters
            object_instance.reception = object_data.reception
            object_instance.type = object_data.type
            object_instance.phone_number = object_data.phone_number
            object_instance.size = object_data.size
            object_instance.renting = object_data.renting
            object_instance.start_rending_date = object_data.start_rending_date
            object_instance.cost_per_month = object_data.cost_per_month
            object_instance.currency = object_data.currency
            object_instance.status = object_data.status
            object_instance.meeting_rom_quantity = object_data.meeting_rom_quantity
            object_instance.phonebooth_quantity = object_data.phonebooth_quantity
            object_instance.amphitheater_quantity = object_data.amphitheater_quantity

            if object_data.building_manager is not None:
                relation_set = []
                for relation_id in object_data.building_manager:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.building_manager.set(relation_set)

            if object_data.it_support is not None:
                relation_set = []
                for relation_id in object_data.it_support:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.it_support.set(relation_set)

            if object_data.general_manager is not None:
                relation_set = []
                for relation_id in object_data.general_manager:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.general_manager.set(relation_set)

            if object_data.culture_embassador is not None:
                relation_set = []
                for relation_id in object_data.culture_embassador:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.culture_embassador.set(relation_set)

            if object_data.people_director is not None:
                relation_set = []
                for relation_id in object_data.people_director:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.people_director.set(relation_set)

            if object_data.people_partner is not None:
                relation_set = []
                for relation_id in object_data.people_partner:
                    relation_object = User.objects.get(pk=relation_id)
                    relation_set.append(relation_object.id)

                object_instance.people_partner.set(relation_set)

            object_instance.save()

            return UpdateBuildingMutation(building=object_instance)
        return None


class DeleteBuildingMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    building = graphene.Field(BuildingType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = Building.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                              AccessControlRecord                             #
# ---------------------------------------------------------------------------- #


class CreateAccessControlRecordMutation(graphene.Mutation):
    class Arguments:
        object_data = AccessControlRecordInput()

    access_control_record = graphene.Field(AccessControlRecordType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        building = None
        user = None

        building = Building.objects.filter(pk=object_data.building).first()
        user = User.objects.filter(pk=object_data.user).first()
        controller = Controller.objects.filter(pk=object_data.controller).first()

        object_instance = AccessControlRecord.objects.create(
            source_id=object_data.source_id,
            building=building,
            date=object_data.date,
            user=user,
            card=object_data.card,
            controler=controller,
            user_type=object_data.visite_type,
        )
        return CreateAccessControlRecordMutation(access_control_record=object_instance)


class UpdateAccessControlRecordMutation(graphene.Mutation):
    class Arguments:
        object_data = AccessControlRecordInput()

    access_control_record = graphene.Field(AccessControlRecordType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = AccessControlRecord.objects.get(pk=object_data.id)
        building = Building.objects.filter(pk=object_data.building).first()
        user = None
        user = User.objects.filter(pk=object_data.user).first()
        controller = None
        controller = Controller.objects.filter(pk=object_data.controller).first()

        if object_instance:
            object_instance.source_id = object_data.source_id
            object_instance.building = building
            object_instance.date = object_data.date
            object_instance.user = user
            object_instance.card = object_data.card
            object_instance.controler = controller
            object_instance.user_type = object_data.visite_type
            object_instance.save()

            return UpdateAccessControlRecordMutation(
                access_control_record=object_instance
            )
        return None


class DeleteAccessControlRecordMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    access_control_record = graphene.Field(AccessControlRecordType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = AccessControlRecord.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None


# ---------------------------------------------------------------------------- #
#                                  controller                                  #
# ---------------------------------------------------------------------------- #


class CreateControllerMutation(graphene.Mutation):
    class Arguments:
        object_data = ControllerInput()

    controller = graphene.Field(ControllerDeviceType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Controller.objects.create(
            name=object_data.name,
            type=object_data.type,
            external_id=object_data.external_id,
        )
        return CreateControllerMutation(controller=object_instance)


class UpdateControllerMutation(graphene.Mutation):
    class Arguments:
        object_data = ControllerInput()

    controller = graphene.Field(ControllerDeviceType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        object_instance = Controller.objects.get(pk=object_data.id)

        if object_instance:
            object_instance.name = object_data.name
            object_instance.type = object_data.type
            object_instance.external_id = object_data.external_id

            object_instance.save()

            return UpdateControllerMutation(controller=object_instance)
        return None


class DeleteControllerMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    controller = graphene.Field(ControllerDeviceType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, id):
        object_instance = Controller.objects.get(pk=id)

        if object_instance:
            object_instance.delete()

        return None
