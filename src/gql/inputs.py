import graphene


class UserInput(graphene.InputObjectType):
    id = graphene.ID()
    email = graphene.String()
    first_name = graphene.String()
    last_name = graphene.String()
    date_joined = graphene.DateTime()
    is_active = graphene.Boolean()
    is_staff = graphene.Boolean()
    is_superuser = graphene.Boolean()
    avatar = graphene.String()
    department = graphene.String()
    subdepartment = graphene.String()
    manager = graphene.String()
    title = graphene.ID()
    company = graphene.String()
    employeeNumber = graphene.String()
    application = graphene.List(graphene.ID)
    hardware_mapping = graphene.ID()
    building_id = graphene.ID()


class DepartmentInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)


class SubDepartmentInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    department = graphene.ID()


class TitleInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)


class ServiceInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    description = graphene.String(required=True)
    department = graphene.ID()
    subdepartment = graphene.ID()
    owner = graphene.List(graphene.ID)
    supported_by = graphene.String()
    service_relationships = graphene.List(graphene.ID)
    tier = graphene.String()
    provider = graphene.ID()
    status = graphene.String(required=True)
    contract = graphene.List(graphene.ID)
    health_check = graphene.String()
    icon_url = graphene.String()
    pricing = graphene.List(graphene.ID)
    company = graphene.ID()
    identity_provider = graphene.ID()
    service_url = graphene.String()
    service_website = graphene.String()
    category = graphene.ID()
    support_url = graphene.String()
    administrator = graphene.List(graphene.ID)
    procurement = graphene.List(graphene.ID)


class ProviderInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    email = graphene.String()
    phone = graphene.String()
    vat_number = graphene.String()
    address = graphene.String()
    street_number = graphene.String()
    city = graphene.String()
    zip_code = graphene.String()
    country = graphene.String()
    currency_preffered = graphene.String()


class ContractInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    provider = graphene.ID()
    start_date = graphene.Date()
    end_date = graphene.Date()
    renewal_date = graphene.Date()
    duration = graphene.Int()
    duration_unit = graphene.String()
    total_cost = graphene.Float()
    cost_unit = graphene.Float()
    volume = graphene.Float()
    currency = graphene.String()
    status = graphene.String()
    company = graphene.ID()


class ContractDocumentInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    file = graphene.String()


class PricingInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    cost_unit = graphene.Float()
    total_cost = graphene.Float()
    volume = graphene.Float()
    pricing_date = graphene.Date()
    currency = graphene.String()
    status = graphene.String()
    time_unit = graphene.String()


class ConfigurationInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    description = graphene.String()
    category = graphene.String()
    key = graphene.String()
    value = graphene.String()
    enabled = graphene.Boolean()


class ServiceCategoryInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)


class IdentityProviderInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    icon_url = graphene.String()
    service_url = graphene.String()


class CompanyInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)


class ContactInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    email = graphene.String()
    firstname = graphene.String()
    lastname = graphene.String()
    phone = graphene.String()


class HardwareCategoryInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)


class HardwareInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    description = graphene.String(required=True)
    provider = graphene.ID()
    status = graphene.String(required=True)
    contract = graphene.List(graphene.ID)
    pricing = graphene.List(graphene.ID)
    category = graphene.ID()
    model_name = graphene.String()
    model_number = graphene.String()
    support_url = graphene.String()


class ApplicationInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    app_id = graphene.Int(required=True)
    icon_url = graphene.String()
    pricing = graphene.ID()
    idp = graphene.ID()
    data_location = graphene.ID()
    provider_location = graphene.ID()
    owner = graphene.List(graphene.ID)
    administrator = graphene.List(graphene.ID)
    status = graphene.String()
    sso_type = graphene.String()
    sso_manage_by_provider = graphene.Boolean()
    provider = graphene.ID()


class RuleMappingInput(graphene.InputObjectType):
    id = graphene.ID()
    department = graphene.ID()
    subdepartment = graphene.ID()
    title = graphene.ID()


class HardwareMappingInput(graphene.InputObjectType):
    id = graphene.ID()
    rule = graphene.ID()
    hardware = graphene.List(graphene.ID)


class BuildingInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    target_presence = graphene.Int()
    building_manager = graphene.List(graphene.ID)
    it_support = graphene.List(graphene.ID)
    general_manager = graphene.List(graphene.ID)
    culture_embassador = graphene.List(graphene.ID)
    people_director = graphene.List(graphene.ID)
    people_partner = graphene.List(graphene.ID)
    address = graphene.String()
    street_number = graphene.String()
    city = graphene.String()
    zip_code = graphene.String()
    country = graphene.String()
    company = graphene.ID()
    capacity = graphene.Int()
    square_meters = graphene.Float()
    reception = graphene.Boolean()
    type = graphene.ID()
    phone_number = graphene.String()
    size = graphene.ID()
    renting = graphene.Boolean()
    start_rending_date = graphene.DateTime()
    cost_per_month = graphene.Float()
    currency = graphene.ID()
    status = graphene.String()
    meeting_rom_quantity = graphene.Int()
    phonebooth_quantity = graphene.Int()
    amphitheater_quantity = graphene.Int()


class AccessControlRecordInput(graphene.InputObjectType):
    id = graphene.ID()
    source_id = graphene.Int(required=False)
    building = graphene.ID(required=False)
    date = graphene.DateTime(required=False)
    user = graphene.ID(required=False)
    card = graphene.String(required=False)
    controller = graphene.ID()
    user_type = graphene.String(required=False)


class ControllerInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String(required=True)
    type = graphene.String(required=False)
    external_id = graphene.Int(required=False)
