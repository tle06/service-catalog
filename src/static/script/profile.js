jQuery(function(){
    $(".profile_edit_button").hide();
    $(".select").hide();
    $(".form-select").hide();
  
    $("#profile_edit_button").click(function(e) {
      $(".profile_edit_button").show();
      $("#profile_edit_button").hide();
      $('.profile_input_field').prop('readonly', false);
      $(".profile_input_field").removeClass("form-control-plaintext");
      $(".profile_input_field").addClass("form-control");
      $(".profile_select_input_field").hide();
      $(".select").show();
      $(".form-select").show();
  }); 
  
  $("#profile_cancel_button").click(function(e) {
    $(".profile_edit_button").hide();
    $("#profile_edit_button").show();
    $('.profile_input_field').prop('readonly', true);
    $(".profile_input_field").removeClass("form-control");
    $(".profile_input_field").addClass("form-control-plaintext");
    $(".profile_select_input_field").show();
    $(".select").hide();
    $(".form-select").hide();
    
  }); 

  $("#avatar").change(function () {
    // submit the form
    $("#form_user_avatar").submit();
  });
  
  });