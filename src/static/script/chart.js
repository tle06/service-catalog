var chart1_data = JSON.parse(document.getElementById("chart1").textContent);
var target_presence = JSON.parse(
  document.getElementById("target_presence").textContent
);

const typeData = {};

chart1_data.forEach((item) => {
  const type = item.type.toLowerCase().replace("_", " ");
  if (!typeData[type]) {
    typeData[type] = {
      name: type,
      data: [],
      type: "column",
      stack: 1,
    };
  }
  typeData[type].data.push([
    new Date(item.date_value).getTime(),
    item.count_entry,
  ]);
});

for (const type in typeData) {
  typeData[type].data.sort((a, b) => a[0] - b[0]); // Sorting based on the Date object
}

// let max_column_height = 100;

// for (let key in typeData) {
//   if (typeData[key].hasOwnProperty("data")) {
//     let maxVal = Math.max(...typeData[key].data.map((subArray) => subArray[1]));
//     max_column_height += maxVal;
//   }
// }

// Convert the object into an array of series
const series = Object.values(typeData);

Highcharts.chart("presence_per_day_per_type", {
  chart: {
    zoomType: "x",
    height: 500,
  },
  rangeSelector: {
    verticalAlign: "top",
  },
  title: {
    text: "Office presence per day and user type",
    align: "center",
  },
  xAxis: {
    type: "datetime",
    crosshair: true,
    labels: {
      format: "{value:%d-%m-%Y}",
    },
  },
  yAxis: {
    min: 0,

    title: {
      text: "Count visit",
    },
    plotLines: [
      {
        label: {
          text: "Target presence:" + target_presence,
          y: -5,
        },
        dashStyle: "longdashdot",
        value: target_presence,
        width: 2,
        color: "red",
      },
    ],

    stackLabels: {
      enabled: true,
    },
  },
  tooltip: {
    pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
  },
  plotOptions: {
    column: {
      stacking: "normal",
      dataLabels: {
        enabled: true,
      },
    },
  },
  series: series,
});
