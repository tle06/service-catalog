var tooltipTriggerList = [].slice.call(
  document.querySelectorAll('[data-bs-toggle="tooltip"]')
);
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl);
});

var defaultHandler = function (action, pathName) {
  return function () {
    pk = this.options[arguments[0]].sourceid;
    lpk = arguments[0];
    url = `${action}/${pk}/${pathName}/${lpk}`;

    if (
      pathName == "linkpricing" ||
      pathName == "linkservice" ||
      pathName == "linkhardware" ||
      pathName == "adddatalocation" ||
      pathName == "addproviderlocation"
    ) {
      url = `${action}/${pk}/${pathName}`;
    }
    $.post(url, {
      reply: false,
      csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
      relation: lpk,
    });
  };
};

var defaultTomSelectSettings = {
  create: false,
  allowEmptyOption: false,
  plugins: ["checkbox_options", "remove_button"],
  maxOptions: 100,
  sortField: {
    field: "text",
    direction: "asc",
  },
};

document.querySelectorAll(".tom-select").forEach((el) => {
  let settings = defaultTomSelectSettings;
  new TomSelect(el, settings);
});

document.querySelectorAll(".tom-select-application").forEach((el) => {
  let settings = defaultTomSelectSettings;
  settings.onItemAdd = defaultHandler(
    (action = "update"),
    (pathName = "linkservice")
  );
  settings.onItemRemove = defaultHandler(
    (action = "update"),
    (pathName = "removeservice")
  );
  settings.maxItems = null;
  new TomSelect(el, settings);
});

document.querySelectorAll(".tom-select-pricing").forEach((el) => {
  let settings = defaultTomSelectSettings;
  (settings.onItemAdd = defaultHandler(
    (action = "update"),
    (pathName = "linkpricing")
  )),
    (settings.onItemRemove = defaultHandler(
      (action = "update"),
      (pathName = "removepricing")
    )),
    new TomSelect(el, settings);
});

document.querySelectorAll(".tom-select-data-location").forEach((el) => {
  let settings = defaultTomSelectSettings;
  (settings.onItemAdd = defaultHandler(
    (action = "update"),
    (pathName = "adddatalocation")
  )),
    (settings.onItemRemove = defaultHandler(
      (action = "update"),
      (pathName = "removedatalocation")
    )),
    new TomSelect(el, settings);
});

document.querySelectorAll(".tom-select-provider-location").forEach((el) => {
  let settings = defaultTomSelectSettings;
  (settings.onItemAdd = defaultHandler(
    (action = "update"),
    (pathName = "addproviderlocation")
  )),
    (settings.onItemRemove = defaultHandler(
      (action = "update"),
      (pathName = "removeproviderlocation")
    )),
    new TomSelect(el, settings);
});

document.querySelectorAll(".tom-select-mapping").forEach((el) => {
  let settings = defaultTomSelectSettings;
  settings.onItemAdd = defaultHandler(
    (action = "update"),
    (pathName = "linkhardware")
  );
  settings.onItemRemove = defaultHandler(
    (action = "update"),
    (pathName = "removehardware")
  );
  settings.maxItems = null;
  new TomSelect(el, settings);
});

(function () {
  "use strict";

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll(".needs-validation");

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach(function (form) {
    form.addEventListener(
      "submit",
      function (event) {
        if (!form.checkValidity()) {
          event.preventDefault();
          event.stopPropagation();
        }

        form.classList.add("was-validated");
      },
      false
    );
  });
})();

var defaultDatatableSettings = {
  columnDefs: [{ orderable: false, targets: 0 }],
  pageLength: 50,
};

document.querySelectorAll(".custom-datatable").forEach((el) => {
  let settings = defaultDatatableSettings;

  if (el.id == "datatable_list_building") {
    settings.columnDefs.push({ orderable: false, targets: 5 });
    settings.columnDefs.push({ orderable: false, targets: 6 });
  }

  if (el.id == "datatable_list_service") {
    settings.columnDefs.push({ orderable: false, targets: 1 });
  }
  new DataTable(el, settings);
});
