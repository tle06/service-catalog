provider "aws" {
  region = "eu-west-3"
}

provider "azurerm" {
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
}

provider "random" {
}

provider "template" {
}

provider "scaleway" {
  zone   = "fr-par-1"
  region = "fr-par"

}