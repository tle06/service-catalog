#!/bin/bash

echo "[EXCUTION] 0.sh file"

if [[ "$WORKER_MODE" != "true" ]]; then
    
    echo "[EXCUTION] WORKDER MODE disabled"
    
    echo "[EXCUTION] prep server"
    poetry run python manage.py prep_server
    
    if [[ "$START_REDIS" == "true" ]]; then
        echo "[EXCUTION] Start local redis server"
        redis-server --version
        redis-server --daemonize yes
        redis-cli ping
    fi
    
    if [[ "$START_CELERY_WORKER" == "true" ]]; then
        echo "[EXCUTION] CELERY WORKER"
        celery -A core worker -l info -E --pool=solo --detach
    fi
    
    if [[ "$START_CELERY_BEAT" == "true" ]]; then
        echo "[EXCUTION] CELERY BEAT"
        celery -A core beat -l info --detach
    fi
fi