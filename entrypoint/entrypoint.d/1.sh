#!/bin/bash
echo "[EXCUTION] 1.sh file"

if [[ "$WORKER_MODE" == "true" ]]; then
    echo "[EXCUTION] Workder mode activated. Web app will not start"
    echo "[EXCUTION] WORKDER MODE enabled"
    echo "[EXCUTION] Start local redis server"
    redis-server --version
    redis-server --daemonize yes
    redis-cli ping
    
    echo "[EXCUTION] CELERY WORKER"
    celery -A core worker -l info -E --pool=solo --detach
    
    echo "[EXCUTION] CELERY BEAT"
    celery -A core beat -l info
    
else
    if [[ "$USE_GUNICORN" == "true" ]]; then
        echo "[EXCUTION] start application with gunicorn"
        gunicorn -c gunicorn.conf.py
    else
        echo "[EXCUTION] start application with poetry"
        poetry run python manage.py start_server
    fi
    
fi